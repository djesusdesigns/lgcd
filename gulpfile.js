var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
	mix.styles([
        'bootstrap.min.css',
        'bootstrap-theme.min.css'
    ]).sass([
        'main.scss'
    ]).scripts([
        'jquery-2.1.4.min.js',
        'bootstrap.min.js',
        'cundiamarillas.js'
    ]).browserSync({
		proxy: '127.0.0.1:8000'
	});    
});
