<?php

return array(

    //'binary'  => base_path('vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64'),
    //'binary'  => env('SNAPPY_BIN','/home/felipe/quicklyapps/lgc/wkhtmltox_linux_64/bin/wkhtmltopdf')
    //'binary'  => '/usr/local/bin/wkhtmltopdf',
    'pdf' => array(
        'enabled' => true,
        'binary'  => env('SNAPPY_BIN','/home/felipe/quicklyapps/lgc/wkhtmltox_linux_64/bin/wkhtmltopdf'),
        'timeout' => false,
        'options' => array(),
        'env'     => array(),
    ),
    'image' => array(
        'enabled' => true,
        'binary'  => '/usr/local/bin/wkhtmltoimage',
        'timeout' => false,
        'options' => array(),
        'env'     => array(),
    ),


);
