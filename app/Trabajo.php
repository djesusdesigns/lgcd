<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Trabajo extends Model
{
    protected $table = 'trabajos';
    use SoftDeletes;

    public function tipo_educacion(){
        return $this->belongsTo('App\TipoEducacion', 'tipo_educacion_id');
    }

    public function tipo_contrato(){
        return $this->belongsTo('App\TipoContrato', 'tipo_contrato_id');
    }

    public function lugar(){
        return $this->belongsTo('App\Lugar', 'lugar_id');
    }

    public function entidad(){
        return $this->belongsTo('App\Entidad', 'entidad_id');
    }

    public function categoria(){
        return $this->belongsTo('App\Categoria', 'categoria_id');
    }

	public function aspirantes(){
        return $this->belongsToMany('App\Aspirante', 'aspirante_trabajo', 'trabajo_id', 'aspirante_id');
    }
}
