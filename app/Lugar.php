<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// representa un municipio/departamento/cuidad

class Lugar extends Model
{
    protected $table = 'lugares';
    public $timestamps = false;

    public function eventos(){
        return $this->hasMany('App\Evento', 'lugar_id');
    }

    // esto es una relacion recursiva, obtiene el lugar de categoria superior
    public function padre(){
        return $this->belongsTo('App\Lugar','lugar_id', 'id');
    }

    // esto es una relacion recursiva, obtiene el lugar de categoria inferior
    public function hijos(){
        return $this->hasMany('App\Lugar','lugar_id', 'id');
    }

    public function cobertura_entidades(){
        return $this->belongsToMany('App\Entidad', 'coberturas', 'lugar_id', 'entidad_id');
    }

    public function sucursales(){
        return $this->hasMany('App\Sucursal', 'lugar_id');
    }

    public function trabajos(){
        return $this->hasMany('App\Trabajo', 'lugar_id');
    }

    public function aspirantes_alertas(){
        return $this->belongsToMany('App\Aspirante', 'alertas', 'lugar_id', 'aspirante_id');
    }

    public function aspirantes(){
        return $this->hasMany('App\Aspirante', 'lugar_id');
    }

    public function experiencias(){
        return $this->hasMany('App\Experiencia', 'lugar_id');
    }

    public function tramites(){
        return $this->hasMany('App\Tramite', 'lugar_id');
    }

    public function multimedias(){
        return $this->hasMany('App\MultimediaLugar', 'lugar_id');
    }

    public function sitiosInteres(){
        return $this->hasMany('App\SitioInteres', 'lugar_id');
    }

    public function simbolos(){
        return $this->hasMany('App\Simbolo', 'lugar_id');
    }

    public function datos(){
        return $this->hasOne('App\DatosLugar', 'lugar_id');
    }

    public function dependencias(){
        return $this->hasMany('App\Dependencia', 'lugar_id');
    }

    public function numEmergencias(){
        return $this->hasMany('App\NumEmergencia', 'lugar_id');
    }

    public function noticias(){
        return $this->hasMany('App\Noticia', 'lugar_id');
    }

    public function fotos(){
        return $this->hasMany('App\MultimediaLugar', 'lugar_id')->where('tipo','=','Foto');
    }

    public function funcionarios()
    {
        return $this->hasManyThrough('App\Funcionario', 'App\Dependencia');
    }

    static function municipios(){
        return Lugar::where('tipo', '=', 'Municipio')->where('lugar_id','LIKE', '25%');
    }

    static function todos_los_municipios(){
        return Lugar::where('tipo', '=', 'Municipio');
    }

    static function departamentos(){
        return Lugar::where('tipo', '=', 'Departamento');
    }

    static function provincias(){
        return Lugar::where('tipo', '=', 'Provincia')->where('lugar_id', '=', '25');
    }

    public function escudo()
    {
        return $this->simbolos()->select('simbolos.*')->where('tipo','=','escudo')->first();
    }

    
}
