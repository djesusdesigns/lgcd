<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//almacena la información necesaria de pagos de municipios o empresas
// para acceder a las caracteristicas premium de la guia
// una de esas caracteristicas es poder personalizar la guia en el caso
// de los municipios
// o predominar en las busquedas en el caso de las empresas

class Suscripcion extends Model
{
    protected $table = 'suscripciones';

    public function usuario(){
        return $this->belongsTo('App\Usuario', 'usuario_id');
    }
}
