<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// representa los funcionarios que tiene la gobernacion de un municipio
// se relaciona fuertemente con la tabla dependencias

class Funcionario extends Model
{
    protected $table = 'funcionarios';

    public function dependencia()
    {
        return $this->belongsTo('App\Dependencia', 'dependencia_id');
    }
}
