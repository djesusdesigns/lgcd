<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReqExpress extends Model
{
    protected $table = 'req_express';

    public function entidad(){
        return $this->belongsTo('App\Entidad', 'entidad_id');
    }

    public function lugar(){
        return $this->belongsTo('App\Lugar', 'lugar_id');
    }

    public function aplicante(){
        return $this->belongsTo('App\Usuario', 'aplicante_id');
    }
}
