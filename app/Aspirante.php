<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

// Un aspirante es una PERSONA que se registra especialente para solicitar empleo
// Aunque tambien puede crear eventos, los eventos no estan relacionados con esta entidad
// para encontrar cuantos eventos a creado un aspirante se debe consultar primero el metodo usuario

class Aspirante extends Model
{
    protected $table = 'aspirantes';
    protected $appends = ['edad'];

    public function lugar(){
		return $this->belongsTo('App\Lugar','lugar_id', 'id');
	}

    public function usuario(){
		return $this->belongsTo('App\Usuario','usuario_id', 'id');
	}

	public function trabajos(){
        return $this->belongsToMany('App\Trabajo', 'aspirante_trabajo', 'aspirante_id', 'trabajo_id')->withPivot('estado');
    }

	public function solicitudes(){
		return $this->hasMany('App\AspiranteTrabajo', 'aspirante_id');
	}

	public function estudios(){
		return $this->hasMany('App\Educacion', 'aspirante_id');
	}

	public function experiencias(){
		return $this->hasMany('App\Experiencia', 'aspirante_id');
	}

	public function alertas(){
		return $this->hasMany('App\Alerta', 'aspirante_id');
	}

	public function alertas_categorias(){
        return $this->belongsToMany('App\Categoria', 'alertas', 'aspirante_id', 'categoria_id');
    }

	public function alertas_lugares(){
        return $this->belongsToMany('App\Lugar', 'alertas', 'aspirante_id', 'lugar_id');
    }
    
    public function tipo_educacion(){
        return $this->belongsTo('App\TipoEducacion', 'tipo_educacion_id');
    }

    public function profesion(){
		return $this->belongsTo('App\Profesion','profesion_id', 'id');
	}

    public function getEdadAttribute()
    {
    	$fecha = time() - strtotime($this->f_nacimiento);
		return floor($fecha / 31556926);
    }

    public function getFotosAttribute()
    {
    	//dd($this->foto[0]);
    	if(is_array($this->foto)){
    		return $this->foto;
    	}else if(!is_null($this->foto)){
    		$fotos = json_decode($this->foto);
			return $fotos;
    	}else{
    		return null;
    	}
    	
    }
}
