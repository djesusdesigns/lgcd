<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// representa la fuente de una noticia

class Fuente extends Model
{
    protected $table = 'fuentes';
    public $timestamps = false;

    public function noticias(){
        return $this->hasMany('App\Noticia', 'fuente_id');
    }
}
