<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



class MultimediaLugar extends Model
{
    protected $table = 'multimedia';
    public $timestamps = false;

    public function lugar(){
        return $this->belongsTo('App\Lugar', 'lugar_id');
    }
}
