<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//representa los productos y servicios que almacena la BD

class Produservicio extends Model
{
    protected $table = 'produservicios';
    public $timestamps = false;


	public function entidades(){
        return $this->belongsToMany('App\Entidad', 'entidades_produservicios', 'produservicio_id', 'entidad_id');
    }

    public function superProduservicio(){
        return $this->belongsTo('App\superProduservicio', 'super_profesion_id');
    }
}
