<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoEducacion extends Model
{
    protected $table = 'tipo_educacion';

	public function trabajos(){
		return $this->hasMany('App\Trabajo', 'tipo_educacion_id');
	}

	public function aspirantes(){
		return $this->hasMany('App\Aspirante', 'tipo_educacion_id');
	}
}
