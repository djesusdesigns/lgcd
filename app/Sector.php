<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// almacena los sectores economicos a los que puede pertenecer un comercio

class Sector extends Model
{
    protected $table = 'sectores';

	public function entidades(){
		return $this->hasMany('App\Entidad', 'sector_id');
	}
}
