<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//una entidad puede subir distintos tipos de archivos
//recordar que la entidad puede ser una empresa,medio o municipio

class Archivo extends Model
{
    protected $table = 'archivos';

    public function modelo()
    {
        return $this->morphTo();
    }
	
	/*
	static function documentos($entidad_id){
		$archivos = Archivo::where('entidad_id', $entidad_id)->
							where('tipo', 'documento')->
							get();
		return $archivos;
	}
	*/
}
