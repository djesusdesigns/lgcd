<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// representa los productos y servicios que compra/vende un comercio

class EntidadProduservicio extends Model
{
    protected $table = 'entidades_produservicio';
    public $timestamps = false;

    public function entidad(){
		return $this->belongsTo('App\Entidad','entidad_id', 'id');
	}

    public function produservicio(){
		return $this->belongsTo('App\Produservicio','produservicio_id', 'id');
	}
}
