<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// representa la experiencia laboral de un aspirante

class Experiencia extends Model
{
    protected $table = 'experiencia';

    public function lugar(){
        return $this->belongsTo('App\Lugar', 'lugar_id');
    }

    public function aspirante(){
        return $this->belongsTo('App\Aspirante', 'aspirante_id');
    }
}
