<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// representa que campo de cobertura tiene una empresa/medio

class Cobertura extends Model
{
    protected $table = 'coberturas';

    public function lugar(){
		return $this->belongsTo('App\Lugar','lugar_id', 'id');
	}

    public function entidad(){
		return $this->belongsTo('App\Entidad','entidad_id', 'id');
	}
}
