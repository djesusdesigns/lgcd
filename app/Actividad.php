<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/*
* Este modelo representa las actividades/categorias economicas que realiza una empresa o comercio
*/

class Actividad extends Model
{
    protected $table = 'actividades';
    public $timestamps = false;

    // esto es una relacion recursiva, obtiene la actividad de categoria superior
    public function padre(){
		return $this->belongsTo('App\Actividad','actividad_id', 'id');
	}

	// esto es una relacion recursiva, obtiene la actividad de categoria inferior
	public function hijos(){
		return $this->hasMany('App\Actividad', 'actividad_id');
	}

	public function entidades(){
        return $this->belongsToMany('App\Entidad', 'entidades_actividades', 'actividad_id', 'entidad_id');
    }
}
