<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// representa la educación que registre un asirante

class Educacion extends Model
{
    protected $table = 'educacion';

    public function aspirante(){
		return $this->belongsTo('App\Aspirante','aspirante_id', 'id');
	}

    public function tipo_educacion(){
		return $this->belongsTo('App\TipoEducacion','tipo_educacion_id', 'id');
	}
}
