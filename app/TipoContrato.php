<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoContrato extends Model
{
    protected $table = 'tipo_contrato';

	public function trabajos(){
		return $this->hasMany('App\Trabajo', 'tipo_contrato_id');
	}
}
