<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//guarda principalmente el himno, escudo y bandera del lugar

class Simbolo extends Model
{
    protected $table = 'simbolos';
    public $timestamps = false;

    public function lugar(){
        return $this->belongsTo('App\Lugar', 'lugar_id');
    }
}
