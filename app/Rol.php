<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// un usuario puede ser una empresa/municipio/aspirante o administrador


class Rol extends Model
{
    protected $table = 'roles';

	public function usuarios(){
		return $this->hasMany('App\Usuario', 'rol_id');
	}
}
