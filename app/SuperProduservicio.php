<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuperProduservicio extends Model
{
    protected $table = 'super_produservicios';
    public $timestamps = false;


	public function produservicios(){
        return $this->hasMany('App\Produservicio', 'super_produservicio_id');
    }
}
