<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// Entidad que representa la tabla de rompimiento aspirante_trabajo
// almacena a cuantos trabajos ha aplicado un aspirante 

class AspiranteTrabajo extends Model
{
    protected $table = 'aspirante_trabajo';

    public function trabajo(){
		return $this->belongsTo('App\Trabajo','trabajo_id', 'id');
	}

    public function aspirante(){
		return $this->belongsTo('App\Aspirante','aspirante_id', 'id');
	}
}
