<?php

namespace App;

/*
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
*/

use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Database\Eloquent\SoftDeletes;

//representa el usuario que se autentica dentro del sistema
// un usuario puede ser una empresa/municipio/aspirante o administrador

class Usuario extends Authenticatable
{
    use Notifiable;
	
    protected $table = 'usuarios';

    protected $hidden = ['contrasena', 'remember_token'];

    protected $dates = ['deleted_at'];

    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    public function getAuthPassword()
    {
        return $this->contrasena;
    }

    public function getEmailForPasswordReset()
    {
        return $this->correo;
    }

    public function username()
    {
        return 'correo';
    }

    public function noticias(){
        return $this->hasMany('App\Noticia', 'usuario_id')->orderBy('noticias.created_at');
    }

    public function eventos(){
        return $this->hasMany('App\Evento', 'usuario_id')->orderBy('eventos.created_at');
    }

    public function columnas(){
        return $this->hasMany('App\Columna', 'usuario_id')->orderBy('columnas.created_at');
    }

    public function sitiosInteres(){
        return $this->hasMany('App\SitioInteresUsuario', 'usuario_id')->orderBy('sitios_interes_usuario.created_at');
    }

    public function aspirante(){
        return $this->hasOne('App\Aspirante', 'usuario_id');
    }

    public function rol(){
        return $this->belongsTo('App\Rol', 'rol_id');
    }

    public function suscripciones(){
        return $this->hasMany('App\Suscripcion', 'usuario_id');
    }

    public function ultimaSuscripcion(){
        return $this->suscripciones()->orderBy('f_inicio','desc')->first();
    }

    public function entidad(){
        return $this->hasOne('App\Entidad', 'usuario_id');
    }

    public function likes(){
        return $this->hasMany('App\Like', 'usuario_id');
    }

    public function telefonos(){
        return $this->hasMany('App\Telefono', 'usuario_id');
    }

    public function comentarios(){
        return $this->hasMany('App\Comentario', 'usuario_id');
    }

    public function getNombreCompletoAttribute()
    {
        if($this->rol_id == 1)
            return $this->nombre." ".$this->apellido;
        else if($this->rol_id == 2)
            return $this->entidad->razon_social;
        else
            return $this->nombre;

    }

    public function getImagenAttribute()
    {
        $img = '/assets/img/user-avatar.png';
        if($this->rol_id == 1){
            if(isset($this->aspirante->foto) && sizeof($this->aspirante->fotos))
                return $this->aspirante->fotos[0];
        }elseif($this->rol_id == 2){
            if(strlen($this->entidad->logo))
                return $this->entidad->logo;
        }elseif($this->rol_id == 4){
            return '/assets/img/icon.png';
        }

        return $img;
    }
}
