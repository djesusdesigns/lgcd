<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

// representa los comentarios almacenados en el sistema
// para comentar las personas deben registrarse

class Like extends Model
{

    protected $table = 'likes';
    public $timestamps = false;

    public function usuario(){
        return $this->belongsTo('App\Usuario');
    }

    public function seccion()
    {
        return $this->morphTo();
    }
}
