<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// representa las dependencias de la gobernación de un municipio
// por ejemplo despacho del alcalde, tesoreria, etc

class Dependencia extends Model
{
    protected $table = 'dependencias';

    public function lugar(){
        return $this->belongsTo('App\Lugar','lugar_id', 'id');
    }

    public function funcionarios(){
        return $this->hasMany('App\Funcionario', 'dependencia_id');
    }

    public function padre(){
		return $this->belongsTo('App\Dependencia','dependencia_id', 'id');
	}

	public function hijos(){
		return $this->hasMany('App\Dependencia', 'dependencia_id');
	}
}
