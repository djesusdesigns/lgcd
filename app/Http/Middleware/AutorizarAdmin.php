<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AutorizarAdmin{

    public function handle($request, Closure $next){
        $rol = Auth::user()->rol_id;
        if($rol === 4){
            return $next($request);
        }else{
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('/');
            }            
        }
        
    }
}