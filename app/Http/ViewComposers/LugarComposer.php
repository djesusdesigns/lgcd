<?php 


namespace App\Http\ViewComposers;

use App\Lugar;
use Illuminate\View\View;


class LugarComposer
{

	public function compose(View $view)
	{
		$municipios = Lugar::municipios()->orWhere('id','=','25')->orderBy('tipo','asc')->orderBy('nombre','asc')->get();
		$view->with('municipios', $municipios);
	}
}
