<?php 


namespace App\Http\ViewComposers;

use App\TipoContrato;
use Illuminate\View\View;


class ContratoComposer{

	public function compose(View $view)
	{
		$contratos = TipoContrato::get();
		$view->with('contratos', $contratos);
	}
}