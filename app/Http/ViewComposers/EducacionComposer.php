<?php 


namespace App\Http\ViewComposers;

use App\TipoEducacion;
use Illuminate\View\View;


class EducacionComposer{

	public function compose(View $view)
	{
		$educaciones = TipoEducacion::get();
		$view->with('educaciones', $educaciones);
	}
}