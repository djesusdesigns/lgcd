<?php 


namespace App\Http\ViewComposers;

use App\Actividad;
use Illuminate\View\View;


class ActividadComposer
{

	public function compose(View $view)
	{
		$actividades = Actividad::get();
		$view->with('actividades', $actividades);
	}
}