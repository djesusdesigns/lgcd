<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Noticia;
use App\Evento;
use App\Columna;
use App\Lugar;
use App\Entidad;
use App\MultimediaLugar;
use App\SitioInteres;

use View;
use DB;

class InicioController extends Controller
{

    public function index(){
        

        $ultimasNoticias = Noticia::publicadas()
                                ->take(20)
                                ->orderBy('f_publicacion', 'desc')
                                ->get();
        
        $ultimasColumnas = Columna::publicadas()
                                ->take(20)
                                ->orderBy('f_publicacion', 'desc')
                                ->get();

        $ultimosEventos = Evento::orderBy('f_realizacion', 'desc')
                                ->take(20)
                                ->get();

        $noticiasDestacadas = Noticia::publicadas()
                                ->take(20)
                                ->where('destacar','=',1)
                                ->orderBy('id', 'desc')
                                ->get();
        
        $eventosDestacados = Evento::where('destacar','=',1)
                                ->take(20)
                                ->orderBy('f_realizacion', 'desc')
                                ->get();

        $sitiosInteres = SitioInteres::orderByRaw("RAND()")
                                ->take(20)
                                ->get();

        if(sizeof($noticiasDestacadas)==0)
            $noticiasDestacadas = $ultimasNoticias;

        if(sizeof($eventosDestacados)==0)
            $eventosDestacados = $ultimosEventos;
       
        foreach ($ultimasNoticias as $key => $noti) {
            $noti = $this->completarDatosNoticia($noti);
            $noticiasDestacadas[] = $noti;
        }

        foreach ($ultimasColumnas as $key => $columna) {
            $columna = $this->completarDatosColumna($columna);
            $ultimasColumnas[] = $columna;
        }

        foreach ($ultimosEventos as $key => $evt) {
            $evt = $this->completarDatosEvento($evt);
            $eventosDestacados[] = $evt;
        }

        foreach ($noticiasDestacadas as $key => $noti) {
            $noti = $this->completarDatosNoticia($noti);
        }

        foreach ($eventosDestacados as $key => $evt) {
            $evt = $this->completarDatosEvento($evt);
        }

        foreach ($sitiosInteres as $key => $sitio) {
            $sitio = $this->completarDatosSitio($sitio);
        }

        if (request()->is('api')) {

            return response()
            ->json([
                'ultimasNoticias' => $ultimasNoticias,
                'ultimosEventos' => $ultimosEventos,
                'noticiasDestacadas' => $noticiasDestacadas,
                'eventosDestacados' => $eventosDestacados,
                'sitiosInteres' => $sitiosInteres,
                'ultimasColumnas' => $ultimasColumnas
            ]);
        }

        return View::make('inicio')
        ->with([
            'ultimasNoticias' => $ultimasNoticias,
            'ultimosEventos' => $ultimosEventos,
            'noticiasDestacadas' => $noticiasDestacadas,
            'eventosDestacados' => $eventosDestacados,
            'sitiosInteres' => $sitiosInteres,
            'ultimasColumnas' => $ultimasColumnas
        ]);
    }

    public function volverInicio()
    {
        request()->session()->forget('lugarPremium');
        return redirect('/');
    }

    public function contactenos(){
        return View::make('contactenos');
    }

    public function acercade(){
        return View::make('acercade');
    }

    public function polidepriv(){
        return View::make('politicas-privacidad');
    }
    
    public function landinglogin(){
        return View::make('landing-login');
    }

    public function terminos(){
        return View::make('terminos');
    }

    public function ayudaEmpresas(){
        return View::make('ayuda-empresas');
    }
    
    public function ayudaPersonas(){
        return View::make('ayuda-personas');
    }

    public function pauteaqui(){
        return View::make('paute-aqui');
    }

    public function preguntas(){
        return View::make('preguntas');
    }

    public function reporteProblema(){
        return View::make('reporte-problema');
    }

    public function getUsuario(){
        $usuario = \Auth::user();
        $usuario->imagen = $usuario->imagen;

        if($usuario->rol_id == 1)
            $usuario->nombre_completo = $usuario->nombre_completo;
        else if($usuario->rol_id == 2 && $usuario->entidad){
            $usuario->nombre_completo = $usuario->entidad->razon_social;

            if($usuario->entidad->sucursal_principal)
                $usuario->entidad->sucursalPrincipal = $usuario->entidad->sucursal_principal()->first();
            else
                $usuario->entidad->sucursalPrincipal = (object) [];
        }

        return response()->json(['usuario' => $usuario ]);
    }
}
