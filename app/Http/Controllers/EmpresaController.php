<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Archivo;
use App\Comentario;
use App\Entidad;
use App\Telefono;
use App\Lugar;
use App\Usuario;
use App\Sucursal;
use App\Actividad;

use Auth;
use Validator;
use Storage;
use DB;

class EmpresaController extends Controller{

    public function index(Request $request){

        $validator = Validator::make($request->all(), [
            'estacionamiento' => 'integer',
            'domicilio' => 'integer',
            'lugar' => 'string'
        ]);

        $error = false;

        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return redirect()->back()->withErrors($error);
        }

        $actividades = Actividad::all();
        $horario = $request->input('horario');

        $empresas = Entidad::join('sucursales', 'entidades.id', '=', 'sucursales.entidad_id')
            ->join('usuarios', 'entidades.usuario_id', '=', 'usuarios.id')
            ->leftjoin('entidades_actividades','entidades_actividades.entidad_id','=','entidades.id')
            ->leftjoin('entidades_produservicios','entidades_produservicios.entidad_id','=','entidades.id')
            ->leftjoin('produservicios','entidades_produservicios.produservicio_id','=','produservicios.id')
            ->join('lugares','sucursales.lugar_id','=','lugares.id')
            ->select('sucursales.id', 'entidades.razon_social', 'sucursales.direccion')
            ->addSelect('entidades.descripcion', 'entidades.web', 'entidades.logo', 'entidades.requiere')
            ->addSelect('entidades.ofrece', 'sucursales.telefonos','sucursales.horario')
            ->addSelect('lugares.nombre as municipio','entidades.id as entidad_id');

        if( sizeof($request->all()) == 0){
            $request->session()->forget('lugar');
            $request->session()->forget('categoria');
            $request->session()->forget('requiere');
            $request->session()->forget('ofrece');
        }

        if($request->has('borrar-lugar')){
            $request->session()->forget('lugar');
        }

        if($request->has('borrar-categoria')){
            $request->session()->forget('categoria');
        }

        if($request->has('borrar-requiere')){
            $request->session()->forget('requiere');
        }

        if($request->has('borrar-ofrece')){
            $request->session()->forget('ofrece');
        }

        if ($request->has('categoria') || session('categoria') ) {
            $actividadId = $request->has('categoria') ? $request->categoria : session('categoria')['id'];
            $actividad = Actividad::find($actividadId);

            $request->session()->put('categoria',[
                'id'=>$actividad->id,
                'nombre'=>$actividad->nombre
            ]);

            $empresas = $empresas->where("entidades_actividades.actividad_id",$request->categoria);
        }

        if ($request->has('requiere') || session('requiere') ) {
            $requiere = $request->has('requiere') ? $request->requiere : session('requiere');
            $request->session()->put('requiere',$requiere);
            $empresas = $empresas->where("entidades.requiere","LIKE","%".$requiere."%");
            $empresas = $empresas->orWhere("entidades.ofrece","LIKE","%".$requiere."%");
        }

//        if ($request->has('ofrece') || session('ofrece') ) {
//            $ofrece = $request->has('ofrece') ? $request->ofrece : session('ofrece');
//            $request->session()->put('ofrece',$ofrece);
//            $empresas = $empresas->where("entidades.ofrece","LIKE","%".$ofrece."%");
//        }

        if($request->has('lugar') || session('lugar') || session('lugarPremium')){
            $lugarId = $request->has('lugar') ? $request->lugar : session('lugar')['id'];
            $lugarId = session('lugarPremium') ? session('lugarPremium')['lugar'] : $lugarId;
            
            $municipio = Lugar::find($lugarId);

            $request->session()->put('lugar',[
                'id'=>$municipio->id,
                'nombre'=>$municipio->nombre
            ]);

            $this->verificarTema($municipio->id);

            $empresas = $empresas->where("sucursales.lugar_id",$request->municipio);
        }
            
        //return $empresas->toSql();
        $empresas = $empresas
            ->where('entidades.tipo', 'Empresa')
            ->orderBy('usuarios.tipo', 'desc')
            ->groupBy('entidades.id')
            ->paginate(4);

        if(isset($municipio)){
            return view('amarillas.index',[
                'error' => $error,
                'empresas' => $empresas,
                'busquedaLugar' => $municipio,
                'actividades' => $actividades
            ]);
        }else{
            return view('amarillas.index',[
                'error' => $error,
                'empresas' => $empresas,
                'actividades' => $actividades
            ]);
        }
    }

    public function consultar($sucursalId){
        // razon-social, nombre, descripción, direccion, municipio, telefonos, web, imagen sucursal, horario, logo, estacionamiento, domicilio,
        //otras sucursales: municipio, direccion, horario, telefonos, domicilio, estacionamiento
        $error = false;

        $entidad = Sucursal::find($sucursalId)->entidad;
        $eventos = $entidad->usuario->eventos;
        $empresa = Sucursal::join('entidades', 'sucursales.entidad_id', '=', 'entidades.id')
            ->join('lugares', 'lugares.id', '=', 'sucursales.lugar_id')
            ->select('sucursales.id','sucursales.entidad_id','sucursales.imagen','sucursales.direccion','sucursales.horario','sucursales.telefonos','sucursales.domicilio','sucursales.estacionamiento')
            ->addSelect('sucursales.latitud','sucursales.longitud','lugares.nombre as municipio','entidades.descripcion','entidades.logo','entidades.web','entidades.razon_social','entidades.ofrece','entidades.requiere')
            ->where('sucursales.id',$sucursalId)
            ->first();

        if($empresa){
            $sucursales = Sucursal::join('entidades', 'sucursales.entidad_id', '=', 'entidades.id')
                ->join('lugares', 'lugares.id', '=', 'sucursales.lugar_id')
                ->select('sucursales.id','sucursales.direccion','sucursales.horario','sucursales.telefonos','sucursales.domicilio')
                ->addSelect('sucursales.estacionamiento','sucursales.latitud','sucursales.longitud','lugares.nombre as municipio')
                ->where('entidades.id', $empresa->entidad_id)
                ->where('sucursales.id','!=', $empresa->id)
                ->get();
                
            $empresa->horario = json_decode($empresa->horario);

            $sucursales->transform(function ($sucursal, $key) {
                $sucursal->horario = json_decode($sucursal->horario);                    
                return $sucursal;
            });

            $comentarios = Comentario::seccion('Sucursal',$empresa->id)->get();
        }else{
            $error = "No se encontró ninguna empresa";
            $sucursales = null;
            $comentarios = null;
        }

        $actividades = Actividad::all();
        
        return view('amarillas.show', [
            'error' => $error,
            'entidad'=> $entidad,
            'empresa'=> $empresa,
            'sucursales'=>$sucursales,
            'comentarios'=>$comentarios,
            'actividades' => $actividades,
            'eventos' => $eventos
        ]);
    }

    public function enviarCorreoContacto(Request $request){

        $validator = Validator::make($request->all(), [
            'idemp'  => 'required|exists:entidades,id',
            'nombre' => 'required|string',
            'correo' => 'required|email',
            'asunto' => 'required|string',
            'mensaje' => 'required|string'
        ]);

        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return redirect()->back()->withErrors($error);
        }

        $empresa = Entidad::find($request->idemp);
        $dataMail = [
                'nombre' => $request->nombre,
                'correo' => $request->correo,
                'asunto' => $request->asunto,
                'mensaje' => $request->mensaje
            ];

        \Mail::send('mail.mail-contacto-empresa', $dataMail, function ($m) use ($request,$empresa){
            $m->from($request->correo,$request->nombre);
            $correoEmpresa = (strlen($empresa->correo_privado)) ? $empresa->correo_privado : $empresa->usuario->correo;
            $m->to($correoEmpresa,$empresa->razon_social)->subject($request->asunto);
        });

        return redirect()->back()->with('mensajeForm','Correo enviado exitosamente');
    }

    public function busquedaPorMunicipio(Request $request,$slugMunicipio)
    {
        $slugArray = explode("-",$slugMunicipio);
        $idMunicipio = $slugArray[sizeof($slugArray)-1];
        $request->merge(['lugar' => $idMunicipio]);
        return $this->index($request);
    }

}