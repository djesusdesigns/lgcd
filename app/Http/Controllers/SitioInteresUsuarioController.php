<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lugar;
use App\SitioInteresUsuario;

use View;
use DB;

use Illuminate\Support\Facades\Log;

class SitioInteresUsuarioController extends Controller
{
	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */

	public function show($id)
	{
		$sitioInteres = SitioInteresUsuario::with('imagenes')->find($id);
		Log::critical($sitioInteres);

		if(!$sitioInteres)
			return abort(404);

		$otrosSitiosInteres = SitioInteresUsuario::take(3)
                            ->orderBy('id', 'desc')
                            ->get();		
		
		$comentarios = $sitioInteres->comentarios;		

		return View::make('sitio-interes-usuario.show')
					   ->with('sitioInteres', $sitioInteres)
					   ->with('comentarios', $comentarios)
					   ->with('otrosSitiosInteres', $otrosSitiosInteres);
		
		
	}

}
