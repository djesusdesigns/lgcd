<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Usuario;
use App\Entidad;
use App\Aspirante;

use View;

class UsuarioController extends Controller
{


	public function formLogin()
	{
		return View::make('usuarios.login');
	}

	public function formRegistro()
	{
		return View::make('usuarios.registro');
	}

	public function formRecuperar()
	{
		return View::make('usuarios.recuperar');
	}

	/**
     * Elimina un registro especifico de la base de datos
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = Usuario::find($id);

        if(is_null($usuario))
        	return response()->json(['error' => 'Error al eliminar este usuario, revisa los datos de ingreso']);

        if($usuario->rol_id == 1){
        	$aspirante = Aspirante::where('usuario_id',$usuario->id)->first();
        	\DB::table('aspirante_trabajo')->where('aspirante_id',$aspirante->id)->delete();
        	$aspirante->delete();
        }

        if($usuario->rol_id == 2 && $usuario->rol_id == 3){
        	$entidad = \Entidad::where('usuario_id',$usuario->id)->first();
        	\DB::table('entidades_actividades')->where('entidad_id',$entidad->id)->delete();
	        \DB::table('sucursales')->where('entidad_id',$entidad->id)->delete();
	        \DB::table('trabajos')->where('entidad_id',$entidad->id)->delete();
	        $entidad->delete();
        }

        \DB::table('eventos')->where('id',$usuario->id)->delete();
        \DB::table('comentarios')->where('id',$usuario->id)->delete();
        
        if($usuario->delete())
			return response()->json(['mensaje' => 'Usuario eliminado con éxito']);
		else
			return response()->json(['error' => 'Error al eliminar este usuario, revisa los datos de ingreso']);
    }

    public function notiEventos()
    {
        $user = \Auth::user();

        if(!$user)
            abort(404);

        $noticias = $user->noticias()->limit(10)->get();
        $eventos = $user->eventos()->limit(10)->get();

        foreach ($noticias as $key => $noti) {
            $noti = $this->completarDatosNoticia($noti);
        }

        foreach ($eventos as $key => $evt) {
            $evt = $this->completarDatosEvento($evt);
        }

        return response()->json([
            'noticias' => $noticias,
            'eventos' => $eventos
        ]);
    }
}