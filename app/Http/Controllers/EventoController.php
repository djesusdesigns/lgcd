<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lugar;
use App\Evento;
use App\Comentario;
use App\Entidad;

use View;
use Redirect;
use Input;
use Auth;
use Validator;

class EventoController extends Controller
{

	public function indexGuest(Request $request)
	{
		$fechasEventos = $this->obtenerFechas();
//		$eventos = Evento::activos()->orderBy('f_realizacion', 'asc');
		$eventos = Evento::orderBy('f_realizacion', 'desc');

		if( sizeof($request->all()) == 0){
			$request->session()->forget('lugar');
			$request->session()->forget('archivo');
		}

		if($request->has('borrar-lugar')){
			$request->session()->forget('lugar');
		}

		if($request->has('borrar-archivo')){
			$request->session()->forget('archivo');
		}

		if($request->has('lugar') || session('lugar') || session('lugarPremium')){
			$lugarId = $request->has('lugar') ? $request->lugar : session('lugar')['id'];
			$lugarId = session('lugarPremium') ? session('lugarPremium')['lugar'] : $lugarId;
			
			$municipio = Lugar::find($lugarId);

			$request->session()->put('lugar',[
				'id'=>$municipio->id,
				'nombre'=>$municipio->nombre
			]);

			$eventos = $eventos->where('lugar_id','=',$lugarId);
	        $this->verificarTema($municipio->id);
		}

		if($request->has('archivo') || session('archivo')){
			$archivo = $request->has('archivo') ? $request->archivo : session('archivo');
			$request->session()->put('archivo',$archivo);

			$eventos = $eventos->whereRaw("('".$request->archivo."' between f_realizacion and f_finalizacion)");
		}

		$eventos = $eventos->paginate(20);

		foreach ($eventos as $key => $evt) {
			$evt = $this->completarDatosEvento($evt);
		}

		if (request()->is('api/*')) {
			return response()
            ->json($eventos);
		}

		if(isset($municipio)){
			return View::make('eventos.index')
							->with('busquedaLugar', $municipio)
							->with('eventos', $eventos)
							->with('fechasEventos', $fechasEventos);	
		}else{
			return View::make('eventos.index')
							->with('eventos', $eventos)
							->with('fechasEventos', $fechasEventos);
		}

	}


	private function createDateRangeArray($strDateFrom,$strDateTo)
	{
	    // takes two dates formatted as YYYY-MM-DD and creates an
	    // inclusive array of the dates between the from and to dates.

	    // could test validity of dates here but I'm already doing
	    // that in the main script

	    $aryRange=array();

	    $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
	    $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

	    if ($iDateTo>=$iDateFrom)
	    {
	        array_push($aryRange,date('Y/m/d',$iDateFrom)); // first entry
	        while ($iDateFrom<$iDateTo)
	        {
	            $iDateFrom+=86400; // add 24 hours
	            array_push($aryRange,date('Y/m/d',$iDateFrom));
	        }
	    }
	    return $aryRange;
	}


	private function obtenerFechas()
	{
		
		$fecha1 = date('Y-m-d',strtotime('-2 month'));
		$fecha2 = date('Y-m-d',strtotime('+2 month'));

		$eventosFechas = Evento::whereBetween( 'f_realizacion', array($fecha1,$fecha2) )->get();

		$fechasEventos = [];

		foreach ($eventosFechas as $key => $evento) {
			$fechas = $this->createDateRangeArray($evento->f_realizacion,$evento->f_realizacion);
			foreach ($fechas as $key => $fecha) {
				$fechasEventos[$fecha] = $fecha;
			}
		}

		return $fechasEventos;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function show($id)
	{
		$evento = Evento::find($id);

		//dd($evento->archivos);
		
		if(!$evento)
			return redirect('eventos');

		$comentarios = $evento->comentarios;

		$fechasEventos = $this->obtenerFechas();
		
		$ultimosEventos = Evento::take(3)
                            ->orderBy('f_publicacion', 'desc')
                            ->get();

		return View::make('eventos.show')
			->with('evento', $evento)
			->with('fechasEventos', $fechasEventos)
			->with('ultimosEventos', $ultimosEventos)
			->with('comentarios', $comentarios);
	}

	public function archivo($fecha)
	{

		$eventos = Evento::whereRaw("('".$fecha."' between f_realizacion and f_finalizacion)")
							->orderBy('f_realizacion', 'desc')
							->paginate(5);

		$fechasEventos = $this->obtenerFechas();

		return View::make('eventos.index')
							->with('fechasEventos', $fechasEventos)
							->with('eventos', $eventos);
	}

	public function busquedaPorMunicipio(Request $request,$slugMunicipio)
	{
		$slugArray = explode("-",$slugMunicipio);
		$idMunicipio = $slugArray[sizeof($slugArray)-1];
		$request->merge(['lugar' => $idMunicipio]);
		return $this->indexGuest($request);
	}
}
