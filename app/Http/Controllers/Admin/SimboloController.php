<?php 
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lugar;
use App\Noticia;
use App\Fuente;
use App\Simbolo;

use View;
use DB;
use Input;
use Auth;
use Redirect;

class SimboloController extends Controller
{

	public $tipos = ['Bandera','Escudo','Himno','Flor','Arbol'];

	/**
	 * Muestra la lista de registros
	 *
	 * @return Response
	 */

	public function index($idMunicipio)
	{
		$municipio = Lugar::find($idMunicipio);
		return View::make('admin.simbolos.inicio')
						->with('simbolos', $municipio->simbolos)
						->with('municipio', $municipio);
	}


	/**
	 * Muestra el formulario para crear un nuevo registro.
	 *
	 * @return Response
	 */

	public function create($idMunicipio,$tipo="")
	{
		$simbolo = new Simbolo;
		$municipio = Lugar::find($idMunicipio);
		$datos = [ 
				'municipio' => $municipio,
				'simbolo' => $simbolo,
				'tipos' => $this->tipos,
				'accion' => 'crear'
			];

		if($tipo == 'escudo'){
			$simbolo->tipo = 'Escudo';
			return View::make('admin.simbolos.formulario-escudo')->with($datos);
		}else if($tipo == 'himno'){
			$simbolo->tipo = 'Himno';
			return View::make('admin.simbolos.formulario-himno')->with($datos);
		}else if($tipo == 'himno_youtube'){
			$simbolo->tipo = 'Himno Youtube';
			return View::make('admin.simbolos.formulario-himno-youtube')->with($datos);
		}else if($tipo == 'flor'){
			$simbolo->tipo = 'Flor';
			return View::make('admin.simbolos.formulario-flor')->with($datos);
		}else if($tipo == 'arbol'){
			$simbolo->tipo = 'Arbol';
			return View::make('admin.simbolos.formulario-arbol')->with($datos);
		}else if($tipo == 'bandera'){
			$simbolo->tipo = 'Bandera';
			return View::make('admin.simbolos.formulario-bandera')->with($datos);
		}else{
			$simbolo->tipo = 'Escudo';
			return View::make('admin.simbolos.formulario-escudo')->with($datos);
		}
	}


	/**
	 * Guarda un nuevo registro en la base de datos
	 *
	 * @return Response
	 */

	public function store($idMunicipio)
	{
		$municipio = Lugar::find($idMunicipio);

		if(!$municipio)
			return Redirect::to('admin/municipio/'.$idMunicipio.'/simbolo')->with('error',"Municipio no encontrado");

		$simbolo = new Simbolo;
		$simbolo->tipo = Input::get('tipo');
		$simbolo->lugar_id = $municipio->id;

		if($simbolo->tipo == 'bandera')
			$carpeta = 'banderas';
		else if($simbolo->tipo == 'escudo')
			$carpeta = 'escudos';
		else if($simbolo->tipo == 'flor')
			$carpeta = 'flores';
		else if($simbolo->tipo == 'arbol')
			$carpeta = 'arboles';

		if(Input::get('tipo') == 'himno')
			$simbolo->descripcion = Input::get('descripcion');
		else
			$simbolo->descripcion = $this->saveFile(Input::file('descripcion'),'multimedia/images/simbolos/'.$carpeta, $this->limpiarNombre($municipio->nombre) );

		$simbolo->save();

		if($simbolo)
			return Redirect::to('admin/municipio/'.$municipio->id.'/simbolo')->with('mensaje',"Simbolo creado con éxito");
		else
			return Redirect::to('admin/municipio/'.$municipio->id.'/simbolo')->with('error',"Error al crear este simbolo, revisa los datos de ingreso");
	}


	/**
	 * Muestra el formulario para editar un registro en especifico
	 *
	 * @param  int $id
	 * @return Response
	 */

	public function edit($idMunicipio,$idSimbolo,$tipo="")
	{
		$simbolo = Simbolo::find($idSimbolo);
		$municipio = Lugar::find($idMunicipio);
		$datos = [ 
				'municipio' => $municipio,
				'simbolo' => $simbolo,
				'tipos' => $this->tipos,
				'accion' => 'editar'
			];

		if($tipo == 'escudo'){
			$simbolo->tipo = 'Escudo';
			return View::make('admin.simbolos.formulario-escudo')->with($datos);
		}else if($tipo == 'himno'){
			$simbolo->tipo = 'Himno';
			return View::make('admin.simbolos.formulario-himno')->with($datos);
		}else if($tipo == 'flor'){
			$simbolo->tipo = 'Flor';
			return View::make('admin.simbolos.formulario-flor')->with($datos);
		}else if($tipo == 'arbol'){
			$simbolo->tipo = 'Arbol';
			return View::make('admin.simbolos.formulario-arbol')->with($datos);
		}else if($tipo == 'bandera'){
			$simbolo->tipo = 'Bandera';
			return View::make('admin.simbolos.formulario-bandera')->with($datos);
		}else{
			$simbolo->tipo = 'Escudo';
			return View::make('admin.simbolos.formulario-escudo')->with($datos);
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @return Response
	 */

	public function update($idMunicipio,$idSimbolo)
	{
		$municipio = Lugar::find($idMunicipio);

		if(!$municipio)
			return Redirect::to('admin/municipio/'.$idMunicipio.'/simbolo')->with('error',"Municipio no encontrado");

		$simbolo = Simbolo::find($idSimbolo);
		$simbolo->tipo = Input::get('tipo');
		$simbolo->lugar_id = $municipio->id;

		if($simbolo->tipo == 'bandera')
			$carpeta = 'banderas';
		else if($simbolo->tipo == 'escudo')
			$carpeta = 'escudos';
		else if($simbolo->tipo == 'flor')
			$carpeta = 'flores';
		else if($simbolo->tipo == 'arbol')
			$carpeta = 'arboles';

		if(Input::get('tipo') == 'himno'){
			$simbolo->descripcion = Input::get('descripcion');
		}else{
			if (Input::hasFile('descripcion'))
			{
				$simbolo->descripcion = $this->saveFile(Input::file('descripcion'),'multimedia/images/simbolos/'.$carpeta, $this->limpiarNombre($municipio->nombre) );
			}
		}

		$simbolo->save();

		if($simbolo)
			return Redirect::to('admin/municipio/'.$municipio->id.'/simbolo')->with('mensaje',"Simbolo editado con éxito");
		else
			return Redirect::to('admin/municipio/'.$municipio->id.'/simbolo')->with('error',"Error al editar este simbolo, revisa los datos de ingreso");
	}


	/**
	 * Elimina un registro especifico de la base de datos
	 *
	 * @param  int $id
	 * @return Response
	 */


	public function destroy($idMunicipio,$idSimbolo)
	{
		$municipio = Lugar::find($idMunicipio);
		$simbolo = Simbolo::find($idSimbolo);

		if($simbolo->delete())
			return Redirect::to('admin/municipio/'.$municipio->id.'/simbolo')->with('mensaje',"Simbolo eliminado con éxito");
		else
			return Redirect::to('admin/municipio/'.$municipio->id.'/simbolo')->with('error',"Error al eliminar este simbolo");
	}

	private function limpiarNombre($nombreMun){
		$nombreMun = str_replace(' ', '_', $nombreMun);
		$nombreMun = preg_replace('([^A-Za-z0-9])', '', $nombreMun);
		$nombreMun = strtolower($nombreMun);
		return $nombreMun;
	}


}
