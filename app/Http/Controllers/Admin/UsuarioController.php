<?php 

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Usuario;
use App\Lugar;
use App\Entidad;
use App\Sucursal;
use App\Actividad;
use App\Suscripcion;

use View;
use DB;
use Validator;
use Storage;
use Redirect;

class UsuarioController extends Controller
{

	/**
	 * Muestra la lista de registros
	 *
	 * @return Response
	 */
	public function index()
	{
        $usuarios = Usuario::all();
		return View::make('admin.usuarios.inicio')->with('usuarios', $usuarios);
	}


    /**
     * Muestra el formulario para editar un registro en especifico
     *
     * @param  int $id
     * @return Response
     */

    public function edit($id)
    {
        $usuario = Usuario::find($id);
        return View::make('admin.usuarios.formulario')
                    ->with('usuario',$usuario)
                    ->with('accion','editar');
    }


    /**
     * Actualizar un registro especifico
     *
     * @param  int $id
     * @return Response
     */

    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'contrasena' => 'nullable|min:6|confirmed'
        ]);

        if ($validator->fails())
            return back()->with('error',$validator->errors()->first());

        $usuario = Usuario::find($id);
        $usuario->correo = $request->correo;
        if(strlen($request->contrasena))
            $usuario->contrasena = bcrypt($request->contrasena);
        
        $usuario->save();

        if($request->enviar_correo){
            $dataCorreo = [ 'correo' => $usuario->correo,'contrasena' => $request->contrasena ];
            \Mail::send('mail.recuperar', $dataCorreo , function($message) use ($usuario)
            {
                $message->to($usuario->correo, 'La Guía Cundinamarca')->subject('Nueva contraseña la Guía Cundinamarca');
            });
        }

        return redirect('admin/usuario')->with('mensaje','Contraseña actualizada correctamente');
    }

    /**
     * Activa o desactiva un usuario, cuando el usuario no está activado no puede ingresar a la plataforma
     *
     * @param  int $id
     * @return Response
     */
    public function cambiar_estado($id)
    {
        $usuario = Usuario::find($id);
        $usuario->estado = ($usuario->estado) ? 0 : 1;
        $usuario->save();
        return back()->withInput()->with('mensaje','Usuario activado correctamente');
    }
}
