<?php 
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lugar;
use App\NumEmergencia;
use App\Fuente;

use View;
use DB;
use Input;
use Auth;
use Redirect;

class NumEmergenciaController extends Controller
{

	/**
	 * Muestra la lista de registros
	 *
	 * @return Response
	 */

	public function index($idMunicipio)
	{
		$municipio = Lugar::find($idMunicipio);
		return View::make('admin.emergencias.inicio')
						->with('numEmergencias', $municipio->numEmergencias)
						->with('municipio', $municipio);
	}


	/**
	 * Muestra el formulario para crear un nuevo registro.
	 *
	 * @return Response
	 */

	public function create($idMunicipio)
	{
		$num = new NumEmergencia;
		$municipio = Lugar::find($idMunicipio);
		$datos = [ 
				'municipio' => $municipio,
				'numEmergencia' => $num,
				'accion' => 'crear'
			];
		return View::make('admin.emergencias.formulario')->with($datos);
	}


	/**
	 * Guarda un nuevo registro en la base de datos
	 *
	 * @return Response
	 */

	public function store($idMunicipio)
	{
		$municipio = Lugar::find($idMunicipio);

		if(!$municipio)
			return Redirect::to('admin/municipio/'.$idMunicipio.'/num-emergencia')->with('error',"Municipio no encontrado");

		$numEmergencia = new NumEmergencia;
		$numEmergencia->nombre = Input::get('nombre');
		$numEmergencia->telefono = Input::get('telefono');
		$numEmergencia->lugar_id = $municipio->id;
		$numEmergencia->save();

		if($numEmergencia)
			return Redirect::to('admin/municipio/'.$municipio->id.'/num-emergencia')->with('mensaje',"Número de emergencia creado con éxito");
		else
			return Redirect::to('admin/municipio/'.$municipio->id.'/num-emergencia')->with('error',"Error al crear este número de emergencia, revisa los datos de ingreso");
	}


	/**
	 * Muestra el formulario para editar un registro en especifico
	 *
	 * @param  int $id
	 * @return Response
	 */

	public function edit($idMunicipio,$idNumEmergencia)
	{
		$numEmergencia = NumEmergencia::find($idNumEmergencia);
		$municipio = Lugar::find($idMunicipio);
		return View::make('admin.emergencias.formulario')
					->with('municipio',$municipio)
					->with('numEmergencia',$numEmergencia)
					->with('accion','editar');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @return Response
	 */

	public function update($idMunicipio,$idNumEmergencia)
	{
		$municipio = Lugar::find($idMunicipio);

		if(!$municipio)
			return Redirect::to('admin/municipio/'.$idMunicipio.'/num-emergencia')->with('error',"Municipio no encontrado");

		$numEmergencia = NumEmergencia::find($idNumEmergencia);
		$numEmergencia->nombre = Input::get('nombre');
		$numEmergencia->telefono = Input::get('telefono');
		$numEmergencia->save();


		if($numEmergencia)
			return Redirect::to('admin/municipio/'.$municipio->id.'/num-emergencia')->with('mensaje',"Número de emergencia creado con éxito");
		else
			return Redirect::to('admin/municipio/'.$municipio->id.'/num-emergencia')->with('error',"Error al crear este número de emergencia, revisa los datos de ingreso");
	}


	/**
	 * Elimina un registro especifico de la base de datos
	 *
	 * @param  int $id
	 * @return Response
	 */


	public function destroy($idMunicipio,$idSimbolo)
	{
		$municipio = Lugar::find($idMunicipio);
		$numEmergencia = NumEmergencia::find($idSimbolo);

		if($numEmergencia->delete())
			return Redirect::to('admin/municipio/'.$municipio->id.'/num-emergencia')->with('mensaje',"Número de emergencia eliminado con éxito");
		else
			return Redirect::to('admin/municipio/'.$municipio->id.'/num-emergencia')->with('error',"Error al eliminar este número de emergencia");
	}


}
