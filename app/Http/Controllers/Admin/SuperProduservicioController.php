<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\SuperProduservicio;

use DB;

class SuperProduservicioController extends Controller
{
    /**
     * Muestra la lista de registros
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $superProduservicios = SuperProduservicio::orderBy('nombre')->paginate(10);
        return view('admin.configuracion.superProduservicios.inicio')
                    ->with('superProduservicios',$superProduservicios);
    }

    /**
     * Muestra el formulario para crear un nuevo registro.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $superProduservicio = new SuperProduservicio;
        return view('admin.configuracion.superProduservicios.formulario')
                    ->with('accion','crear')
                    ->with('superProduservicio',$superProduservicio);
    }

    /**
     * Guarda un nuevo registro en la base de datos
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            DB::connection()->getPdo()->beginTransaction();
            $superProduservicio = new SuperProduservicio;
            $superProduservicio->nombre = $request->nombre;
            $superProduservicio->save();

            DB::connection()->getPdo()->commit();
        }catch(\Exception $e){
            DB::connection()->getPdo()->rollBack();
            return redirect('/admin/configuracion/superProduservicio')->with('error',"Error al crear esta superProduservicio, revisa los datos de ingreso; el sistema ha generado el siguiente error ".$e->getMessage());
        }


        return redirect('/admin/configuracion/superProduservicio')->with('mensaje',"SuperProduservicio creada con éxito");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Muestra el formulario para editar un registro en especifico
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $superProduservicio = SuperProduservicio::find($id);
        return view('admin.configuracion.superProduservicios.formulario')
                    ->with('accion','editar')
                    ->with('superProduservicio',$superProduservicio);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            DB::connection()->getPdo()->beginTransaction();
            $superProduservicio = SuperProduservicio::find($id);
            $superProduservicio->nombre = $request->nombre;
            $superProduservicio->save();

            DB::connection()->getPdo()->commit();
        }catch(\Exception $e){
            DB::connection()->getPdo()->rollBack();
            return redirect('/admin/configuracion/superProduservicio')->with('error',"Error al editar esta superProduservicio, revisa los datos de ingreso; el sistema ha generado el siguiente error ".$e->getMessage());
        }

        return redirect('/admin/configuracion/superProduservicio')->with('mensaje',"SuperProduservicio editada con éxito");
    }

    /**
     * Elimina un registro especifico de la base de datos
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::connection()->getPdo()->beginTransaction();
            $superProduservicio = SuperProduservicio::find($id);
            $superProduservicio->delete();

            DB::connection()->getPdo()->commit();
        }catch(\Exception $e){
            DB::connection()->getPdo()->rollBack();
            return redirect('/admin/configuracion/superProduservicio')->with('error',"Error al eliminar esta superProduservicio, revisa los datos de ingreso; el sistema ha generado el siguiente error ".$e->getMessage());
        }

        return redirect('/admin/configuracion/superProduservicio')->with('mensaje',"SuperProduservicio eliminada con éxito");        
    }

    public function buscar(Request $request)
    {
        $superProduservicios = SuperProduservicio::where('nombre','LIKE','%'.$request->palabraClave.'%')->paginate(10);
        return view('admin.configuracion.superProduservicios.inicio')
                    ->with('superProduservicios',$superProduservicios)
                    ->with('palabraClave',$request->palabraClave);
    }
}
