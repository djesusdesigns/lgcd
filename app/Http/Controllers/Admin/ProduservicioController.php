<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Produservicio;
use App\SuperProduservicio;

use DB;

class ProduservicioController extends Controller
{
    /**
     * Muestra la lista de registros
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produservicios = Produservicio::paginate(10);
        return view('admin.configuracion.produservicios.inicio')
                    ->with('produservicios',$produservicios);
    }

    /**
     * Muestra el formulario para crear un nuevo registro.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $produservicio = new Produservicio;
        $superProduservicios = SuperProduservicio::all();
        return view('admin.configuracion.produservicios.formulario')
                    ->with('accion','crear')
                    ->with('superProduservicios',$superProduservicios)
                    ->with('produservicio',$produservicio);
    }

    /**
     * Guarda un nuevo registro en la base de datos
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            DB::connection()->getPdo()->beginTransaction();
            $produservicio = new Produservicio;
            $produservicio->nombre = $request->nombre;
            $produservicio->super_produservicio_id = $request->super_produservicio_id;
            $produservicio->save();

            DB::connection()->getPdo()->commit();
        }catch(\Exception $e){
            DB::connection()->getPdo()->rollBack();
            return redirect('/admin/configuracion/produservicio')->with('error',"Error al crear esta produservicio, revisa los datos de ingreso; el sistema ha generado el siguiente error ".$e->getMessage());
        }


        return redirect('/admin/configuracion/produservicio')->with('mensaje',"Produservicio creada con éxito");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Muestra el formulario para editar un registro en especifico
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $superProduservicios = SuperProduservicio::all();
        $produservicio = Produservicio::find($id);
        return view('admin.configuracion.produservicios.formulario')
                    ->with('accion','editar')
                    ->with('superProduservicios',$superProduservicios)
                    ->with('produservicio',$produservicio);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            DB::connection()->getPdo()->beginTransaction();
            $produservicio = Produservicio::find($id);
            $produservicio->nombre = $request->nombre;
            $produservicio->super_produservicio_id = $request->super_produservicio_id;
            $produservicio->save();

            DB::connection()->getPdo()->commit();
        }catch(\Exception $e){
            DB::connection()->getPdo()->rollBack();
            return redirect('/admin/configuracion/produservicio')->with('error',"Error al editar esta produservicio, revisa los datos de ingreso; el sistema ha generado el siguiente error ".$e->getMessage());
        }

        return redirect('/admin/configuracion/produservicio')->with('mensaje',"Produservicio editada con éxito");
    }

    /**
     * Elimina un registro especifico de la base de datos
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::connection()->getPdo()->beginTransaction();
            $produservicio = Produservicio::find($id);
            $produservicio->delete();

            DB::connection()->getPdo()->commit();
        }catch(\Exception $e){
            DB::connection()->getPdo()->rollBack();
            return redirect('/admin/configuracion/produservicio')->with('error',"Error al eliminar esta produservicio, revisa los datos de ingreso; el sistema ha generado el siguiente error ".$e->getMessage());
        }

        return redirect('/admin/configuracion/produservicio')->with('mensaje',"Produservicio eliminada con éxito");        
    }

    public function buscar(Request $request,$formato = 'view')
    {
        if($formato=='view'){
            $produservicios = Produservicio::where('nombre','LIKE','%'.$request->palabraClave.'%')->paginate(10);
        return view('admin.configuracion.produservicios.inicio')
                    ->with('produservicios',$produservicios)
                    ->with('palabraClave',$request->palabraClave);
        }else{
            $produservicios = Produservicio::where('nombre','LIKE',$request->palabraClave.'%')->limit(100)->get();
            return $produservicios;
        }
    }
}
