<?php 

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lugar;
use App\Evento;
use App\Session;
use App\Funcionario;
use App\MultimediaLugar;

use View;
use DB;
use Redirect;
use Input;

class GaleriaController extends Controller
{

	/**
	 * Muestra la lista de registros
	 *
	 * @return Response
	 */

	public function index($idMunicipio)
	{
		$municipio = Lugar::find($idMunicipio);
		$fotos = $municipio->fotos;
		return View::make('admin.galeria.inicio')
						->with('fotos', $fotos)
						->with('municipio', $municipio);
	}


	/**
	 * Muestra el formulario para crear un nuevo registro.
	 *
	 * @return Response
	 */

	public function create($idMunicipio)
	{
		$foto = new MultimediaLugar;
		$municipio = Lugar::find($idMunicipio);
		$datos = [ 
				'municipio' => $municipio,
				'foto' => $foto,
				'accion' => 'crear'
			];
		return View::make('admin.galeria.formulario')->with($datos);
	}


	/**
	 * Guarda un nuevo registro en la base de datos
	 *
	 * @return Response
	 */

	public function store($idMunicipio)
	{
		$municipio = Lugar::find($idMunicipio);

		if(!$municipio)
			return Redirect::to('admin/municipio/'.$idMunicipio.'/galeria')->with('error',"Municipio no encontrado");

		$foto = new MultimediaLugar;
		$foto->tipo = 'Foto';
		$foto->ruta = $this->guardarImagen(Input::file('foto'),'municipios');
		$foto->lugar_id = $municipio->id;
		$foto->save();

		if($foto)
			return Redirect::to('admin/municipio/'.$municipio->id.'/galeria')->with('mensaje',"Foto creada con éxito");
		else
			return Redirect::to('admin/municipio/'.$municipio->id.'/galeria')->with('error',"Error al crear esta foto, revisa los datos de ingreso");
	}

	/**
	 * Elimina un registro especifico de la base de datos
	 *
	 * @param  int $id
	 * @return Response
	 */


	public function destroy($idMunicipio,$idSitio)
	{
		$municipio = Lugar::find($idMunicipio);
		$foto = MultimediaLugar::find($idSitio);

		if($foto->delete())
			return Redirect::to('admin/municipio/'.$municipio->id.'/galeria')->with('mensaje',"Foto eliminada con éxito");
		else
			return Redirect::to('admin/municipio/'.$municipio->id.'/galeria')->with('error',"Error al eliminar esta foto");
	}


}