<?php 

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lugar;
use App\Noticia;
use App\Fuente;
use App\Comentario;
use App\Entidad;

use View;
use DB;
use Input;
use Auth;
use Redirect;

class NoticiaController extends Controller
{

	/**
	 * Muestra la lista de registros
	 *
	 * @return Response
	 */

	public function index(Request $request)
	{
		$noticias = Noticia::orderBy('destacar', 'desc')->orderBy('id', 'desc');

		if($request->has('palabraClave'))
			$noticias = $noticias->where('titulo','LIKE','%'.$request->palabraClave.'%');

		$noticias = $noticias->paginate(8);

		return View::make('admin.noticias.inicio')->with('noticias', $noticias);
	}

	/**
	 * Muestra el formulario para crear un nuevo registro.
	 *
	 * @return Response
	 */
	public function create()
	{
		$municipios = Lugar::municipios()->orWhere('id','=','25')->get();
		$fuentes = Fuente::all();
		$noticia = new Noticia;
		return View::make('admin.noticias.formulario')
					->with('fuentes',$fuentes)
					->with('municipios',$municipios)
					->with('noticia',$noticia)
					->with('accion','crear');
	}


	/**
	 * Guarda un nuevo registro en la base de datos
	 *
	 * @return Response
	 */

	public function store()
	{

		$noticia = new Noticia;
		$noticia->titulo = Input::get('titulo');
		$noticia->descripcion = Input::get('descripcion');
		$noticia->contenido = Input::get('contenido');
		$noticia->f_publicacion = Input::get('f_publicacion');
		$noticia->f_caducidad = Input::get('f_caducidad');
		$noticia->miniatura = $this->guardarImagen(Input::file('miniatura'),'noticias');
		$noticia->lugar_id = Input::get('lugar_id');
		$noticia->fuente_id = 0;
		$noticia->otra_fuente = Input::get('otra_fuente');
		$noticia->usuario_id = Auth::user()->id;
		$noticia->save();

		if($noticia)
			return response()->json(['mensaje' => 'Noticia creada con éxito', 'id' => $noticia->id ]);
		else
			return response()->json(['error' => 'Error al crear esta noticia, revisa los datos de ingreso.']);
			
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */

	public function show($id)
	{
		return "noticia";
	}


	/**
	 * Muestra el formulario para editar un registro en especifico
	 *
	 * @param  int $id
	 * @return Response
	 */

	public function edit($id)
	{
		$municipios = Lugar::municipios()->orWhere('id','=','25')->get();
		$fuentes = Fuente::all();
		$noticia = Noticia::find($id);
		return View::make('admin.noticias.formulario')
					->with('fuentes',$fuentes)
					->with('municipios',$municipios)
					->with('noticia',$noticia)
					->with('accion','editar');
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @return Response
	 */

	public function update($id)
	{
		$noticia = Noticia::find($id);
		$noticia->titulo = Input::get('titulo');
		$noticia->descripcion = Input::get('descripcion');
		$noticia->otra_fuente = Input::get('otra_fuente');
		$noticia->contenido = Input::get('contenido');
		$noticia->f_publicacion = Input::get('f_publicacion');
		$noticia->f_caducidad = Input::get('f_caducidad');

		if (Input::hasFile('miniatura'))
		{
			$noticia->miniatura = $this->guardarImagen(Input::file('miniatura'),'noticias');
		}

		$noticia->lugar_id = Input::get('lugar_id');
		$noticia->fuente_id = 0;
		$noticia->save();

		if($noticia)
			return response()->json(['mensaje' => 'Noticia editado con éxito', 'id' => $noticia->id]);
		else
			return response()->json(['error' => 'Error al editar este noticia, revisa los datos de ingreso']);
	}


	/**
	 * Elimina un registro especifico de la base de datos
	 *
	 * @param  int $id
	 * @return Response
	 */


	public function destroy($id)
	{
		$noticia = Noticia::find($id);

		if($noticia->delete())
			return Redirect::to('admin/noticia')->with('mensaje',"Noticia eliminada correctamente");
		else
			return Redirect::to('admin/noticia')->with('error',"Error al eliminar esta noticia");
	}

	public function destacar($id)
	{
		$noticia = Noticia::find($id);
		$noticia->destacar = 1;

		if($noticia->save())
			return Redirect::to('admin/noticia')->with('mensaje',"Noticia destacada correctamente");
		else
			return Redirect::to('admin/noticia')->with('error',"Error al destacar esta noticia");
	}

	public function replegar($id)
	{
		$noticia = Noticia::find($id);
		$noticia->destacar = 0;

		if($noticia->save())
			return Redirect::to('admin/noticia')->with('mensaje',"Noticia replegada correctamente");
		else
			return Redirect::to('admin/noticia')->with('error',"Error al replegar esta noticia");
	}
}
