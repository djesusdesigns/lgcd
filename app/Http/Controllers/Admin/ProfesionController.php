<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Profesion;
use App\SuperProfesion;

use DB;

class ProfesionController extends Controller
{
    /**
     * Muestra la lista de registros
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profesiones = Profesion::paginate(10);
        return view('admin.configuracion.profesiones.inicio')
                    ->with('profesiones',$profesiones);
    }

    /**
     * Muestra el formulario para crear un nuevo registro.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $profesion = new Profesion;
        $superProfesiones = SuperProfesion::all();
        return view('admin.configuracion.profesiones.formulario')
                    ->with('accion','crear')
                    ->with('superProfesiones',$superProfesiones)
                    ->with('profesion',$profesion);
    }

    /**
     * Guarda un nuevo registro en la base de datos
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            DB::connection()->getPdo()->beginTransaction();
            $profesion = new Profesion;
            $profesion->nombre = $request->nombre;
            $profesion->super_profesion_id = $request->super_profesion_id;
            $profesion->save();

            DB::connection()->getPdo()->commit();
        }catch(\Exception $e){
            DB::connection()->getPdo()->rollBack();
            return redirect('/admin/configuracion/profesion')->with('error',"Error al crear esta profesion, revisa los datos de ingreso; el sistema ha generado el siguiente error ".$e->getMessage());
        }


        return redirect('/admin/configuracion/profesion')->with('mensaje',"Profesion creada con éxito");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Muestra el formulario para editar un registro en especifico
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $superProfesiones = SuperProfesion::all();
        $profesion = Profesion::find($id);
        return view('admin.configuracion.profesiones.formulario')
                    ->with('accion','editar')
                    ->with('superProfesiones',$superProfesiones)
                    ->with('profesion',$profesion);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            DB::connection()->getPdo()->beginTransaction();
            $profesion = Profesion::find($id);
            $profesion->nombre = $request->nombre;
            $profesion->super_profesion_id = $request->super_profesion_id;
            $profesion->save();

            DB::connection()->getPdo()->commit();
        }catch(\Exception $e){
            DB::connection()->getPdo()->rollBack();
            return redirect('/admin/configuracion/profesion')->with('error',"Error al editar esta profesion, revisa los datos de ingreso; el sistema ha generado el siguiente error ".$e->getMessage());
        }

        return redirect('/admin/configuracion/profesion')->with('mensaje',"Profesion editada con éxito");
    }

    /**
     * Elimina un registro especifico de la base de datos
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::connection()->getPdo()->beginTransaction();
            $profesion = Profesion::find($id);
            $profesion->delete();

            DB::connection()->getPdo()->commit();
        }catch(\Exception $e){
            DB::connection()->getPdo()->rollBack();
            return redirect('/admin/configuracion/profesion')->with('error',"Error al eliminar esta profesion, revisa los datos de ingreso; el sistema ha generado el siguiente error ".$e->getMessage());
        }

        return redirect('/admin/configuracion/profesion')->with('mensaje',"Profesion eliminada con éxito");        
    }

    public function buscar(Request $request,$formato = 'view')
    {
        
        if($formato=='view'){
            $profesiones = Profesion::where('nombre','LIKE','%'.$request->palabraClave.'%')->paginate(10);
            return view('admin.configuracion.profesiones.inicio')
                    ->with('profesiones',$profesiones)
                    ->with('palabraClave',$request->palabraClave);
        }else{
            $profesiones = Profesion::where('nombre','LIKE',$request->palabraClave.'%')->limit(100)->get();
            return $profesiones;
        }
    }
}
