<?php 

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Usuario;
use App\Lugar;
use App\Entidad;
use App\Sucursal;
use App\Actividad;
use App\Suscripcion;

use View;
use DB;
use Validator;
use Storage;
use Redirect;

class EntidadController extends Controller
{

	/**
	 * Muestra la lista de registros
	 *
	 * @return Response
	 */
	public function index()
	{
        $entidades = Entidad::where('entidades.tipo', 'Empresa')->get();

        $entidades->map(function($entidad, $key) use ($entidades){
            $usuario = $entidad->usuario;
            $entidad->suscripcion = $usuario->ultimaSuscripcion();
            return $entidad;
        });

        //return $entidades;
		return View::make('admin.entidades.inicio')->with('entidades', $entidades);
	}

	/**
	 * Muestra el formulario para crear un nuevo registro.
	 *
	 * @return Response
	 */
	public function create()
	{
		$entidad = new Entidad;
		$sucursal = new Sucursal;
		$horario = json_decode('[["",""],["",""]]');
		$telefonos = json_decode('[]');
        $actividades = Actividad::all();
        $municipios2 = Lugar::municipios()->get();
        $actividadesEmpresa = $entidad->actividades()->get()->pluck('id','nombre')->toArray();
        $actividades = Actividad::all()->pluck('id','nombre')->toArray();

		return View::make('admin.entidades.formulario')
					->with('entidad',$entidad)
					->with('sucursal',$sucursal)
					->with('horario',$horario)
					->with('telefonos',$telefonos)
                    ->with('actividades',$actividades)
                    ->with('actividadesEmpresa',$actividadesEmpresa)
                    ->with('municipios2',$municipios2)
					->with('accion','crear');
	}

	/**
	 * Guarda un nuevo registro en la base de datos
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        //return dd($request->all());
        //**param** categoria, correo_publico, direccion, domicilio, apellido, cargo, encargado_correo, nombre, telefono
        //**param** estacionamiento, hora01, hora02, hora11, hora12, latitud, longitud, municipio, n_empleados, nit, ofrece, requiere, sector, tecnologia, telefono_principal, web
        $validator = Validator::make($request->all(), [
        	'razon_social' => 'required|string|unique:entidades,razon_social',
            'correo' => 'required|email|unique:entidades,correo_privado|unique:usuarios,correo',
            'nit' => 'required|string',
            'lugar_id' => 'required'
        ]); 

        if (!$validator->fails()) {    

            try{
                DB::connection()->getPdo()->beginTransaction();

                $usuario = new Usuario();

                $usuario->correo = $request->correo;   
                $usuario->rol_id = 2;   
                $usuario->tipo = 1;   
                $usuario->contrasena = bcrypt('G2HwIkyO6mQA');   
                $usuario->estado = 1;
                $usuario->save();

                if($request->hasFile('logo')){
                    $carpeta_logo = 'usuario_'.$usuario->id.'/logo.'.$request->file('logo')->getClientOriginalExtension();
                    $logo = file_get_contents($request->file('logo')->getRealPath());
                    Storage::put($carpeta_logo, $logo);                 
                    $ruta_logo = '/file/'.$usuario->id.'/logo.'.$request->file('logo')->getClientOriginalExtension();
                }


                $horario = array();
                array_push( $horario, array( $request->hora01, $request->hora02 ) );
                array_push( $horario, array( $request->hora11, $request->hora12 ) );
                $horario = json_encode($horario);

                $telefonos = array();
                
                array_push( $telefonos, $request->telefono1 );
                array_push( $telefonos, $request->telefono2 );
                array_push( $telefonos, $request->telefono3 );
                array_push( $telefonos, $request->telefono4 );

                $telefonos = json_encode($telefonos);

				$empresa = new Entidad();
                $empresa->usuario_id = $usuario->id;
                $empresa->nombre = $request->razon_social;
                $empresa->razon_social = $request->razon_social;
                $empresa->correo_privado = $request->correo;
                $empresa->nit = $request->nit;
                $empresa->tipo = "Empresa";

                $empresa->tipo_negocio = $request->tipo_negocio;
                $empresa->web = $request->web;
                $empresa->descripcion = $request->descripcion;
                
                if(isset($ruta_logo))
                    $empresa->logo = $ruta_logo;

                $empresa->save();

                DB::table('entidades_actividades')->where('entidad_id',$empresa->id)->delete();

                foreach ($request->actividades as $key => $actividad) {
                    DB::table('entidades_actividades')->insert(['entidad_id' => $empresa->id, 'actividad_id' => $actividad]);
                }

                $sucursal = new Sucursal;

                $sucursal->entidad_id = $empresa->id;
                $sucursal->direccion = $request->direccion;
                $sucursal->telefonos = $telefonos;
                $sucursal->horario = $horario;
                $sucursal->lugar_id = $request->lugar_id;
                $sucursal->tipo = "Principal";

                $sucursal->save();

                if($request->hasFile('foto_sucursal')){
                    $carpeta_sucursal = 'usuario_'.$usuario->id.'/sucursal_'.$sucursal->id.'.'.$request->file('foto_sucursal')->getClientOriginalExtension();
                    $sucursal_image = file_get_contents($request->file('foto_sucursal')->getRealPath());
                    Storage::put($carpeta_sucursal, $sucursal_image);                 
                    $ruta_sucursal = '/file/'.$usuario->id.'/sucursal_'.$sucursal->id.'.'.$request->file('foto_sucursal')->getClientOriginalExtension();
                    $sucursal->imagen = $ruta_sucursal;
                }

                $sucursal->save();

                \Mail::send('mail.nueva-empresa', ['usuario' => $usuario->correo, 'contrasena' => 'G2HwIkyO6mQA'], function ($m)  use($usuario,$empresa){
                    $m->from('info@laguiacundinamarca.com', 'La Guía Cundinamarca');
                    $m->to($usuario->correo, $empresa->razon_social)->subject('Bienvenido a La Guía Cundinamarca');
                });
                
                DB::connection()->getPdo()->commit();
            }catch(\Exception $e){
                DB::connection()->getPdo()->rollBack();
                return Redirect::to('admin/entidad')->with('mensaje','Ha ocurrido el error, el sistema ha generado el siguiente error '.$e->getMessage());
            }

            return Redirect::to('admin/entidad')->with('mensaje',"Empresa creada con éxito");

        }else{
            return back()->withInput()->with('error', $validator->errors()->first());
        }
	}

	/**
	 * Muestra el formulario para editar un registro en especifico
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function edit($id)
	{
		$entidad = Entidad::find($id);
		$actividadesEmpresa = $entidad->actividades()->get()->pluck('id','nombre')->toArray();
        $sucursal = Sucursal::where('entidad_id',$entidad->id)->where('tipo','Principal')->first();
		
        if(!is_null($sucursal)){
            $horario = json_decode($sucursal->horario);
            $telefonos = json_decode($sucursal->telefonos);
        }else{
            $sucursal = new Sucursal;
            $horario = json_decode('[["",""],["",""]]');
            $telefonos = json_decode('[]');
        }

        $actividades = Actividad::all()->pluck('id','nombre')->toArray();
        $municipios2 = Lugar::municipios()->get();

		return View::make('admin.entidades.formulario')
					->with('entidad',$entidad)
                    ->with('municipios2',$municipios2)
					->with('sucursal',$sucursal)
					->with('horario',$horario)
					->with('telefonos',$telefonos)
                    ->with('actividades',$actividades)
                    ->with('actividadesEmpresa',$actividadesEmpresa)
					->with('accion','editar');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
        //**param** categoria, correo_publico, direccion, domicilio, apellido, cargo, encargado_correo, nombre, telefono
        //**param** estacionamiento, hora01, hora02, hora11, hora12, latitud, longitud, municipio, n_empleados, nit, ofrece, requiere, sector, tecnologia, telefono_principal, web
        
        $validator = Validator::make($request->all(), [
            'razon_social' => 'required|string',
            'correo' => 'required|email',
            'nit' => 'required|string',
            'municipio' => 'required'
        ]); 

        if (!$validator->fails()) {    

            $reporte = DB::transaction(function () use($request,$id) {

            	$empresa = Entidad::find($id);
                $usuario = $empresa->usuario;

                $usuario->correo = $request->correo;   
                $usuario->rol_id = 2;   
                $usuario->tipo = 1; 
                $usuario->estado = 1;

                $usuario->save();

                if($request->hasFile('logo')){
                	$carpeta_logo = 'usuario_'.$usuario->id.'/logo.'.$request->file('logo')->getClientOriginalExtension();
	                $logo = file_get_contents($request->file('logo')->getRealPath());
	                Storage::put($carpeta_logo, $logo);                 
	                $ruta_logo = '/file/'.$usuario->id.'/logo.'.$request->file('logo')->getClientOriginalExtension();	
	                $empresa->logo = $ruta_logo;
                }
                

                DB::table('entidades_actividades')->where('entidad_id',$empresa->id)->delete();

                foreach ($request->actividades as $key => $actividad) {
                    DB::table('entidades_actividades')->insert(['entidad_id' => $empresa->id, 'actividad_id' => $actividad]);
                }
                	

                $empresa->nombre = $request->razon_social;
                $empresa->razon_social = $request->razon_social;
                $empresa->correo_privado = $request->correo;
                $empresa->tipo_negocio = (sizeof($request->actividades[0])) ? $request->actividades[0] : 0;
                $empresa->web = $request->web;
                $empresa->descripcion = $request->descripcion;
                $empresa->requiere = $request->requiere;
                $empresa->ofrece = $request->ofrece;
                $empresa->nit = $request->nit;
                $empresa->save();

                $sucursal = $empresa->sucursal_principal();

                $horario = array();
                array_push( $horario, array( $request->hora01, $request->hora02 ) );
                array_push( $horario, array( $request->hora11, $request->hora12 ) );
                $horario = json_encode($horario);

                $telefonos = array();
                if($request->has('telefono1') )
                    array_push( $telefonos, $request->telefono1 );

                if($request->has('telefono2') )
                    array_push( $telefonos, $request->telefono2 );

                if($request->has('telefono3') )
                    array_push( $telefonos, $request->telefono3 );

                if($request->has('telefono4') )
                    array_push( $telefonos, $request->telefono4 );


                $telefonos = json_encode($telefonos);

                $sucursal->entidad_id = $empresa->id;
                $sucursal->direccion = $request->direccion;
                $sucursal->telefonos = $telefonos;
                $sucursal->horario = $horario;
                $sucursal->lugar_id = $request->municipio;
                $sucursal->latitud = Lugar::find($request->municipio)->latitud;
                $sucursal->longitud = Lugar::find($request->municipio)->longitud;

                $sucursal->save();

                if($request->hasFile('foto_sucursal')){
	                $carpeta_sucursal = 'usuario_'.$usuario->id.'/sucursal_'.$sucursal->id.'.'.$request->file('foto_sucursal')->getClientOriginalExtension();
                    $sucursal_image = file_get_contents($request->file('foto_sucursal')->getRealPath());
                    Storage::put($carpeta_sucursal, $sucursal_image);                 
                    $ruta_sucursal = '/file/'.$usuario->id.'/sucursal_'.$sucursal->id.'.'.$request->file('foto_sucursal')->getClientOriginalExtension();
                    $sucursal->imagen = $ruta_sucursal;
            	}

                $sucursal->save();
                
            });

            return Redirect::to('admin/entidad')->with('mensaje',"Empresa editada con éxito");

        }else{
            return back()->withInput()->with('error', $validator->errors()->first());
        }
	}	

	/**
	 * Elimina un registro especifico de la base de datos
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function destroy($id)
	{
		// delete
		$entidad = Entidad::find($id);

        DB::table('entidades_actividades')->where('entidad_id',$entidad->id)->delete();
        DB::table('sucursales')->where('entidad_id',$entidad->id)->delete();
        DB::table('trabajos')->where('entidad_id',$entidad->id)->delete();
        DB::table('usuarios')->where('id',$entidad->usuario_id)->delete();
        DB::table('eventos')->where('id',$entidad->usuario_id)->delete();
        DB::table('comentarios')->where('id',$entidad->usuario_id)->delete();
		
		if($entidad->delete())
			return Redirect::to('admin/entidad')->with('mensaje',"Empresa eliminado con éxito");
		else
			return Redirect::to('admin/entidad')->with('error',"Error al eliminar esta empresa, revisa los datos de ingreso");
	}

    public function mostrar_suscripcion(Request $request, $id){
        $entidad = Entidad::find($id);
        $usuario = null;
        $suscripciones = collect([]);
        $tipo = 'municipio';
        $lugar_id = ($request->has("id"))? $request->id:null;
        if($entidad){
            $usuario = $entidad->usuario_id;
            $tipo = $entidad->tipo;
            $suscripciones = Suscripcion::where("usuario_id", $usuario)
                ->select("suscripciones.id", "suscripciones.valor", DB::raw("date_format(suscripciones.f_inicio, '%d de %b de %Y') as f_inicio"), DB::raw("date_format(suscripciones.f_fin, '%d de %b de %Y') as f_fin"))
                ->orderBy("suscripciones.f_fin")
                ->get();
        }
        
        return View::make('admin.entidades.suscripcion')
            ->with('suscripciones', $suscripciones)
            ->with('tipo', $tipo)
            ->with('usuario_id', $usuario)
            ->with('lugar_id', $lugar_id);
    }


    public function suscribir(Request $request){

        $validator = Validator::make($request->all(), [
            'usuario' => 'required|numeric',
            'correo' =>'required_if:usuario,0',
            'lugar' =>'required_if:usuario,0|exists:lugares,id',
            'valor' => 'required|numeric|min:0',
            'fecha_inicial' => 'required|date_format:Y-m-d',
            'fecha_final' => 'required|date_format:Y-m-d|after:fecha_inicial'          
        ]);

        $validator->after(function($validator) use($request){
            if($request->usuario != 0){
                $inicio = Suscripcion::where("f_inicio",'<',$request->fecha_inicial." 00:00:00")
                    ->where("f_fin",'>',$request->fecha_inicial." 00:00:00")
                    ->where("usuario_id",$request->usuario)
                    ->first();
                $fin = Suscripcion::where("f_inicio",'<',$request->fecha_final." 23:59:59")
                    ->where("f_fin",'>',$request->fecha_final." 23:59:59")
                    ->where("usuario_id",$request->usuario)
                    ->first();
                if($inicio || $fin){
                    $validator->errors()->add('fecha', 'Ya se cuenta con una suscripción activa en ese rango de fechas.');
                }
            }
        });

        if (!$validator->fails()) {

                if($request->has("lugar") && $request->usuario == 0){

                    try{
                        DB::connection()->getPdo()->beginTransaction();

                        $lugar = Lugar::find($request->lugar);

                        $usuario = new Usuario();

                        $usuario->correo = $request->correo;   
                        $usuario->rol_id = 3;   
                        $usuario->tipo = 1;   
                        $usuario->contrasena = bcrypt('G2HwIkyO6mQA'); 
                        $usuario->estado = 1;

                        $usuario->save();

                        $municipio = new Entidad();
                        $municipio->usuario_id = $usuario->id;
                        $municipio->nombre = $lugar->nombre;
                        $municipio->razon_social = $lugar->nombre;
                        $municipio->correo_privado = $request->correo;
                        $municipio->tipo = "Municipio";

                        $municipio->save();

                        $horario = 

                        $sucursal = new Sucursal;

                        $sucursal->entidad_id = $municipio->id;
                        $sucursal->direccion = '';
                        $sucursal->telefonos = json_encode([]);
                        $sucursal->lugar_id = $lugar->id;
                        $sucursal->tipo = "Principal";
                        $sucursal->domicilio = 0;
                        $sucursal->estacionamiento = 0;
                        $sucursal->horario = json_encode([['00:00', '00:00'],['00:00', '00:00']]);
                        $sucursal->latitud = $lugar->latitud;
                        $sucursal->longitud = $lugar->longitud;
                        $sucursal->vehiculos = 0;

                        $sucursal->save();

                        $suscripcion = new Suscripcion();

                        $suscripcion->usuario_id = $usuario->id;
                        $suscripcion->valor = $request->valor;
                        $suscripcion->f_inicio = $request->fecha_inicial." 00:00:00";
                        $suscripcion->f_fin = $request->fecha_final." 23:59:59";

                        $suscripcion->save();

                        $today = time();

                        if($today > strtotime($request->fecha_inicial." 00:00:00") && $today < strtotime($request->fecha_final." 23:59:59")){
                            $usuario->tipo = 2;
                            $usuario->save();
                        }

                        if($request->enviar_correo){
                            \Mail::send('mail.nuevo-municipio-premium', ['usuario' => $usuario->correo, 'contrasena' => 'G2HwIkyO6mQA'],
                                function ($m) use($usuario,$municipio){
                                    $m->from('info@laguiacundinamarca.com', 'La Guía Cundinamarca');
                                    $m->to($usuario->correo, $municipio->razon_social)->subject('Bienvenido a La Guía Cundinamarca');
                                }
                            );
                        }

                        DB::connection()->getPdo()->commit();
                        return redirect("/admin/entidad/".$municipio->id."/suscripcion?tipo=municipio&id=".$lugar->id);

                    }catch(\Exception $e){
                        DB::connection()->getPdo()->rollBack();
                        return redirect()->back()->with('error', 'Error al registrar este suscripcion, revise los datos de ingreso. El error generado por el sistema es '.$e->getMessage() );
                    }

                }else{
                    $suscripcion = new Suscripcion();

                    $suscripcion->usuario_id = $request->usuario;
                    $suscripcion->valor = $request->valor;
                    $suscripcion->f_inicio = $request->fecha_inicial." 00:00:00";
                    $suscripcion->f_fin = $request->fecha_final." 23:59:59";

                    $suscripcion->save();

                    $today = time();

                    if($today > strtotime($request->fecha_inicial." 00:00:00") && $today < strtotime($request->fecha_final." 23:59:59")){
                        $usuario = Usuario::find($request->usuario);
                        $usuario->tipo = 2;

                        $usuario->save();
                    }
                    return redirect()->back();
                }            

        }else{
            return redirect()->back()->with('error', $validator->errors()->first());
        }
    }

    public function estadisticas($id)
    {
        $entidad = Entidad::find($id);
        return View::make('admin.entidades.estadisticas')
                        ->with('entidad',$entidad);
    }

}
