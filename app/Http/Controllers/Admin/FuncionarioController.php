<?php 

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lugar;
use App\Evento;
use App\Session;
use App\Funcionario;

use View;
use DB;
use Redirect;
use Input;

class FuncionarioController extends Controller
{

	/**
	 * Muestra la lista de registros
	 *
	 * @return Response
	 */

	public function index($idMunicipio)
	{
		$municipio = Lugar::find($idMunicipio);
		$funcionarios = $municipio->funcionarios()->paginate(10);
		return View::make('admin.funcionarios.inicio')
						->with('funcionarios', $funcionarios)
						->with('municipio', $municipio);
	}


	/**
	 * Muestra el formulario para crear un nuevo registro.
	 *
	 * @return Response
	 */

	public function create($idMunicipio)
	{
		$funcionario = new Funcionario;
		$municipio = Lugar::find($idMunicipio);
		return View::make('admin.funcionarios.formulario')
					->with('municipio',$municipio)
					->with('funcionario',$funcionario)
					->with('accion','crear');
	}


	/**
	 * Guarda un nuevo registro en la base de datos
	 *
	 * @return Response
	 */

	public function store($idMunicipio)
	{
		$municipio = Lugar::find($idMunicipio);

		if(!$municipio)
			return Redirect::to('admin/municipio/'.$idMunicipio.'/funcionario')->with('error',"Municipio no encontrado");

		$funcionario = new Funcionario;
		$funcionario->cargo = Input::get('cargo');
		$funcionario->nombre = Input::get('nombre');
		$funcionario->genero = Input::get('genero');
		$funcionario->foto = $this->guardarImagen(Input::file('foto'),'funcionarios');
		$funcionario->email = Input::get('email');
		$funcionario->dependencia_id = Input::get('dependencia_id');
		$funcionario->save();


		if($funcionario)
			return Redirect::to('admin/municipio/'.$municipio->id.'/funcionario')->with('mensaje',"Funcionario creado con éxito");
		else
			return Redirect::to('admin/municipio/'.$municipio->id.'/funcionario')->with('error',"Error al crear este funcionario, revisa los datos de ingreso");
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */

	public function show($id)
	{
		return "funcionario";
	}


	/**
	 * Muestra el formulario para editar un registro en especifico
	 *
	 * @param  int $id
	 * @return Response
	 */

	public function edit($idMunicipio,$idFuncionario)
	{
		$funcionario = Funcionario::find($idFuncionario);
		$municipio = Lugar::find($idMunicipio);
		return View::make('admin.funcionarios.formulario')
					->with('municipio',$municipio)
					->with('funcionario',$funcionario)
					->with('accion','editar');
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @return Response
	 */

	public function update($idMunicipio,$idFuncionario)
	{
		$municipio = Lugar::find($idMunicipio);

		if(!$municipio)
			return Redirect::to('admin/municipio/'.$idMunicipio.'/funcionario')->with('error',"Municipio no encontrado");

		$funcionario = Funcionario::find($idFuncionario);
		$funcionario->cargo = Input::get('cargo');
		$funcionario->nombre = Input::get('nombre');
		$funcionario->genero = Input::get('genero');

		if (Input::hasFile('foto'))
		{
			$funcionario->foto = $this->guardarImagen(Input::file('foto'),'funcionarios');
		}

		$funcionario->email = Input::get('email');
		$funcionario->dependencia_id = Input::get('dependencia_id');
		$funcionario->save();


		if($funcionario)
			return Redirect::to('admin/municipio/'.$municipio->id.'/funcionario')->with('mensaje',"Funcionario creado con éxito");
		else
			return Redirect::to('admin/municipio/'.$municipio->id.'/funcionario')->with('error',"Error al crear este funcionario, revisa los datos de ingreso");
	}


	/**
	 * Elimina un registro especifico de la base de datos
	 *
	 * @param  int $id
	 * @return Response
	 */


	public function destroy($idMunicipio,$idFuncionario)
	{
		$municipio = Lugar::find($idMunicipio);
		$funcionario = Funcionario::find($idFuncionario);

		if($funcionario->delete())
			return Redirect::to('admin/municipio/'.$municipio->id.'/funcionario')->with('mensaje',"Funcionario eliminado con éxito");
		else
			return Redirect::to('admin/municipio/'.$municipio->id.'/funcionario')->with('error',"Error al eliminar este funcionario");
	}


}