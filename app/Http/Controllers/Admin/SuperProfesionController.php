<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\SuperProfesion;

use DB;

class SuperProfesionController extends Controller
{
    /**
     * Muestra la lista de registros
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $superProfesiones = superProfesion::orderBy('nombre')->paginate(10);
        return view('admin.configuracion.superProfesiones.inicio')
                    ->with('superProfesiones',$superProfesiones);
    }

    /**
     * Muestra el formulario para crear un nuevo registro.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $superProfesion = new superProfesion;
        return view('admin.configuracion.superProfesiones.formulario')
                    ->with('accion','crear')
                    ->with('superProfesion',$superProfesion);
    }

    /**
     * Guarda un nuevo registro en la base de datos
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            DB::connection()->getPdo()->beginTransaction();
            $superProfesion = new superProfesion;
            $superProfesion->nombre = $request->nombre;
            $superProfesion->save();

            DB::connection()->getPdo()->commit();
        }catch(\Exception $e){
            DB::connection()->getPdo()->rollBack();
            return redirect('/admin/configuracion/superProfesion')->with('error',"Error al crear esta superProfesion, revisa los datos de ingreso; el sistema ha generado el siguiente error ".$e->getMessage());
        }


        return redirect('/admin/configuracion/superProfesion')->with('mensaje',"superProfesion creada con éxito");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Muestra el formulario para editar un registro en especifico
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $superProfesion = superProfesion::find($id);
        return view('admin.configuracion.superProfesiones.formulario')
                    ->with('accion','editar')
                    ->with('superProfesion',$superProfesion);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            DB::connection()->getPdo()->beginTransaction();
            $superProfesion = superProfesion::find($id);
            $superProfesion->nombre = $request->nombre;
            $superProfesion->save();

            DB::connection()->getPdo()->commit();
        }catch(\Exception $e){
            DB::connection()->getPdo()->rollBack();
            return redirect('/admin/configuracion/superProfesion')->with('error',"Error al editar esta superProfesion, revisa los datos de ingreso; el sistema ha generado el siguiente error ".$e->getMessage());
        }

        return redirect('/admin/configuracion/superProfesion')->with('mensaje',"superProfesion editada con éxito");
    }

    /**
     * Elimina un registro especifico de la base de datos
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::connection()->getPdo()->beginTransaction();
            $superProfesion = superProfesion::find($id);
            $superProfesion->delete();

            DB::connection()->getPdo()->commit();
        }catch(\Exception $e){
            DB::connection()->getPdo()->rollBack();
            return redirect('/admin/configuracion/superProfesion')->with('error',"Error al eliminar esta superProfesion, revisa los datos de ingreso; el sistema ha generado el siguiente error ".$e->getMessage());
        }

        return redirect('/admin/configuracion/superProfesion')->with('mensaje',"superProfesion eliminada con éxito");        
    }

    public function buscar(Request $request)
    {
        $superProfesiones = superProfesion::where('nombre','LIKE','%'.$request->palabraClave.'%')->paginate(10);
        return view('admin.configuracion.superProfesiones.inicio')
                    ->with('superProfesiones',$superProfesiones)
                    ->with('palabraClave',$request->palabraClave);
    }
}
