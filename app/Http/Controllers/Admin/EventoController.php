<?php 

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lugar;
use App\Evento;
use App\Comentario;
use App\Entidad;

use View;
use Redirect;
use Input;
use Auth;

class EventoController extends Controller
{

	/**
	 * Muestra la lista de registros
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$eventos = Evento::orderBy('destacar', 'desc')->orderBy('id', 'desc');

		if($request->has('palabraClave'))
			$eventos = $eventos->where('titulo','LIKE','%'.$request->palabraClave.'%');

		$eventos = $eventos->paginate(8);

		return View::make('admin.eventos.inicio')->with('eventos', $eventos);
	}

	/**
	 * Muestra el formulario para crear un nuevo registro.
	 *
	 * @return Response
	 */
	public function create()
	{
		$evento = new Evento;
		return View::make('admin.eventos.formulario')
					->with('evento',$evento)
					->with('accion','crear');
	}

	/**
	 * Guarda un nuevo registro en la base de datos
	 *
	 * @return Response
	 */
	public function store()
	{
		$evento = new Evento;
		$evento->titulo = Input::get('titulo');
		$evento->contenido = Input::get('contenido');
		$evento->f_realizacion = Input::get('f_realizacion');
		$evento->f_caducidad = Input::get('f_caducidad');
		$evento->horario = Input::get('horario');
		$evento->f_finalizacion = Input::get('f_finalizacion');
		$evento->lugar_evento = Input::get('lugar_evento');
		$evento->telefonos_info = Input::get('telefonos_info');
		$evento->correo_info = Input::get('correo_info');
		$evento->f_publicacion = Input::get('f_publicacion');
		$evento->miniatura = $this->guardarImagen(Input::file('miniatura'),'eventos');
		$evento->lugar_id = Input::get('lugar_id');
		$evento->usuario_id = Auth::user()->id;
		$evento->save();

		if($evento)
			return Redirect::to('admin/evento')->with('mensaje',"Evento creado con éxito");
		else
			return Redirect::to('admin/evento')->with('error',"Error al crear este evento, revisa los datos de ingreso");

	}

	/**
	 * Muestra el formulario para editar un registro en especifico
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function edit($id)
	{
		$municipios = Lugar::municipios()->orWhere('id','=','25')->get();
		$evento = Evento::find($id);
		return View::make('admin.eventos.formulario')
					->with('municipios',$municipios)
					->with('evento',$evento)
					->with('accion','editar');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function update($id)
	{
		$evento = Evento::find($id);
		$evento->titulo = Input::get('titulo');
		$evento->contenido = Input::get('contenido');
		$evento->f_realizacion = Input::get('f_realizacion');
		$evento->f_caducidad = Input::get('f_caducidad');
		$evento->f_finalizacion = Input::get('f_finalizacion');
		$evento->horario = Input::get('horario');
		$evento->lugar_evento = Input::get('lugar_evento');
		$evento->telefonos_info = Input::get('telefonos_info');
		$evento->correo_info = Input::get('correo_info');
		$evento->f_publicacion = Input::get('f_publicacion');
		
		if (Input::hasFile('miniatura'))
		{
			$evento->miniatura = $this->guardarImagen(Input::file('miniatura'),'eventos');
		}
		
		$evento->lugar_id = Input::get('lugar_id');
		$evento->save();

		if($evento)
			return Redirect::to('admin/evento')->with('mensaje',"Evento editado con éxito");
		else
			return Redirect::to('admin/evento')->with('error',"Error al editar este evento, revisa los datos de ingreso");

	}

	/**
	 * Elimina un registro especifico de la base de datos
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function destroy($id)
	{
		// delete
		$evento = Evento::find($id);
		$evento->delete();

		if($evento)
			return Redirect::to('admin/evento')->with('mensaje',"Evento eliminado con éxito");
		else
			return Redirect::to('admin/evento')->with('error',"Error al eliminar este evento, revisa los datos de ingreso");
	}

	public function destacar($id)
	{
		$evento = Evento::find($id);
		$evento->destacar = 1;

		if($evento->save())
			return Redirect::to('admin/evento')->with('mensaje',"Evento destacado correctamente");
		else
			return Redirect::to('admin/evento')->with('error',"Error al destacar esta noticia");
	}

	public function replegar($id)
	{
		$evento = Evento::find($id);
		$evento->destacar = 0;

		if($evento->save())
			return Redirect::to('admin/evento')->with('mensaje',"Evento replegado correctamente");
		else
			return Redirect::to('admin/evento')->with('error',"Error al replegar esta noticia");
	}
}
