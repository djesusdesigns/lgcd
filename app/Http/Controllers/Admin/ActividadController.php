<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Actividad;

use DB;

class ActividadController extends Controller
{
    /**
     * Muestra la lista de actividades
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $actividades = Actividad::orderBy('nombre')->paginate(10);
        return view('admin.configuracion.actividades.inicio')
                    ->with('actividades',$actividades);
    }

    /**
     * Muestra el formulario para crear un nuevo registro.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $actividad = new Actividad;
        return view('admin.configuracion.actividades.formulario')
                    ->with('accion','crear')
                    ->with('actividad',$actividad);
    }

    /**
     * Guarda un nuevo registro en la base de datos
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            DB::connection()->getPdo()->beginTransaction();
            $actividad = new Actividad;
            $actividad->nombre = $request->nombre;
            $actividad->save();

            DB::connection()->getPdo()->commit();
        }catch(\Exception $e){
            DB::connection()->getPdo()->rollBack();
            return redirect('/admin/configuracion/actividad')->with('error',"Error al crear esta actividad, revisa los datos de ingreso; el sistema ha generado el siguiente error ".$e->getMessage());
        }


        return redirect('/admin/configuracion/actividad')->with('mensaje',"Actividad creada con éxito");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Muestra el formulario para editar un registro en especifico
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $actividad = Actividad::find($id);
        return view('admin.configuracion.actividades.formulario')
                    ->with('accion','editar')
                    ->with('actividad',$actividad);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            DB::connection()->getPdo()->beginTransaction();
            $actividad = Actividad::find($id);
            $actividad->nombre = $request->nombre;
            $actividad->save();

            DB::connection()->getPdo()->commit();
        }catch(\Exception $e){
            DB::connection()->getPdo()->rollBack();
            return redirect('/admin/configuracion/actividad')->with('error',"Error al editar esta actividad, revisa los datos de ingreso; el sistema ha generado el siguiente error ".$e->getMessage());
        }

        return redirect('/admin/configuracion/actividad')->with('mensaje',"Actividad editada con éxito");
    }

    /**
     * Elimina un registro especifico de la base de datos
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::connection()->getPdo()->beginTransaction();
            $actividad = Actividad::find($id);
            $actividad->delete();

            DB::connection()->getPdo()->commit();
        }catch(\Exception $e){
            DB::connection()->getPdo()->rollBack();
            return redirect('/admin/configuracion/actividad')->with('error',"Error al eliminar esta actividad, revisa los datos de ingreso; el sistema ha generado el siguiente error ".$e->getMessage());
        }

        return redirect('/admin/configuracion/actividad')->with('mensaje',"Actividad eliminada con éxito");        
    }

    public function buscar(Request $request)
    {
        $actividades = Actividad::where('nombre','LIKE','%'.$request->palabraClave.'%')->paginate(10);
        return view('admin.configuracion.actividades.inicio')
                    ->with('actividades',$actividades)
                    ->with('palabraClave',$request->palabraClave);
    }
}
