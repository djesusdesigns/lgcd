<?php  
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Comentario;

use View;
use Redirect;

class ComentarioController extends Controller
{

	/**
	 * Muestra la lista de registros
	 *
	 * @return Response
	 */
	public function index()
	{
		$comentarios = Comentario::orderBy('created_at', 'desc')->take(100)->get();
		return View::make('admin.comentarios.inicio')->with('comentarios', $comentarios);
	}

	/**
	 * Elimina un registro especifico de la base de datos
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function destroy($id)
	{
		// delete
		$comentario = Comentario::find($id);

		if( $comentario->respuestas() ){
			if($comentario->delete() && $comentario->respuestas())
				return Redirect::to('admin/comentario')->with('mensaje',"Comentario eliminado con éxito");
			else
				return Redirect::to('admin/comentario')->with('error',"Error al eliminar este comentario y sus respuestas");
		}else{
			if($comentario->delete())
				return Redirect::to('admin/comentario')->with('mensaje',"Comentario eliminado con éxito");
			else
				return Redirect::to('admin/comentario')->with('error',"Error al eliminar este comentario");
		}

		
	}

}