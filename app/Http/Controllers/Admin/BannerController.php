<?php  

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lugar;
use App\Noticia;
use App\Banner;

use View;
use DB;
use Input;
use Auth;
use Redirect;

class BannerController extends Controller
{

	public $tipos = [
		'Banner superior' => 'superior',
		'Banner lateral derecho' => 'lateralDer',
		'Banner carta incrustada' => 'carta',
		'Banner inferior' => 'inferior',
		'Banner móvil inferior' => 'm-inferior',
		'Banner móvil superior' => 'm-superior',
		'Banner móvil carta incrustada' => 'm-carta',
	];


	public $secciones = ['todas','inicio','sitioInteres','municipios','noticias','eventos','amarillas','empleos'];

	/**
	 * Muestra la lista de registros
	 *
	 * @return Response
	 */
	public function index()
	{
		$banners = Banner::all();
		return View::make('admin.banners.inicio')->with('banners', $banners);
	}


	/**
	 * Muestra el formulario para crear un nuevo registro.
	 *
	 * @return Response
	 */
	public function create()
	{
		$banner = new Banner;
		return View::make('admin.banners.formulario')
					->with('banner',$banner)
					->with('tipos',$this->tipos)
					->with('secciones',$this->secciones)
					->with('accion','crear');
	}

	/**
	 * Guarda un nuevo registro en la base de datos
	 *
	 * @return Response
	 */
	public function store()
	{
		try{
			$banner = new Banner;
			$banner->tipo = Input::get('tipo');
			$banner->seccion = Input::get('seccion');
			$banner->lugar_id = Input::get('lugar_id');
			$banner->imagen = $this->guardarImagen(Input::file('imagen'),'banners');
			$banner->website = Input::get('website');
			$banner->save();
			return Redirect::to('admin/banner')->with('mensaje',"Banner creado con éxito");
		}catch (Exception $e) {
			return Redirect::to('admin/banner')->with('error',"Error al crear este banner, revisa los datos de ingreso");
		}
	}

	/**
	 * Muestra el formulario para editar un registro en especifico
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function edit($id)
	{
		$banner = Banner::find($id);
		return View::make('admin.banners.formulario')
					->with('banner',$banner)
					->with('tipos',$this->tipos)
					->with('secciones',$this->secciones)
					->with('accion','editar');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function update($id)
	{
		try{
			$banner = Banner::find($id);
			$banner->tipo = Input::get('tipo');
			$banner->seccion = Input::get('seccion');
			$banner->lugar_id = Input::get('lugar_id');
			$banner->website = Input::get('website');
			
			if (Input::hasFile('imagen'))
			{
				$banner->imagen = $this->guardarImagen(Input::file('imagen'),'banners');
			}

			$banner->save();
			return Redirect::to('admin/banner')->with('mensaje',"Banner editado con éxito");
		}catch (Exception $e) {
			return Redirect::to('admin/banner')->with('error',"Error al editar este Banner, revisa los datos de ingreso");
		}

	}

	/**
	 * Elimina un registro especifico de la base de datos
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function destroy($id)
	{
		try{
			$banner = Banner::find($id);
			$banner->delete();
			return Redirect::to('admin/banner')->with('mensaje',"Banner eliminado con éxito");
		}catch (Exception $e) {
			return Redirect::to('admin/banner')->with('error',"Error al eliminar este Banner");
		}
	}

	public function getBanners()
	{
		$section = Input::get('section','inicio');
		$type = Input::get('type','superior');
		$ramdom = Input::get('ramdom',0);
		$limit = Input::get('limit',0);
		$banners = \App\Banner::getAll($type,$section,$ramdom,$limit);
		return response()->json([ 'banners' => $banners ]);
	}

}
