<?php  
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lugar;
use App\Dependencia;

use Input;
use Redirect;
use View;

class DependenciaController extends Controller
{

	/**
	 * Muestra la lista de registros
	 *
	 * @return Response
	 */

	public function index($idMunicipio)
	{
		$municipio = Lugar::find($idMunicipio);
		$dependencias = $municipio->dependencias()->get();
		return View::make('admin.dependencias.inicio')
						->with('dependencias', $dependencias)
						->with('municipio', $municipio);
	}


	/**
	 * Muestra el formulario para crear un nuevo registro.
	 *
	 * @return Response
	 */

	public function create($idMunicipio)
	{
		$dependencia = new Dependencia;
		return View::make('admin.dependencias.formulario')
					->with('idMunicipio',$idMunicipio)
					->with('dependencia',$dependencia)
					->with('accion','crear');
	}


	/**
	 * Guarda un nuevo registro en la base de datos
	 *
	 * @return Response
	 */

	public function store($idMunicipio)
	{
		$municipio = Lugar::find($idMunicipio);

		if(!$municipio)
			return Redirect::to('admin/municipio/'.$idMunicipio.'/dependencia')->with('error',"Municipio no encontrado");

		$dependencia = new Dependencia;
		$dependencia->nombre = Input::get('nombre');
		$dependencia->direccion = Input::get('direccion');
		$dependencia->telefonos = Input::get('telefonos');
		$dependencia->web = Input::get('web');
		$dependencia->email = Input::get('email');
		$dependencia->lugar_id = $municipio->id;
		$dependencia->dependencia_id = Input::get('dependencia_id',0);
		$dependencia->save();


		if($dependencia)
			return Redirect::to('admin/municipio/'.$municipio->id.'/dependencia')->with('mensaje',"Dependencia creada con éxito");
		else
			return Redirect::to('admin/municipio/'.$municipio->id.'/dependencia')->with('error',"Error al crear esta dependencia, revisa los datos de ingreso");
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */

	public function show($id)
	{
		return "dependencia";
	}


	/**
	 * Muestra el formulario para editar un registro en especifico
	 *
	 * @param  int $id
	 * @return Response
	 */

	public function edit($idMunicipio,$idDependencia)
	{
		$dependencia = Dependencia::find($idDependencia);
		return View::make('admin.dependencias.formulario')
					->with('idMunicipio',$idMunicipio)
					->with('dependencia',$dependencia)
					->with('accion','editar');
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @return Response
	 */

	public function update($idMunicipio,$idDependencia)
	{
		$municipio = Lugar::find($idMunicipio);

		if(!$municipio)
			return Redirect::to('admin/municipio/'.$idMunicipio.'/dependencia')->with('error',"Municipio no encontrado");

		$dependencia = Dependencia::find($idDependencia);
		$dependencia->nombre = Input::get('nombre');
		$dependencia->direccion = Input::get('direccion');
		$dependencia->telefonos = Input::get('telefonos');
		$dependencia->web = Input::get('web');
		$dependencia->email = Input::get('email');
		$dependencia->lugar_id = $municipio->id;
		$dependencia->dependencia_id = Input::get('dependencia_id',0);
		$dependencia->save();


		if($dependencia)
			return Redirect::to('admin/municipio/'.$municipio->id.'/dependencia')->with('mensaje',"Dependencia creada con éxito");
		else
			return Redirect::to('admin/municipio/'.$municipio->id.'/dependencia')->with('error',"Error al crear esta dependencia, revisa los datos de ingreso");
	}


	/**
	 * Elimina un registro especifico de la base de datos
	 *
	 * @param  int $id
	 * @return Response
	 */


	public function destroy($idMunicipio,$idDependencia)
	{
		$municipio = Lugar::find($idMunicipio);
		$dependencia = Dependencia::find($idDependencia);

		if($dependencia->delete())
			return Redirect::to('admin/municipio/'.$municipio->id.'/dependencia')->with('mensaje',"Dependencia eliminada con éxito");
		else
			return Redirect::to('admin/municipio/'.$municipio->id.'/dependencia')->with('error',"Error al eliminar esta dependencia");
	}


}