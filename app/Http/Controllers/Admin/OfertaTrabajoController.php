<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\OfertaTrabajo;
use App\Entidad;
use App\Lugar;
use App\Actividad;
use App\TipoContrato;
use App\TipoEducacion;

use View;
use DB;
use Redirect;

class OfertaTrabajoController extends Controller
{

    private $tiposContrato;
    private $tiposEducacion;
    private $categorias;

    public function __construct()
    {
        if(\Auth::check())
            $this->middleware('auth');
        else
            $this->middleware('guest');

        $this->tiposContrato = TipoContrato::all();
        $this->tiposEducacion = TipoEducacion::all();
        $this->categorias = Actividad::all();
    }

    /*
     * Muestra la lista de registros
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $empleos = OfertaTrabajo::take(500)->get();
        return view('admin.empleos.inicio')->with('empleos',$empleos);
    }

    /**
     * Muestra el formulario para crear un nuevo registro.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empleo = new OfertaTrabajo;
        $empresas = Entidad::where('entidades.tipo', 'Empresa')->get();
        return View::make('admin.empleos.formulario')
                    ->with('empleo',$empleo)
                    ->with('empresas',$empresas)
                    ->with('tiposContrato',$this->tiposContrato)
                    ->with('tiposEducacion',$this->tiposEducacion)
                    ->with('actividades',$this->categorias)
                    ->with('accion','crear');
    }

    /**
     * Guarda un nuevo registro en la base de datos
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reporte = DB::transaction(function () use($request) {
            $oferta = $this->guardar_actualizar($request);
            $oferta->save();
        });

        return Redirect::to('admin/empleo')->with('mensaje',"Empleo creado con éxito");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $oferta = Trabajo::find($id);
        return $oferta->id;
    }

    /**
     * Muestra el formulario para editar un registro en especifico
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empleo = OfertaTrabajo::find($id);
        $empresas = Entidad::where('entidades.tipo', 'Empresa')->get();
        return View::make('admin.empleos.formulario')
                    ->with('empleo',$empleo)
                    ->with('empresas',$empresas)
                    ->with('tiposContrato',$this->tiposContrato)
                    ->with('tiposEducacion',$this->tiposEducacion)
                    ->with('actividades',$this->categorias)
                    ->with('accion','editar');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //return $request->all();
        $reporte = DB::transaction(function () use($request,$id) {
            $oferta = $this->guardar_actualizar($request,$id);
            $oferta->save();
        });

        return Redirect::to('admin/empleo')->with('mensaje',"Empleo editado con éxito");
    }

    /**
     * Elimina un registro especifico de la base de datos
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $oferta = OfertaTrabajo::find($id);
        $oferta->delete();
        if($oferta)
            return Redirect::to('admin/empleo')->with('mensaje',"Empleo eliminado correctamente");
        else
            return Redirect::to('admin/empleo')->with('error',"Error al eliminar este empleo");
    }

    private function guardar_actualizar($request,$id=0)
    {
        $oferta = ($id > 0) ?  OfertaTrabajo::find($id) : new OfertaTrabajo;
        $oferta->entidad_id = $request->entidad_id;
        $oferta->categoria_id = $request->categoria_id;
        $oferta->tipo_contrato_id = $request->tipo_contrato_id;
        $oferta->experiencia = $request->experiencia;
        $oferta->lugar_id = $request->lugar_id;
        $oferta->tipo_educacion_id = $request->tipo_educacion_id;
        $oferta->edad_min = $request->edad_min;
        $oferta->edad_max = $request->edad_max;
        $oferta->titulo = $request->titulo;
        $oferta->descripcion = $request->descripcion;
        $oferta->requerimientos = $request->requerimientos;
        $oferta->salario = $request->salario;
        $oferta->estado = "activa";
        $oferta->horario_laboral = $request->horario_laboral;
        $oferta->vacantes = $request->vacantes;
        $oferta->f_vencimiento = $request->f_vencimiento;
        $oferta->disp_viaje = $request->disp_viaje ? "Si" : "No";
        $oferta->disp_cambio_hogar = $request->disp_cambio_hogar ? "Si" : "No";
        return $oferta;
    }


    public function destacar($id)
    {
        $empleo = OfertaTrabajo::find($id);
        $empleo->destacar = 1;

        if($empleo->save())
            return Redirect::to('admin/empleo')->with('mensaje',"Empleo destacado correctamente");
        else
            return Redirect::to('admin/empleo')->with('error',"Error al destacar este empleo");
    }

    public function replegar($id)
    {
        $empleo = OfertaTrabajo::find($id);
        $empleo->destacar = 0;

        if($empleo->save())
            return Redirect::to('admin/empleo')->with('mensaje',"Empleo replegado correctamente");
        else
            return Redirect::to('admin/empleo')->with('error',"Error al replegar este empleo");
    }
}
