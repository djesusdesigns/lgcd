<?php 

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lugar;
use App\Tramite;
use App\Fuente;
use App\Comentario;
use App\Entidad;

use View;
use DB;
use Input;
use Redirect;

class TramiteController extends Controller
{

	public $tipos = ['Directorio de tramites','Servicios Sociales','Educación','Salud','Cultura, Deportes y Recreación','Espacio público y transporte','Medio Ambiente','Empresas Economia y trabajo','Vivienda'];
	/**
	 * Muestra la lista de registros
	 *
	 * @return Response
	 */

	public function index()
	{
		$tramites = Tramite::orderBy('created_at', 'desc')->take(100)->get();
		$tramitesDesc = Tramite::where('destacar','=',1)->get();
		return View::make('admin.tramites.inicio')
								->with('tramites',$tramites)
								->with('tramitesDesc',$tramitesDesc);
	}

	/**
	 * Muestra el formulario para crear un nuevo registro.
	 *
	 * @return Response
	 */

	public function create()
	{
		$tramite = new Tramite;
		return View::make('admin.tramites.formulario')
					->with('tramite',$tramite)
					->with('tipos',$this->tipos)
					->with('accion','crear');
	}


	/**
	 * Guarda un nuevo registro en la base de datos
	 *
	 * @return Response
	 */

	public function store()
	{
		try{
			$tramite = new Tramite;
			$tramite->nombre = Input::get('nombre');
			$tramite->descripcion = Input::get('descripcion');
			$tramite->miniatura = $this->guardarImagen(Input::file('miniatura'),'tramites');
			$tramite->lugar_id = Input::get('lugar_id');
			$tramite->tipo = Input::get('tipo');
			$tramite->website = Input::get('website');
			$tramite->save();
			return Redirect::to('admin/tramite')->with('mensaje',"Tramite creado con éxito");
		}catch (Exception $e) {
			return Redirect::to('admin/tramite')->with('error',"Error al crear este tramite, revisa los datos de ingreso");
		}
			
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */

	public function show($id)
	{
		return "tramite";
	}


	/**
	 * Muestra el formulario para editar un registro en especifico
	 *
	 * @param  int $id
	 * @return Response
	 */

	public function edit($id)
	{
		$fuentes = Fuente::all();
		$tramite = Tramite::find($id);
		return View::make('admin.tramites.formulario')
					->with('fuentes',$fuentes)
					->with('tipos',$this->tipos)
					->with('tramite',$tramite)
					->with('accion','editar');
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @return Response
	 */

	public function update($id)

	{
		$tramite = Tramite::find($id);
		$tramite->nombre = Input::get('nombre');
		$tramite->descripcion = Input::get('descripcion');
		$tramite->tipo = Input::get('tipo');
		$tramite->website = Input::get('website');

		if (Input::hasFile('miniatura'))
		{
			$tramite->miniatura = $this->guardarImagen(Input::file('miniatura'),'tramites');
		}

		$tramite->lugar_id = Input::get('lugar_id');

		if($tramite->save())
			return Redirect::to('admin/tramite')->with('mensaje',"Tramite editado con éxito");
		else
			return Redirect::to('admin/tramite')->with('error',"Error al editar este tramite, revisa los datos de ingreso");

	}


	/**
	 * Elimina un registro especifico de la base de datos
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$tramite = Tramite::find($id);

		if($tramite->delete())
			return Redirect::to('admin/tramite')->with('mensaje',"Tramite eliminada correctamente");
		else
			return Redirect::to('admin/tramite')->with('error',"Error al eliminar esta tramite");
	}
}
