<?php  

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Lugar;
use App\DatosLugar;
use App\Dependencia;
use App\Noticia;
use App\Evento;
use App\Tramite;
use App\Comentario;
use App\Banner;
use App\Entidad;

use View;
use Input;

class InicioController extends Controller{


	public function inicio()
	{

		$numNoticias = Noticia::publicadas()->count();
		$numEventos = Evento::activos()->count();
		$numEmpresas = Entidad::where('tipo','Empresa')->count();
		$numTramites = Tramite::count();
		$numComentarios = Comentario::where('created_at','>',date('Y-m-d',strtotime('-5 day')))->count();
		$numBanners = Banner::count();
		return View::make('admin.inicio')
				->with('usuario',\Auth::user())
				->with('numNoticias',$numNoticias)
				->with('numEventos',$numEventos)
				->with('numEmpresas',$numEmpresas)
				->with('numTramites',$numTramites)
				->with('numComentarios',$numComentarios)
				->with('numBanners',$numBanners);
	}

	public function inicioConf()
	{
		return View::make('admin.configuracion.inicio');
	}

	public function subirImagen()
	{
		return asset( $this->guardarImagen(Input::file('imagen'),Input::get('seccion')) );
	}

}