<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lugar;
use App\Columna;
use App\Comentario;

use View;
use DB;

class ColumnaController extends Controller
{

	public function indexGuest(Request $request)
	{

		$columnas = Columna::publicadas()->orderBy('id', 'desc');

		if( sizeof($request->all()) == 0){
			$request->session()->forget('lugar');
			$request->session()->forget('archivo');
		}

		if($request->has('archivo') || session('archivo')){
			$archivo = $request->has('archivo') ? $request->archivo : session('archivo');
			$request->session()->put('archivo',$archivo);

			$inicioMes = $request->archivo."-01";
			$finMes = $request->archivo."-".date('t',strtotime($request->archivo));
			$columnas = $columnas->whereBetween('f_publicacion',[$inicioMes,$finMes]);
		}

		$columnas = $columnas->paginate(3);

		foreach ($columnas as $key => $noti) {
			$evt = $this->completarDatosNoticia($noti);
		}

		if (request()->is('api/*')) {
			return response()
            ->json($columnas);
		}

		if(isset($municipio)){
			return View::make('columnas.index')
					->with('columnas', $columnas)
					->with('busquedaLugar', $municipio)
					->with('aniosColumna', $this->aniosColumna() );
		}else{
			return View::make('columnas.index')
					->with('columnas', $columnas)
					->with('aniosColumna', $this->aniosColumna() );	
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */

	public function show($id)
	{
		$columna = Columna::find($id);

		if(!$columna)
			return redirect('noticias');
		
		$ultimasColumna = Columna::take(3)
                            ->orderBy('id', 'desc')
                            ->get();		
		
		$comentarios = $columnas->comentarios;
		//$aniosNoticias = $this->aniosNoticias();

		return View::make('columnas.show')
					   ->with('columnas', $columnas)
					   ->with('ultimasColumnas', $ultimasColumnas)
					   ->with('comentarios', $comentarios);

		
	}	

	public function aniosColumnas()
	{
		$aniosColumna = Columna::publicadas()
							->select(DB::raw('YEAR(f_publicacion) as anio'))
							->orderBy('f_publicacion', 'desc')
							->groupBy('anio')
							->having('anio','>',2000)
							->get();

		return $aniosColumna;
	}

	public function busquedaPorMunicipio(Request $request,$slugMunicipio)
	{
		$slugArray = explode("-",$slugMunicipio);
		$idMunicipio = $slugArray[sizeof($slugArray)-1];
		$request->merge(['lugar' => $idMunicipio]);
		return $this->indexGuest($request);
	}
}
