<?php  
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Entidad;
use App\Noticia;
use App\Evento;
use App\SitioInteres;

use Redirect;
use Input;
use DB;
use View;

class BuscadorController extends Controller
{

	/**
	 * Muestra la lista de registros
	 *
	 * @return Response
	 */
	public function redireccionar()
	{  
        $municipio = request()->input('municipio');
        $municipio = ($municipio) ? $municipio : 0;
		return Redirect::to( 'busqueda/'.Input::get('seccion').'/'.$municipio."/".urlencode(Input::get('search')) );
	}


	public function busqueda(Request $request,$tipo='todos_los_resultados',$idMunicipio, $search=""){
        $search = urldecode($search);

        $noticias = Noticia::where('titulo','LIKE','%'.$search.'%')
                        ->select(DB::raw('id,"noticia" as tipo,noticias.titulo as nombre,miniatura,descripcion as contenido,f_publicacion as fecha'))
                        ->orderBy('f_publicacion','desc');

        $eventos = Evento::where('titulo','LIKE','%'.$search.'%')
                        ->select(DB::raw('id,"evento" as tipo,eventos.titulo as nombre,miniatura,contenido as contenido,f_realizacion as fecha'))
                        ->orderBy('f_realizacion','desc');

        $sitio_interes = SitioInteres::where('nombre','LIKE','%'.$search.'%')
                        ->select(DB::raw('id,"sitio_interes" as tipo,sitio_interes.nombre as nombre,foto as miniatura,descripcion as contenido,"'.date('-1 week').'" as fecha'))
                        ->orderBy('fecha','desc');


        if($idMunicipio && $idMunicipio != 25){
            $noticias = $noticias->where('lugar_id','=',$idMunicipio);
            $eventos = $eventos->where('lugar_id','=',$idMunicipio);
            $sitio_interes = $sitio_interes->where('lugar_id','=',$idMunicipio);
        }


        if($tipo=='inicio'){
            $query = $noticias
                        ->unionAll($eventos)
                        ->unionAll($sitio_interes);
        }elseif($tipo == 'noticias'){
            $query = $noticias;
        }elseif($tipo == 'eventos'){
            $query = $eventos;
        }elseif($tipo == 'sitio_interes'){
            $query = $sitio_interes;
        }

        $resultados = $query->orderBy('fecha','desc')->simplePaginate(8);

        return View::make('busquedaInicio')
                    ->with('tipo',$tipo)
                    ->with('idMunicipio',$idMunicipio)
                    ->with('search',$search)
                    ->with('resultados',$resultados);
    }

    public function newSearch(Request $request){

        $text = $request->search;

        

    }

}
