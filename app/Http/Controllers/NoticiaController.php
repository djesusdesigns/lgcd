<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lugar;
use App\Noticia;
use App\Comentario;

use View;
use DB;

class NoticiaController extends Controller
{

	public function indexGuest(Request $request)
	{

		$noticias = Noticia::publicadas()->orderBy('id', 'desc');

		if( sizeof($request->all()) == 0){
			$request->session()->forget('lugar');
			$request->session()->forget('archivo');
		}

		if($request->has('archivo') || session('archivo')){
			$archivo = $request->has('archivo') ? $request->archivo : session('archivo');
			$request->session()->put('archivo',$archivo);

			$inicioMes = $request->archivo."-01";
			$finMes = $request->archivo."-".date('t',strtotime($request->archivo));
			$noticias = $noticias->whereBetween('f_publicacion',[$inicioMes,$finMes]);
		}

		$noticias = $noticias->paginate(8);

		foreach ($noticias as $key => $noti) {
			$evt = $this->completarDatosNoticia($noti);
		}

		if (request()->is('api/*')) {
			return response()
            ->json($noticias);
		}

		if(isset($municipio)){
			return View::make('noticias.index')
					->with('noticias', $noticias)
					->with('busquedaLugar', $municipio)
					->with('aniosNoticias', $this->aniosNoticias() );
		}else{
			return View::make('noticias.index')
					->with('noticias', $noticias)
					->with('aniosNoticias', $this->aniosNoticias() );	
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */

	public function show($id)
	{
		$noticia = Noticia::find($id);

		if(!$noticia)
			return redirect('noticias');
		
		$ultimasNoticias = Noticia::take(3)
                            ->orderBy('id', 'desc')
                            ->get();		
		
		$comentarios = $noticia->comentarios;
		//$aniosNoticias = $this->aniosNoticias();

		return View::make('noticias.show')
					   ->with('noticia', $noticia)
					   ->with('ultimasNoticias', $ultimasNoticias)
					   ->with('comentarios', $comentarios);

		
	}	

	public function aniosNoticias()
	{
		$aniosNoticias = Noticia::publicadas()
							->select(DB::raw('YEAR(f_publicacion) as anio'))
							->orderBy('f_publicacion', 'desc')
							->groupBy('anio')
							->having('anio','>',2000)
							->get();

		return $aniosNoticias;
	}

	public function busquedaPorMunicipio(Request $request,$slugMunicipio)
	{
		$slugArray = explode("-",$slugMunicipio);
		$idMunicipio = $slugArray[sizeof($slugArray)-1];
		$request->merge(['lugar' => $idMunicipio]);
		return $this->indexGuest($request);
	}
}
