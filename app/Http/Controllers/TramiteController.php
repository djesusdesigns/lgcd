<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Tramite;
use App\Comentario;

use View;

class TramiteController extends Controller
{

	public $tipos = ['Directorio de tramites','Servicios Sociales','Educación','Salud','Cultura, Deportes y Recreación','Espacio público y transporte','Medio Ambiente','Empresas Economia y trabajo','Vivienda'];
	

	public function indexGuest(Request $request)
	{
		$tramites = Tramite::orderBy('created_at', 'desc');		

		if( sizeof($request->all()) == 0){
			$request->session()->forget('lugar');
			$request->session()->forget('tipo');
		}

		if($request->has('borrar-lugar')){
			$request->session()->forget('lugar');
		}

		if($request->has('borrar-tipo')){
			$request->session()->forget('tipo');
		}

		if($request->has('lugar') || session('lugar') || session('lugarPremium')){
			$lugarId = $request->has('lugar') ? $request->lugar : session('lugar')['id'];
			$lugarId = session('lugarPremium') ? session('lugarPremium')['lugar'] : $lugarId;
			
			$municipio = Lugar::find($lugarId);

			$request->session()->put('lugar',[
				'id'=>$municipio->id,
				'nombre'=>$municipio->nombre
			]);

			$tramites = $tramites->where('lugar_id','=',session('lugar'));
	        $this->verificarTema($municipio->id);
		}

		if($request->has('tipo') || session('tipo')){
			$tipo = $request->has('tipo') ? $request->tipo : session('tipo');
			$request->session()->put('tipo',$tipo);

			$tramites = $tramites->where(function ($query) use ($tipo) {
				                $query->orWhere('nombre','LIKE','%'.$tipo.'%')
				                      ->orWhere('tipo','LIKE','%'.$tipo.'%');
				            });
		}

		$tramites = $tramites->paginate(4);

		//return $tramites->toSql();

		if(isset($municipio)){
			return View::make('tramites.index')
					->with('busquedaLugar', $municipio)
					->with('tramites',$tramites)
					->with('tipos',$this->tipos);
		}else{
			return View::make('tramites.index')
					->with('tramites',$tramites)
					->with('tipos',$this->tipos);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */

	public function show($id)
	{
		$tramite = Tramite::find($id);
		$comentarios = Comentario::seccion('Tramite',$tramite->id)->get();
		return View::make('tramites.show')
					  	->with('tramite', $tramite)
					  	->with('tipos',$this->tipos)
						->with('comentarios', $comentarios);
	}

	public function busquedaPorMunicipio(Request $request,$slugMunicipio)
	{
		$slugArray = explode("-",$slugMunicipio);
		$idMunicipio = $slugArray[sizeof($slugArray)-1];
		$request->merge(['lugar' => $idMunicipio]);
		return $this->indexGuest($request);
	}
}
