<?php 

namespace App\Http\Controllers\PanelUsuarios;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lugar;
use App\SitioInteresUsuario;
use App\Entidad;

use Input;
use Auth;
use Validator;
use Illuminate\Support\Facades\Log;

class SitioInteresController extends Controller
{

	public function index(Request $request){
		return view('panel-usuarios.sitios-interes');
	}

	public function create(Request $request){
	 $sitioInteresUsuario = new SitioInteresUsuario;
		return view('panel-usuarios.form-sitios-interes')
		 ->with('sitioInteres',$sitioInteresUsuario);
	}

	 public function edit(Request $request,$id){
	 	$sitioInteresUsuario = SitioInteresUsuario::find($id);

	 	if(!$sitioInteresUsuario)
			abort(404);

	 	return view('panel-usuarios.form-SitioInteresUsuarios')
	 	->with('sitioInteres',$sitioInteresUsuario);
	 }

	 public function store(Request $request)
	 {
	 	try{
	 		\DB::connection()->getPdo()->beginTransaction();
            
            $validator = Validator::make($request->all(), [
	            'nombre' => 'required|string',
				'contenido' => 'required',
	            'miniatura' => 'required|max:8500',
	            'lugar_id' => 'required',
	        ]);

	        if ($validator->fails())
	 			return response()->json(['errors' => $validator->errors()->first() ]);

         	$usuario = Auth::user();

      		$sitioInteres = new SitioInteresUsuario;
			$sitioInteres->nombre = $request->input('nombre');
	 		$sitioInteres->descripcion = strip_tags(str_limit(Input::get('contenido'),300));
			$sitioInteres->contenido = Input::get('contenido');
	 		$sitioInteres->miniatura = $this->guardarImagen(Input::file('miniatura'),'sitio_interes_usuario');
	 		$sitioInteres->lugar_id = Input::get('lugar_id');
	 		$sitioInteres->usuario_id = $usuario->id;
	 		$sitioInteres->save();

	 		$dataMail = [
                'nombre' => $sitioInteres->nombre,
                'contenido' => $sitioInteres->contenido,
                'miniatura' => $sitioInteres->miniatura,
                'lugar' => $sitioInteres->lugar->nombre,
                'nombre_usuario' =>$sitioInteres->usuario->nombre." ".$sitioInteres->usuario->apellido,
                'correo_usuario' => $sitioInteres->usuario->correo
            ];

            \Mail::send('mail.sitios-interes-datos', $dataMail, function ($m) use ($request){
	            $m->from("info@laguiacundinamarca.com");
	            $m->to("info@laguiacundinamarca.com")->subject("Nuevo sitio de interés");
	        });

	        $dataMailMensaje = [
                'nombre_usuario' =>$sitioInteres->usuario->nombre." ".$sitioInteres->usuario->apellido
            ];
            log::critical($sitioInteres->usuario->correo);
	        \Mail::send('mail.sitios-interes-mensaje-usuario', $dataMailMensaje, function ($m) use ($request,$sitioInteres){
	            $m->from("info@laguiacundinamarca.com");
	            $m->to($sitioInteres->usuario->correo)->subject("Nuevo sitio de interés");
	        });

	        \DB::connection()->getPdo()->commit();

 		return response()->json(['mensaje' => 'SitioInteres creado con éxito', 'id' => $sitioInteres->id ]);
	 	}catch(\Exception $e){
	 		Log::critical('Error: ' . $e->getCode() . ', ' . $e->getLine() . ', ' . $e->getMessage());
            \DB::connection()->getPdo()->rollBack();
	 		return response()->json(['error' => 'Error al crear este SitioInteresUsuario, revisa los datos de ingreso.']);
        }

	}

	public function update(Request $request,$id)
	{
		$validator = Validator::make($request->all(), [
            'nombre' => 'required|string',
			'contenido' => 'required',
            'miniatura' => 'max:8500',
            'lugar_id' => 'required',
        ]);

        if ($validator->fails()){
			if($request->is('api/*'))
				return response()->json(['errors' => $validator->errors()->first() ]);
			else
				return response()->json(['errors' => $validator->errors()->first() ]);
		}	

		$sitioInteresUsuario = SitioInteresUsuario::find($id);
		$sitioInteresUsuario->nombre = Input::get('nombre');
		$sitioInteresUsuario->descripcion = strip_tags(str_limit(Input::get('contenido'),300));
		$sitioInteresUsuario->contenido = Input::get('contenido');

		if (Input::hasFile('miniatura'))
		{
			$sitioInteresUsuario->miniatura = $this->guardarImagen(Input::file('imagen_sitio_interes_usuario'),'sitio_interes_usuario');
		}

		
		$sitioInteresUsuario->lugar_id = Input::get('lugar_id');
		$sitioInteresUsuario->usuario_id = Auth::user()->id;
		$sitioInteresUsuario->save();

		if($sitioInteresUsuario)
			return response()->json(['mensaje' => 'SitioInteresUsuario editado con éxito', 'id' => $sitioInteresUsuario->id]);
		else
			return response()->json(['error' => 'Error al editar este Sitio interes, revisa los datos de ingreso']);
	}

	public function destroy($id)
	{
		// delete
		$sitioInteresUsuario = SitioInteresUsuario::find($id);
		$sitioInteresUsuario->delete();

		if($sitioInteresUsuario)
			return response()->json(['mensaje' => 'SitioInteresUsuario eliminado con éxito']);
		else
			return response()->json(['error' => 'Error al eliminar este Sitio de interes, revisa los datos de ingreso']);
	}

}
