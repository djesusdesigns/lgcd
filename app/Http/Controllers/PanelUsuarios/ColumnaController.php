<?php 

namespace App\Http\Controllers\PanelUsuarios;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lugar;
use App\Columna;
use App\Entidad;

use Input;
use Auth;
use Validator;

class ColumnaController extends Controller
{

	public function index(Request $request){
		return view('panel-usuarios.columnas');
	}

	public function create(Request $request){
		$columna = new Columna;
		return view('panel-usuarios.form-columnas')
		->with('columna',$columna);
	}

	public function edit(Request $request,$id){
		$columna = Columna::find($id);

		if(!$columna)
			abort(404);

		return view('panel-usuarios.form-columnas')
		->with('columna',$columna);
	}

	public function store(Request $request)
	{
		try{
			\DB::connection()->getPdo()->beginTransaction();
            
			$validator = Validator::make($request->all(), [
	            'titulo' => 'required|string',
				'contenido' => 'required',
	            'f_publicacion' => 'required',
	            'f_caducidad' => 'required',
	            'miniatura' => 'required|max:8500'
	        ]);

	        if ($validator->fails())
				return response()->json(['errors' => $validator->errors()->first() ]);

	        $usuario = Auth::user();

	        $columna = new Columna;
			$columna->titulo = Input::get('titulo');
			$columna->descripcion = strip_tags(str_limit(Input::get('contenido'),300));
			$columna->contenido = Input::get('contenido');
			$columna->f_publicacion = Input::get('f_publicacion');
			$columna->f_caducidad = Input::get('f_caducidad');
			$columna->lugar_id = Input::get('lugar_id');
			$columna->fuente_id = 0;
			//$columna->otra_fuente = is_null($request->otra_fuente) ? $usuario->nombre_completo : $request->otra_fuente;
			$columna->usuario_id = $usuario->id;
			$columna->save();

			\DB::connection()->getPdo()->commit();

			return response()->json(['mensaje' => 'Columna creada con éxito', 'id' => $columna->id ]);
		}catch(\Exception $e){
            \DB::connection()->getPdo()->rollBack();
			return response()->json(['error' => 'Error al crear esta columna, revisa los datos de ingreso.']);
        }

	}

	public function update(Request $request,$id)
	{
		$validator = Validator::make($request->all(), [
            'titulo' => 'required|string',
			'contenido' => 'required',
            'f_publicacion' => 'required',
            'f_caducidad' => 'required',
            'miniatura' => 'max:8500',
            'lugar_id' => 'required',
        ]);

        if ($validator->fails()){
			if($request->is('api/*'))
				return response()->json(['errors' => $validator->errors()->first() ]);
			else
				return response()->json(['errors' => $validator->errors()->first() ]);
		}	

		$columna = Columna::find($id);
		$columna->titulo = Input::get('titulo');
		$columna->descripcion = strip_tags(str_limit(Input::get('contenido'),300));
		$columna->contenido = Input::get('contenido');
		$columna->f_publicacion = Input::get('f_publicacion');
		$columna->f_caducidad = Input::get('f_caducidad');

		if (Input::hasFile('miniatura'))
		{
			$columna->miniatura = $this->guardarImagen(Input::file('miniatura'),'columnas');
		}

		
		$columna->lugar_id = Input::get('lugar_id');
		$columna->fuente_id = 0;
		//$columna->otra_fuente = (Input::get('otra_fuente')!='')?Input::get('otra_fuente'):'no';
		$columna->usuario_id = Auth::user()->id;
		$columna->save();

		if($columna)
			return response()->json(['mensaje' => 'Columna editado con éxito', 'id' => $columna->id]);
		else
			return response()->json(['error' => 'Error al editar este columna, revisa los datos de ingreso']);
	}

	public function destroy($id)
	{
		// delete
		$columna = Columna::find($id);
		$columna->delete();

		if($columna)
			return response()->json(['mensaje' => 'Noticia eliminada con éxito']);
		else
			return response()->json(['error' => 'Error al eliminar este noticia, revisa los datos de ingreso']);
	}

}
