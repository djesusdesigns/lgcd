<?php 

namespace App\Http\Controllers\PanelUsuarios;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lugar;
use App\Noticia;
use App\Entidad;

use Input;
use Auth;
use Validator;

class NoticiaController extends Controller
{

	public function index(Request $request){
		return view('panel-usuarios.noticias');
	}

	public function create(Request $request){
		$noticia = new Noticia;
		return view('panel-usuarios.form-noticias')
		->with('noti',$noticia);
	}

	public function edit(Request $request,$id){
		$noticia = Noticia::find($id);

		if(!$noticia)
			abort(404);

		return view('panel-usuarios.form-noticias')
		->with('noti',$noticia);
	}

	public function store(Request $request)
	{
		try{
			\DB::connection()->getPdo()->beginTransaction();
            
			$validator = Validator::make($request->all(), [
	            'titulo' => 'required|string',
				'contenido' => 'required',
	            'f_publicacion' => 'required',
	            'f_caducidad' => 'required',
	            'miniatura' => 'required|max:8500',
	            'lugar_id' => 'required',
	        ]);

	        if ($validator->fails())
				return response()->json(['errors' => $validator->errors()->first() ]);

	        $usuario = Auth::user();

	        $noticia = new Noticia;
			$noticia->titulo = Input::get('titulo');
			$noticia->descripcion = strip_tags(str_limit(Input::get('contenido'),300));
			$noticia->contenido = Input::get('contenido');
			$noticia->f_publicacion = Input::get('f_publicacion');
			$noticia->f_caducidad = Input::get('f_caducidad');
			$noticia->miniatura = $this->guardarImagen(Input::file('miniatura'),'noticias');
			$noticia->lugar_id = Input::get('lugar_id');
			$noticia->fuente_id = 0;
			$noticia->otra_fuente = is_null($request->otra_fuente) ? $usuario->nombre_completo : $request->otra_fuente;
			$noticia->usuario_id = $usuario->id;
			$noticia->save();

			\DB::connection()->getPdo()->commit();

			return response()->json(['mensaje' => 'Noticia creada con éxito', 'id' => $noticia->id ]);
		}catch(\Exception $e){
            \DB::connection()->getPdo()->rollBack();
			return response()->json(['error' => 'Error al crear esta noticia, revisa los datos de ingreso.']);
        }

	}

	public function update(Request $request,$id)
	{
		$validator = Validator::make($request->all(), [
            'titulo' => 'required|string',
			'contenido' => 'required',
            'f_publicacion' => 'required',
            'f_caducidad' => 'required',
            'miniatura' => 'max:8500',
            'lugar_id' => 'required',
        ]);

        if ($validator->fails()){
			if($request->is('api/*'))
				return response()->json(['errors' => $validator->errors()->first() ]);
			else
				return response()->json(['errors' => $validator->errors()->first() ]);
		}	

		$noticia = Noticia::find($id);
		$noticia->titulo = Input::get('titulo');
		$noticia->descripcion = strip_tags(str_limit(Input::get('contenido'),300));
		$noticia->contenido = Input::get('contenido');
		$noticia->f_publicacion = Input::get('f_publicacion');
		$noticia->f_caducidad = Input::get('f_caducidad');

		if (Input::hasFile('miniatura'))
		{
			$noticia->miniatura = $this->guardarImagen(Input::file('imagen_noticia'),'noticias');
		}

		
		$noticia->lugar_id = Input::get('lugar_id');
		$noticia->fuente_id = 0;
		$noticia->otra_fuente = Input::get('otra_fuente');
		$noticia->usuario_id = Auth::user()->id;
		$noticia->save();

		if($noticia)
			return response()->json(['mensaje' => 'Noticia editado con éxito', 'id' => $noticia->id]);
		else
			return response()->json(['error' => 'Error al editar este noticia, revisa los datos de ingreso']);
	}

	public function destroy($id)
	{
		// delete
		$noticia = Noticia::find($id);
		$noticia->delete();

		if($noticia)
			return response()->json(['mensaje' => 'Noticia eliminada con éxito']);
		else
			return response()->json(['error' => 'Error al eliminar este noticia, revisa los datos de ingreso']);
	}

}
