<?php

namespace App\Http\Controllers\PanelUsuarios;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Entidad;
use App\Archivo;

use Storage;
use Auth;
use Validator;

class ArchivoController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function cargar(Request $request){
        //**params tipo=Imagen|Video|Audio|Documento, archivo, link
        $usuario_sesion = Auth::user();
        $entidad = Entidad::where('usuario_id', $usuario_sesion->id)->first();

        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string|max:50',
            'tipo' => 'required|in:Imagen,Video,Audio,Documento',
            'archivo' => 'required_if:tipo,Imagen,tipo,Audio,tipo,Documento|max:10000',
            'link' => 'required_if:tipo,Video|url'
        ]);

        $validator->after(function($validator) use($request){
            $mime = ($request->hasFile('archivo'))?$request->archivo->getMimeType():null;
            $tipo = $request->tipo;

            if($tipo=='Imagen' && $mime!="image/png" && $mime!="image/jpeg" && $mime!="image/gif"){
                $validator->errors()->add('archivo', $mime.': El archivo no es una imagen, por favor seleccione un archivo de tipo imagen.');
            }
            if($tipo == 'Audio' && $mime!="audio/mpeg" && $mime!="audio/mpeg3" && $mime!="audio/x-mpeg-3"){
                $validator->errors()->add('archivo', $mime.': Por el momento no es posible cargar archivos de audio, inténtelo más tarde');
            }
            if($tipo=='Documento' 
                && $mime!="application/pdf"
                && $mime!="application/msword"
                && $mime!="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                && $mime!="application/vnd.openxmlformats-officedocument.wordprocessingml.document"
            ){
                $validator->errors()->add('archivo', $mime );
            }
        });


        if (!$validator->fails()) {

            $archivo = new Archivo();
            $archivo->entidad_id = $entidad->id;
            $archivo->tipo = $request->tipo;
            $archivo->nombre = $request->nombre;
            $archivo->ruta = ($request->has("link"))? $request->link:"";

            

            if($request->tipo != "Video"){
                $carpeta = 'usuario_'.$usuario_sesion->id.'/archivo_'.$archivo->id.'.'.$request->file('archivo')->getClientOriginalExtension();
                $contenido = file_get_contents($request->file('archivo')->getRealPath());
                Storage::put($carpeta, $contenido);                 
                $ruta = '/file/'.$usuario_sesion->id.'/archivo_'.$archivo->id.'.'.$request->file('archivo')->getClientOriginalExtension();
                $archivo->ruta = $ruta;
            }else{
                $pattern =
                '%^# Match any youtube URL
                (?:https?://)?  # Optional scheme. Either http or https
                (?:www\.)?      # Optional www subdomain
                (?:             # Group host alternatives
                  youtu\.be/    # Either youtu.be,
                | youtube\.com  # or youtube.com
                  (?:           # Group path alternatives
                    /embed/     # Either /embed/
                  | /v/         # or /v/
                  | .*v=        # or /watch\?v=
                  )             # End path alternatives.
                )               # End host alternatives.
                ([\w-]{10,12})  # Allow 10-12 for 11 char youtube id.
                ($|&).*         # if additional parameters are also in query string after video id.
                $%x'
                ;
                $result = preg_match($pattern, $archivo->ruta, $matches);
                if (false !== $result) {
                  $archivo->ruta = $matches[1];
                }
            }

            $archivo->save();

            return response()->json(['mensaje' => 'Se ha cargado el archivo correctamente!']);
        }else{
            return response()->json(['error' => $validator->errors()->first()]);
        }
    }

    public function eliminar($id){
        $usuario = Auth::id();
        $entidad = Entidad::where('usuario_id', $usuario)->first();
        $archivo = Archivo::find($id);

        if($archivo->entidad_id == $entidad->id){
            $archivo->delete();
            return response()->json(['mensaje' => 'Se ha eliminado el archivo correctamente!']);
        }else{
            return response()->json(['error' => 'Ocurrió un error eliminando el archivo, intentelo nuevamente']);
        }
    }

    public function descargar($usuario, $archivo){
        $ruta = "usuario_".$usuario."/".$archivo;
        if(Storage::has($ruta)){
            $archivo = Storage::get($ruta);
            $mimeType =  Storage::mimeType($ruta);
            return response($archivo)->header('Content-Type', $mimeType);
        }else{
            return response("no se encontró el archivo", 404);
        }
    }
}
