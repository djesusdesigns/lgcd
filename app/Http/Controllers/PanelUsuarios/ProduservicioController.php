<?php

namespace App\Http\Controllers\PanelUsuarios;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Produservicio;

class ProduservicioController extends Controller
{

    public function buscar(Request $request,$formato = 'view')
    {
        $produservicios = Produservicio::where('nombre','LIKE',$request->palabraClave.'%')->limit(100)->get();
        return $produservicios;
    }
}