<?php 

namespace App\Http\Controllers\PanelUsuarios;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lugar;
use App\Like;

use Input;
use Auth;
use Validator;

class LikeController extends Controller
{

	public function index(Request $request)
	{
		$usuario = \Auth::user();

		$likesNoticia = $usuario
							->likes()
							->join('noticias','seccion_id','noticias.id')
							->select('likes.*')
							->where('seccion_type','Noticia')
							->get();

		$likesEvento = $usuario
							->likes()
							->join('eventos','seccion_id','eventos.id')
							->select('likes.*')
							->where('seccion_type','Evento')
							->get();

		$likesSitios = $usuario
							->likes()
							->join('sitio_interes','seccion_id','sitio_interes.id')
							->select('likes.*')
							->where('seccion_type','SitioInteres')
							->get();
		//dd($likesNoticia);

		foreach ($likesNoticia as $like) {
			$noti = $like->seccion;
			$noti = $this->completarDatosNoticia($noti);
		}

		foreach ($likesEvento as $like) {
			$evt = $like->seccion;
			$evt = $this->completarDatosEvento($evt);
		}

		foreach ($likesSitios as $like) {
			$sitio = $like->seccion;
			$sitio = $this->completarDatosSitio($sitio);
		}

		if(request()->is('api/*')){
			return response()->json([
				'likesEvento' => $likesEvento,
				'likesNoticia' => $likesNoticia,
				'likesSitios' => $likesSitios
			]);
		}else{
			return view('panel-usuarios.favoritos')
			->with('likesEvento',$likesEvento)
			->with('likesNoticia',$likesNoticia)
			->with('likesSitios',$likesSitios);
		}
	}

	public function store(Request $request)
	{

		$like = Like::where('usuario_id',Auth::user()->id)
						->where('seccion_id',$request->id)
						->where('seccion_type',$request->modelo)
						->first();

		if($like)
			return $this->delete($like->id);

		$like = new Like;
		$like->seccion_id = $request->id;
		$like->seccion_type = $request->modelo;
		$like->usuario_id = Auth::user()->id; 
		$like->save();

		return response()->json(['mensaje' => 'Like creado con exito', 'plus' => 1 ]);
	}

	public function delete($id)
	{
		// delete
		$like = Like::find($id);
		$like->delete();

		if($like)
			return response()->json(['mensaje' => 'Like eliminado con éxito', 'minus' => 1 ]);
		else
			return response()->json(['error' => 'Error al eliminar este like, revisa los datos de ingreso']);
	}

}
