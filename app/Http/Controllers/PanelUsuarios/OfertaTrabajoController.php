<?php

namespace App\Http\Controllers\PanelUsuarios;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\OfertaTrabajo;
use App\Trabajo;
use App\Entidad;
use App\Lugar;
use App\Actividad;
use App\Aspirante;
use App\Alerta;

use Auth;
use Validator;
use Mail;
use DB;

class OfertaTrabajoController extends Controller{    

    //Muestra la lista de registros
    public function index(Request $request){
        //parametros de busqueda detalle, ciudad
        
        $usuario_id = Auth::id();
        $validator = Validator::make($request->all(), [
            'palabra_clave' => 'string|max:100|min:4',
            'ciudad' => 'exists:lugares,id'
        ]);
        if (!$validator->fails()) {
            //BUSQUEDA
            $ofertas = Trabajo::leftJoin('lugares', 'trabajos.lugar_id', '=', 'lugares.id')
                ->select('trabajos.id', 'trabajos.titulo', 'trabajos.vacantes', 'lugares.nombre as ciudad', 'trabajos.salario', 'trabajos.f_contrato', 'trabajos.f_vencimiento')
                ->where(function ($query) use ($request){
                    if ($request->has('palabra_clave')) {
                        $query->where("trabajos.titulo","LIKE","%".$request->palabra_clave."%")
                            ->orWhere("trabajos.descripcion","LIKE","%".$request->palabra_clave."%")
                            ->orWhere("trabajos.requerimientos","LIKE","%".$request->palabra_clave."%");
                    }
                })
                ->where(function ($query) use ($request){
                    if ($request->has('ciudad')) {
                        $query->where("trabajos.lugar_id", $request->ciudad);
                    }else{
                        $query->whereNotNull('trabajos.lugar_id');
                    }
                })
                ->whereNull('trabajos.deleted_at')
                ->orderBy('trabajos.f_contrato', 'asc')
                ->get();
            if($ofertas->count() > 0)
                return response()->json(['mensaje'=>'Se ha consultado el listado de ofertas laborales exitosamente', 'datos'=>$ofertas]);
            
            return response()->json(['error'=>'No se encontraron ofertas laborales.']);
        }else{
            return response()->json(['error' => $validator->errors()->first()]);            
        }
    }

    //Muestra el formulario para crear un nuevo registro.
    public function create(){
    }

    //Guarda un nuevo registro en la base de datos
    public function store(Request $request){
        $usuario = Auth::id();
        $validator = Validator::make($request->all(), [
            'categoria_del_empleo' => 'required|exists:actividades,id',
            'tipo_de_contrato' => 'required|exists:tipo_contrato,id',
            'experiencia' => 'required|integer|min:0',
            'municipio' => 'required|exists:lugares,id',
            'nivel_de_educacion' => 'required|exists:tipo_educacion,id',
            'cargo' => 'required|max:255',
            'descripcion' => 'required|max:1500',
            'requerimientos' => 'required|max:1500',
            'salario' => 'required|numeric|min:0',
            'fecha_de_publicacion' => 'required|date_format:Y-m-d|after:today',
            'horario_laboral' => 'required|max:255',
            'vacantes' => 'required|integer|min:0',
            'fecha_de_caducidad' => 'required|date_format:Y-m-d|after:fecha_de_publicacion'
        ]);
        $validator->after(function($validator) use($request, $usuario){
            $entidad = Entidad::where('usuario_id', $usuario)->first();
            if(!$entidad){
                $validator->errors()->add('entidad', 'No se encontró la empresa o el municipio.');
            }
        });
        if (!$validator->fails()) {
            $entidad = Entidad::where('usuario_id', $usuario)->first();
            $oferta = new Trabajo();
            $oferta->entidad_id = $entidad->id;
            $oferta->categoria_id = $request->categoria_del_empleo;
            $oferta->tipo_contrato_id = $request->tipo_de_contrato;
            $oferta->experiencia = $request->experiencia;
            $oferta->lugar_id = $request->municipio;
            $oferta->tipo_educacion_id = $request->nivel_de_educacion;
            $oferta->titulo = $request->cargo;
            $oferta->descripcion = $request->descripcion;
            $oferta->requerimientos = $request->requerimientos;
            $oferta->salario = floatval($request->salario);
            $oferta->f_contrato = $request->fecha_de_publicacion;
            $oferta->estado = "activa";
            $oferta->destacar = 0;
            $oferta->horario_laboral = $request->horario_laboral;
            $oferta->vacantes = $request->vacantes;
            $oferta->f_vencimiento = $request->fecha_de_caducidad;
            $oferta->save();
            return response()->json(['mensaje' => 'Se ha creado la oferta de trabajo exitosamente!']);
        }else{
            return response()->json(['error' => $validator->errors()->first()]);
        }
    }

    //Display the specified resource.
    public function show($id){
    }

    //Muestra el formulario para editar un registro en especifico
    public function edit($id){
        $oferta = Trabajo::find($id);
        $oferta->f_contrato = date('Y-m-d', strtotime($oferta->f_contrato));
        $oferta->f_vencimiento = date('Y-m-d', strtotime($oferta->f_vencimiento));
        if($oferta)
            return response()->json(['mensaje'=>'se ha consultado la oferta de trabajo exitosamente', 'datos'=>$oferta]);
        return response()->json(['error'=>'No se encontró la oferta de trabajo']);
    }
    //Update the specified resource in storage.
    public function update(Request $request, $id){
        $usuario = Auth::id();
        $validator = Validator::make($request->all(), [
            'categoria_del_empleo' => 'required|exists:actividades,id',
            'tipo_de_contrato' => 'required|exists:tipo_contrato,id',
            'experiencia' => 'required|integer|min:0',
            'municipio' => 'required|exists:lugares,id',
            'nivel_de_educacion' => 'required|exists:tipo_educacion,id',
            'cargo' => 'required|max:255',
            'descripcion' => 'required|max:1500',
            'requerimientos' => 'required|max:1500',
            'salario' => 'required|numeric|min:0',
            'fecha_de_publicacion' => 'required|date_format:Y-m-d|after:today',
            'horario_laboral' => 'required|max:255',
            'vacantes' => 'required|integer|min:0',
            'fecha_de_caducidad' => 'required|date_format:Y-m-d|after:fecha_de_publicacion'
        ]);
        $validator->after(function($validator) use($id){
            $oferta = Trabajo::find($id);
            if(!$oferta){
                $validator->errors()->add('oferta', 'No se encontró la oferta de trabajo especificada.');
            }
        });
        if (!$validator->fails()) {
            $oferta = Trabajo::find($id);
            $oferta->categoria_id = $request->categoria_del_empleo;
            $oferta->tipo_contrato_id = $request->tipo_de_contrato;
            $oferta->experiencia = $request->experiencia;
            $oferta->lugar_id = $request->municipio;
            $oferta->tipo_educacion_id = $request->nivel_de_educacion;
            $oferta->titulo = $request->cargo;
            $oferta->descripcion = $request->descripcion;
            $oferta->requerimientos = $request->requerimientos;
            $oferta->salario = floatval($request->salario);
            $oferta->f_contrato = $request->fecha_de_publicacion;
            $oferta->estado = "activa";
            $oferta->destacar = 0;
            $oferta->horario_laboral = $request->horario_laboral;
            $oferta->vacantes = $request->vacantes;
            $oferta->f_vencimiento = $request->fecha_de_caducidad;
            $oferta->save();
            return response()->json(['mensaje' => 'Se ha actualizado la oferta de trabajo exitosamente!']);
        }else{
            return response()->json(['error' => $validator->errors()->first()]);
        }
    }
    // Elimina un registro especifico de la base de datos
    public function destroy($id){
        try{            
            DB::connection()->getPdo()->beginTransaction();
            $oferta = Trabajo::find($id);
            $oferta->delete();
            DB::connection()->getPdo()->commit();
            return redirect('/usuario/perfil');
        }catch(\Exception $e){
            DB::connection()->getPdo()->rollBack();
            return redirect('/usuario/perfil');
        }
        return redirect('/usuario/perfil');       
    }

}