<?php

namespace App\Http\Controllers\PanelUsuarios;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Entidad;
use App\Archivo;

use Storage;
use Auth;
use Validator;

use Illuminate\Support\Facades\Log;


class ArchivoPublicoController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function cargar(Request $request,$modelo,$modeloId){
                Log::critical("Cargando imagen");
                Log::critical($request);
                Log::critical($modelo);
                Log::critical($modeloId);
        //**params tipo=Imagen|Video|Audio|Documento, archivo, link
        $usuario_sesion = Auth::user();

        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string',
            'tipo' => 'required|in:Imagen,Video,Audio,Documento',
            'archivo' => 'required_if:tipo,Imagen,tipo,Audio,tipo,Documento|max:100000',
            'link' => 'required_if:tipo,Video|url'
        ]);
        
        $validator->after(function($validator) use($request){
            $mime = ($request->hasFile('archivo'))?$request->archivo->getMimeType():null;
            $tipo = $request->tipo;

            if($tipo=='Imagen' && !($mime=="image/png" || $mime=="image/jpeg" || $mime=="image/gif") ){
                $validator->errors()->add('archivo', $mime.':El archivo no es una imagen, por favor seleccione un archivo de tipo imagen.');
            }

            if($tipo == 'Audio' && !($mime=="audio/mpeg" || $mime=="audio/mp3" || $mime=="audio/mpeg3" || $mime=="application/octet-stream") ){
                $validator->errors()->add('archivo', $mime.':Por el momento no es posible cargar este audio, inténtelo más tarde');
            }

            if($tipo=='Documento' &&
                !($mime=="application/pdf"
                || $mime=="application/msword"
                || $mime=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                || $mime=="application/vnd.openxmlformats-officedocument.wordprocessingml.document")
            ){
                $validator->errors()->add('archivo', $mime.':Por el momento no es posible este documento, inténtelo más tarde' );
            }
        });

        if (!$validator->fails()) {
            $archivo = new Archivo();
            $archivo->modelo_type = $modelo;
            $archivo->modelo_id = $modeloId;
            $archivo->tipo = $request->tipo;
            $archivo->nombre = $request->nombre;
            $archivo->ruta = ($request->has("link"))? $request->link:"";
            $archivo->save();
            Log::critical($archivo->id);

            if($request->tipo != "Video"){
                $nombreArchivo = 'archivo_'.$archivo->id.'.'.$request->file('archivo')->getClientOriginalExtension();
                $carpeta = '/'.$modelo.'s/'.$modelo.'-'.$modeloId;
                $archivo->ruta = '/multimedia/another_files/'.$request->file('archivo')->storeAs($carpeta, $nombreArchivo);
            }else{
                $p = "/^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/";
                $result = preg_match($p, $archivo->ruta, $matches);
                if (false !== $result) {
                  $archivo->ruta = $matches[1];
                }
            }

            $archivo->save();

            return response()->json(['mensaje' => 'Se ha cargado el archivo correctamente!','id' => $archivo->id,'tipo' => $archivo->tipo,'archivo' => $archivo->nombre]);
        }else{
            return response()->json(['error' => $validator->errors()->first() ]);
        }
    }

    public function eliminar($id){
        $archivo = Archivo::find($id);
        $archivo->ruta = str_replace("/multimedia/another_files/", "", $archivo->ruta);

        if($archivo->tipo != 'Video'){
            //borrando arvhivo en disco
            if(Storage::disk('local')->exists($archivo->ruta))
                $archivoDisco = Storage::disk('local')->delete($archivo->ruta);    
        }
        
        //borrando archivo en base de datos
        $archivo->delete();

        return response()->json(['mensaje' => 'Se ha eliminado el archivo correctamente!']);
        //return response()->json(['error' => 'Ocurrió un error eliminando el archivo, intentelo nuevamente']);
    }

    public function descargar($usuario, $archivo){
        $ruta = "usuario_".$usuario."/".$archivo;
        if(Storage::has($ruta)){
            $archivo = Storage::get($ruta);
            $mimeType =  Storage::mimeType($ruta);
            return response($archivo)->header('Content-Type', $mimeType);
        }else{
            return response("no se encontró el archivo", 404);
        }
    }

    public function obtenerEnlace($archivo_id){
        return response()->json(['id' => $archivo_id]);
    }
}
