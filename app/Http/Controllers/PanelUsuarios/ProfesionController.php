<?php

namespace App\Http\Controllers\PanelUsuarios;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Profesion;

class ProfesionController extends Controller
{
    
    public function buscar(Request $request,$formato = 'view')
    {
        $profesiones = Profesion::where('nombre','LIKE',$request->palabraClave.'%')->limit(100)->get();
        return $profesiones;
    }
}