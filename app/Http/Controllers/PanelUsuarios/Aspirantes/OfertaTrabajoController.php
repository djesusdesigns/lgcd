<?php

namespace App\Http\Controllers\PanelUsuarios\Aspirantes;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\OfertaTrabajo;
use App\Trabajo;
use App\Entidad;
use App\Lugar;
use App\Actividad;
use App\Aspirante;
use App\Alerta;

use Auth;
use Validator;
use Mail;
use DB;

class OfertaTrabajoController extends Controller{    

    

    public function correo_ofertas_similares()
    {
        set_time_limit(0);
        
        $aspirantesIds = Alerta::join('aspirantes', 'alertas.aspirante_id', '=', 'aspirantes.id')
                                    ->groupBy('aspirante_id')
                                    ->get()
                                    ->pluck('aspirante_id');

        if(sizeof($aspirantesIds)==0)
            return 'no hay aspirantes con preferencias sobre ofertas de trabajo';

        $aspirantes = Aspirante::whereIn('id',$aspirantesIds)->get();

        foreach ($aspirantes as $key => $aspirante) {
            if($aspirante->tengo_empleo == 'No' || is_null($aspirante->tengo_empleo)){
                $actividadesIds = Alerta::where('aspirante_id',$aspirante->id)
                                        ->get()
                                        ->pluck('actividad_id');

                $solicitudes = $aspirante->solicitudes()->get()->pluck('trabajo_id');

                $ofertasSimilares = Trabajo::select('trabajos.*')
                                                ->leftjoin('aspirante_trabajo', 'aspirante_trabajo.trabajo_id', '=', 'trabajos.id')
                                                ->whereNotIn('trabajos.id',$solicitudes)
                                                ->where('trabajos.estado','activa')
                                                ->where('trabajos.vacantes','>',0)
                                                ->whereIn('trabajos.categoria_id',$actividadesIds)
                                                ->get();

                if(sizeof($ofertasSimilares)){

                    $usuario = $aspirante->usuario;

                    if($usuario){
                        \Mail::send('mail.trabajos.ofertas-similares', ['ofertas'=>$ofertasSimilares], function ($m)  use($usuario){
                            $m->from('info@laguiacundinamarca.com', 'CundiEmpleos, La Guía Cundinamarca');
                            $m->to($usuario->correo, $usuario->nombre)->subject('Nuevas ofertas de trabajo para usted en CundiEmpleos');
                        });
                    }
                }

                sleep(2);
            }
        }

        return "correos enviados exitosamente";
    }

    public function aplicar($ofertaId)
    {
        $oferta = OfertaTrabajo::find($ofertaId);
        $aspirante = Auth::user()->aspirante;
        $requisitos = true;
        $experiencia = false;
        $tipo_educacion =false;

        if($aspirante->experiencia < $oferta->experiencia)
            $requisitos = false;
        else
            $experiencia = true;

        if(is_null($aspirante->tipo_educacion_id))
            $aspirante->tipo_educacion_id = 0;

        if($aspirante->tipo_educacion_id==0)
            $aspirante->tipo_educacion_id = 1;

        if($aspirante->tipo_educacion_id < $oferta->tipo_educacion_id)
            $requisitos = false;
        else
            $tipo_educacion = true;

        if($aspirante->edad < $oferta->edad_minima && $oferta->edad_minima > 0)
            $requisitos = false;

        if($aspirante->edad < $oferta->edad_minima && $oferta->edad_maxima > 0)
            $requisitos = false;

        if(is_null($aspirante->disp_viaje))
            $aspirante->disp_viaje = "No";
    
        if($aspirante->disp_viaje == $oferta->disp_viaje)
            $requisitos = false;

        if(is_null($aspirante->disp_cambio_hogar))
            $aspirante->disp_viaje = "No";

        if($aspirante->disp_cambio_hogar == $oferta->disp_cambio_hogar)
            $requisitos = false;

        $trabajoAplicado = \DB::table('aspirante_trabajo')
                    ->where('aspirante_id',$aspirante->id)
                    ->where('trabajo_id',$oferta->id)
                    ->get();
        
        if(sizeof($trabajoAplicado))
            return back()->with('error', "Usted ya ha aplicado a esta oferta laboral");


        if($experiencia && $tipo_educacion){
            \DB::table('aspirante_trabajo')->insert([
                'aspirante_id' => $aspirante->id,
                'trabajo_id' => $oferta->id,
                'f_solicitud' => date('Y-m-d H:i:s'),
                'estado' => 'En espera',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            $this->emailAplicaTrabajo($oferta,$aspirante);
        }


        if(!$experiencia)
            return back()->with('error', 'Usted no ha podido aplicar a esta oferta, no cumple con el minimo de experiencia requerida');
        else if(!$tipo_educacion)
            return back()->with('error', 'Usted no ha podido aplicar a esta oferta, no cumple con la educación minima requerida');
        else if(!$requisitos)
            return back()->with('warning', 'Usted ha aplicado a esta oferta sin embargo no cumple con todos los requisitos, su hoja de vida entrará en revisión');
        else
            return back()->with('mensaje', 'Usted ha aplicado con exito a la oferta laboral');
    }

    private function emailAplicaTrabajo($oferta,$aspirante)
    {
      
        $dataMail = [
            'oferta' => $oferta
        ];

        if($aspirante->usuario){
            \Mail::send('mail.trabajos.aplicar-oferta', $dataMail, function ($m) use ($aspirante){
                $m->from('info@laguiacundinamarca.com', 'La Guía cundinamarca');
                $m->to($aspirante->usuario->correo,$aspirante->usuario->nombre)
                            ->subject('Información de la oferta laboral aplicada');
            });
        }
    }
    
}