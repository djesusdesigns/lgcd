<?php

namespace App\Http\Controllers\PanelUsuarios\Aspirantes;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Auth;
use Validator;
use Storage;
use DB;

use App\Archivo;
use App\Usuario;
use App\Aspirante;
use App\Experiencia;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ExperienciaController extends Controller{

    public function index(){}

    public function store(Request $request){

        $id = Auth::id();

        $validator = Validator::make($request->all(), [
            'empresa' => 'required|string|max:255',
            'cargo' => 'required|string|max:255',
            'area' => 'required|string|max:255',
            'telefono' => 'required|string|max:50',
            'ciudad' => 'required|exists:lugares,id',
            'fecha_de_ingreso' => 'required|date_format:Y-m-d|before:now',
            'fecha_de_salida' => 'required|date_format:Y-m-d|after:fecha_de_ingreso',
            'funciones' => 'required|string|max:200'
        ]);

        if (!$validator->fails()) {

            $usuario = Usuario::find($id);                
            $aspirante = Aspirante::where("usuario_id", $id)->first();
            $f_ingreso = date("Y-m",strtotime($request->fecha_de_ingreso));
            $f_salida = date("Y-m",strtotime($request->fecha_de_salida));
            $aspirante->experiencia += floor((strtotime($f_salida) - strtotime($f_ingreso)) / 31557600);
            $aspirante->save();

            $experiencia = new Experiencia();

            $experiencia->aspirante_id = $aspirante->id;
            $experiencia->lugar_id = $request->ciudad;
            $experiencia->empresa = $request->empresa;
            $experiencia->cargo = $request->cargo;
            $experiencia->area = $request->area;
            $experiencia->f_ingreso = $request->fecha_de_ingreso;
            $experiencia->f_salida = $request->fecha_de_salida;
            $experiencia->descripcion = $request->funciones;
            $experiencia->telefono = $request->telefono;

            $experiencia->save();

            return response()->json(['mensaje' => "Se ha creado el registro exitosamente!"]);

        }else{
            return response()->json(['error' => $validator->errors()->first()]);
        }
    }

    public function edit($id){
        $experiencia = Experiencia::find($id);

        if($experiencia){
            $experiencia->f_ingreso = date('Y-m-01', strtotime($experiencia->f_ingreso));
            $experiencia->f_salida = date('Y-m-02', strtotime($experiencia->f_salida));
            return response()->json(['mensaje'=>'Se ha consultado la experiencia laboral exitosamente', 'datos'=>$experiencia]);
        }
        return response()->json(['error'=>'No se encontró la experiencia laboral']);
    }

    public function update(Request $request, $id){

        $usuario_id = Auth::id();

        $validator = Validator::make($request->all(), [
            'empresa' => 'required|string|max:255',
            'cargo' => 'required|string|max:255',
            'area' => 'required|string|max:255',
            'telefono' => 'required|string|max:50',
            'ciudad' => 'required|exists:lugares,id',
            'fecha_de_ingreso' => 'required||date_format:Y-m-d|before:now',
            'fecha_de_salida' => 'required||date_format:Y-m-d|after:fecha_de_ingreso',
            'funciones' => 'required|string|max:200'
        ]);

        if ($validator->fails())
            return response()->json(['error' => $validator->errors()->first()]);

        try{
            DB::connection()->getPdo()->beginTransaction();
               
            $experiencia = Experiencia::find($id);

            $f_ingreso_anterior = date("Y-m-01",strtotime($experiencia->f_ingreso));
            $f_salida_anterior = date("Y-m-02",strtotime($experiencia->f_salida));

            $experiencia->lugar_id = $request->ciudad;
            $experiencia->empresa = $request->empresa;
            $experiencia->cargo = $request->cargo;
            $experiencia->area = $request->area;
            $experiencia->f_ingreso = $request->fecha_de_ingreso;
            $experiencia->f_salida = $request->fecha_de_salida;
            $experiencia->descripcion = $request->funciones;
            $experiencia->telefono = $request->telefono;
            $experiencia->save();

            $aspirante = $experiencia->aspirante;

            $aspirante->experiencia -= floor((strtotime($f_salida_anterior) - strtotime($f_ingreso_anterior)) / 31557600);
            if($aspirante->experiencia < 0) $aspirante->experiencia = 0;
            
            $f_ingreso = date("Y-m-01",strtotime($experiencia->f_ingreso));
            $f_salida = date("Y-m-02",strtotime($experiencia->f_salida));

            $aspirante->experiencia += floor((strtotime($f_salida) - strtotime($f_ingreso)) / 31557600);
            $aspirante->save();

            DB::connection()->getPdo()->commit();
            return response()->json(['mensaje' => "Se ha actualizado el registro exitosamente!"]);
        }catch(\Exception $e){
            DB::connection()->getPdo()->rollBack();
            return response()->json(['error' => 'Se ha producido un error al editar este registro, revise los datos de ingreso']);
        }
        
    }

    public function destroy($id){

        try{
            DB::connection()->getPdo()->beginTransaction();
            
            $experiencia = Experiencia::find($id);
            $aspirante = $experiencia->aspirante;

            $f_ingreso = date("Y-m-01",strtotime($experiencia->f_ingreso));
            $f_salida = date("Y-m-02",strtotime($experiencia->f_salida));

            $aspirante->experiencia -= floor((strtotime($f_salida) - strtotime($f_ingreso)) / 31557600);
            if($aspirante->experiencia < 0) $aspirante->experiencia = 0;
            
            $aspirante->save();

            if($experiencia)
                $experiencia->delete();

            DB::connection()->getPdo()->commit();
        }catch(\Exception $e){
            DB::connection()->getPdo()->rollBack();
        }

        return redirect('/perfil');
    }

    public function show($id){

        $usuario_id = Auth::id();
        $usuario = Usuario::find($usuario_id);               
        $aspirante = Aspirante::where("usuario_id", $usuario->id)->first();
        $experiencia = Experiencia::find($id)->first();

        if($experiencia && $experiencia->aspirante_id == $aspirante->id){
            return response()->json(['mensaje' => "Datos consultados correctamente!", 'datos'=>$experiencia]);         
        }else{
            return response()->json(['error' => "No tiene los permisos para consultar este registro!"]);
        }
    }
}