<?php

namespace App\Http\Controllers\PanelUsuarios\Aspirantes;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Auth;
use Validator;
use Storage;
use DB;

use App\Archivo;
use App\Usuario;
use App\Aspirante;
use App\Educacion;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class EducacionController extends Controller{

    public function index(){}

    public function store(Request $request){

        $id = Auth::id();

        $validator = Validator::make($request->all(), [
            'titulo' => 'required|string|max:150',
            'institucion' => 'required|string|max:255',
            'nivel_de_estudios' => 'required|exists:tipo_educacion,id',
            'estado' => 'required|string|max:50',
            'fecha_de_ingreso' => 'required|date_format:Y-m-d|before:now',
            'fecha_de_salida' => 'date_format:Y-m-d|after:fecha_de_ingreso',
            'certificado' => 'max:500'
        ]);

        $validator->after(function($validator) use($request){
            if($request->hasFile('certificado')){
                $mime = $request->certificado->getMimeType();

                if($mime!="image/png" && $mime!="image/jpeg" && $mime!="image/gif" && $mime!="image/bmp"){
                    $validator->errors()->add('certificado', 'El certificado debe ser una imagen.');
                }    
            }
            
        });

        if (!$validator->fails()) {

            $usuario = Usuario::find($id);                
            $aspirante = Aspirante::where("usuario_id", $id)->first();

            $educacion = new Educacion();

            $educacion->aspirante_id =  $aspirante->id;
            $educacion->tipo_educacion_id = $request->nivel_de_estudios;
            $educacion->institucion = $request->institucion;
            $educacion->f_ingreso = $request->fecha_de_ingreso;
            $educacion->f_salida = ($request->has('fecha_de_salida'))? $request->fecha_de_salida:null;
            $educacion->estado = $request->estado;
            $educacion->titulo = $request->titulo;

            $educacion->save();

            if($educacion->tipo_educacion_id > $aspirante->tipo_educacion_id){
                $aspirante->tipo_educacion_id = $educacion->tipo_educacion_id;
                $aspirante->save();
            }

            if ($request->hasFile('certificado')) {

                $carpeta_certificado = 'usuario_'.$id.'/certificado'.$educacion->id.'.'.$request->file("certificado")->getClientOriginalExtension();
                $certificado = file_get_contents($request->file("certificado")->getRealPath());
                Storage::put($carpeta_certificado, $certificado);                 
                $educacion->certificado = '/file/'.$id.'/certificado'.$educacion->id.'.'.$request->file("certificado")->getClientOriginalExtension();

                $educacion->save();
                
            }

            return response()->json(['mensaje' => "Se ha creado el registro exitosamente!"]);

        }else{
            return response()->json(['error' => $validator->errors()->first()]);
        }
    }

    public function edit($id){
        $estudio = Educacion::find($id);

        if($estudio){
            $estudio->f_ingreso = date('Y-m-d', strtotime($estudio->f_ingreso));
            $estudio->f_salida = date('Y-m-d', strtotime($estudio->f_salida));

            return response()->json(['mensaje'=>'Se ha consultado la foramación académica exitosamente', 'datos'=>$estudio]);
        }
        return response()->json(['error'=>'No se encontró la formación académica.']);
    }

    public function update(Request $request, $id){

        $validator = Validator::make($request->all(), [
            'titulo' => 'required|string|max:150',
            'institucion' => 'required|string|max:255',
            'nivel_de_estudios' => 'required|exists:tipo_educacion,id',
            'estado' => 'required|string|max:50',
            'fecha_de_ingreso' => 'required|date_format:Y-m-d|before:now',
            'fecha_de_salida' => 'date_format:Y-m-d|after:fecha_de_ingreso',
            'certificado' => 'max:3000'
        ]);

        $validator->after(function($validator) use($request){
            if($request->hasFile('certificado')){
                $mime = $request->certificado->getMimeType();

                if($mime!="image/png" && $mime!="image/jpeg" && $mime!="image/gif" && $mime!="image/bmp"){
                    $validator->errors()->add('certificado', 'El certificado debe ser una imagen.');
                }
            }
        });

        if (!$validator->fails()) {
               
            $educacion = Educacion::find($id);

            $educacion->tipo_educacion_id = $request->nivel_de_estudios;
            $educacion->institucion = $request->institucion;
            $educacion->f_ingreso = $request->fecha_de_ingreso;
            $educacion->f_salida = ($request->has('fecha_de_salida'))? $request->fecha_de_salida:null;
            $educacion->estado = $request->estado;
            $educacion->titulo = $request->titulo;
            $educacion->save();

            $usuario_id = Auth::id();
            $aspirante = Aspirante::where("usuario_id", $usuario_id)->first();

            if($educacion->tipo_educacion_id > $aspirante->tipo_educacion_id){
                $aspirante->tipo_educacion_id = $educacion->tipo_educacion_id;
                $aspirante->save();
            }

            if($request->hasFile("certificado")){

                $carpeta_certificado = 'usuario_'.$id.'/certificado'.$educacion->id.'.'.$request->file("certificado")->getClientOriginalExtension();
                $certificado = file_get_contents($request->file("certificado")->getRealPath());
                Storage::put($carpeta_certificado, $certificado);                 
                $educacion->certificado = '/file/'.$id.'/certificado'.$educacion->id.'.'.$request->file("certificado")->getClientOriginalExtension();

                $educacion->save();

            }

            return response()->json(['mensaje' => "Se ha actualizado el registro exitosamente!"]);

        }else{
            return response()->json(['error' => $validator->errors()->first()]);
        }
    }

    public function destroy($id){

        $educacion = Educacion::find($id);
        if($educacion)
            $educacion->delete();

        return redirect('/perfil');       

    }

    public function show($id){

        $usuario_id = Auth::id();
        $usuario = Usuario::find($usuario_id);               
        $aspirante = Aspirante::where("usuario_id", $usuario->id)->first();
        $educacion = Educacion::find($id)->first();

        if($educacion && $educacion->aspirante_id == $aspirante->id){
            return response()->json(['mensaje' => "Datos consultados correctamente!", 'datos'=>$educacion]);         
        }else{
            return response()->json(['error' => "No tiene los permisos para consultar este registro!"]);
        }
    }
}