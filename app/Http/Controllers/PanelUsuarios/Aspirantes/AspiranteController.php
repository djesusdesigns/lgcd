<?php

namespace App\Http\Controllers\PanelUsuarios\Aspirantes;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Auth;
use Validator;
use Storage;
use DB;
use App;
use PDF;

use App\Archivo;
use App\Usuario;
use App\Aspirante;
use App\AspiranteTrabajo;
use App\Trabajo;
use App\Alerta;
use App\Lugar;
use App\OfertaTrabajo;

use App\Http\Requests;
use App\Http\Controllers\Controller;


class AspiranteController extends Controller{


    public function index(Request $request){

        //parametros de busqueda categoria_id, experiencia, tipo_educacion_id, salario, lugar_id, oferta_id
        
        $usuario_id = Auth::id();

        $validator = Validator::make($request->all(), [
            'detalle' => 'string|min:4',
            'categoria_id' => 'exists:actividades,id',
            'experiencia' => 'integer|min:0',
            'tipo_educacion_id' => 'exists:tipo_educacion,id',
            'salario' => 'numeric|min:0',
            'lugar_id' => 'exists:lugares,id',
            'oferta_id' => 'exists:trabajos,id'
        ]);

        if (!$validator->fails()) {

            //BUSQUEDA
            $aspirantes = Aspirante::join('usuarios', 'aspirantes.usuario_id', '=', 'usuarios.id')
                ->leftJoin('lugares', 'aspirantes.lugar_id', '=', 'lugares.id')
                ->leftJoin('tipo_educacion', 'aspirantes.tipo_educacion_id', '=', 'tipo_educacion.id')
                ->leftJoin('aspirante_trabajo', 'aspirantes.id', '=', 'aspirante_trabajo.aspirante_id')
                ->leftJoin('profesiones', 'aspirantes.profesion_id', '=', 'profesiones.id')
                ->select('aspirantes.id', 'usuarios.nombre', 'usuarios.apellido', 'aspirantes.telefonos')
                ->addSelect('lugares.nombre as ciudad', 'aspirantes.experiencia', 'aspirantes.tipo_educacion_id')
                ->addSelect('aspirantes.salario','aspirantes.lugar_id','aspirante_trabajo.trabajo_id')
                ->addSelect('aspirantes.profesion_otra', 'profesiones.nombre as profesion')
                ->addSelect('tipo_educacion.nombre as nivel_educativo', 'aspirante_trabajo.estado')
                ->where(function ($query) use ($request){
                    if ($request->has('detalle')) {
                        $query->where("usuarios.nombre","LIKE","%".$request->detalle."%")
                            ->orWhere("usuarios.apellido","LIKE","%".$request->detalle."%")
                            ->orWhere("aspirantes.perfil_laboral","LIKE","%".$request->detalle."%")
                            ->orWhere("aspirantes.habilidades","LIKE","%".$request->detalle."%")
                            ->orWhere("aspirantes.profesion_otra","LIKE","%".$request->detalle."%");
                    }
                })
                
                ->where(function ($query) use ($request){
                    if ($request->has('experiencia')) {
                        $query->where('aspirantes.experiencia','>=',$request->experiencia);
                    }
                })
                ->where(function ($query) use ($request){
                    if ($request->has('tipo_educacion_id')) {
                        $query->where("aspirantes.tipo_educacion_id",'>=',$request->tipo_educacion_id);
                    }
                })
                ->where(function ($query) use ($request){
                    if ($request->has('salario')) {
                        $query->where("aspirantes.salario","<",$request->salario);
                    }
                })
                ->where(function ($query) use ($request){
                    if ($request->has('lugar_id')) {
                        $query->where("aspirantes.lugar_id", $request->lugar_id);
                    }
                })
                ->where(function ($query) use ($request){
                    if ($request->has('oferta_id')) {
                        $query->where("aspirante_trabajo.trabajo_id",$request->oferta_id);
                    }
                })
                ->orderBy('usuarios.nombre', 'asc')
                ->distinct()
                ->groupBy('aspirantes.id')
                ->get();


            if($aspirantes->count() > 0)
                $aspirantes = $aspirantes->map(function($item, $key){
                    if(!is_null($item->telefonos)){
                        $telefonos = json_decode($item->telefonos);
                        if(!is_null($telefonos))
                            $item->telefonos = $telefonos->telefono.' - '.$telefonos->celular;
                        else
                            $item->telefonos = "";
                    }
                    return $item;
                });
                return response()->json(['mensaje'=>'Se ha consultado el listado de aspirantes exitosamente', 'datos'=>$aspirantes]);
            
            return response()->json(['error'=>'No se encontraron aspirantes.']);

        }else{
            return response()->json(['error' => $validator->errors()->first()]);            
        }
    }

    public function mostrar_panel(){
        return redirect('usuario/noticia');

        $usuario = Auth::user();
        $campos = 0;
        $total = 6;

        $eventos = $usuario->eventos;
        $noticias = $usuario->noticias;

        $aspirante = Aspirante::where('usuario_id', $usuario->id)->first();
        $aspirante->foto = json_decode($aspirante->foto);
        $aspirante->telefonos = json_decode($aspirante->telefonos);
        $aspirante->f_nacimiento = ($aspirante->f_nacimiento)? date('Y-m-d', strtotime($aspirante->f_nacimiento)):'';
        $aspirante->habilidades = ($aspirante->habilidades)? json_decode($aspirante->habilidades):[];
        $tecnologias = [];
        $idiomas = [];
        for ($i=0; $i < count($aspirante->habilidades) ; $i++) { 
            if ($aspirante->habilidades[$i][3] == 1) {
                array_push($tecnologias, $aspirante->habilidades[$i]);
            }else{
                array_push($idiomas, $aspirante->habilidades[$i]);
            }
        }

        $experiencias = $aspirante->experiencias;
        $experiencias = $experiencias->map(function($item, $key){
            $item->f_ingreso = date('m/Y', strtotime($item->f_ingreso));
            $item->f_salida = date('m/Y', strtotime($item->f_salida));
            return $item;
        });

        $estudios = $aspirante->estudios;
        $estudios = $estudios->map(function($item, $key){
            $item->f_ingreso = date('m/Y', strtotime($item->f_ingreso));
            $item->f_salida = date('m/Y', strtotime($item->f_salida));
            $item["tipo_educacion"] = $item->tipo_educacion;
            return $item;
        });

        $aplicaciones = $aspirante->trabajos;
        $aplicaciones = $aplicaciones->map(function($item, $key){
            $item["ciudad"] = $item->lugar->nombre;
            $item["empresa"] = $item->entidad->razon_social;
            return $item;
        });

        $total -= ($aspirante->tengo_empleo)? 0:1;
        $campos_aspirante = collect($aspirante)->values()->all();

        for ($i=0; $i < count($campos_aspirante); $i++) {
            $campos += ($campos_aspirante[$i])?1:0;
            $total += 1;
        }

        $campos += ($experiencias->count()>0)? 3:0;
        $campos += ($estudios->count()>0)? 3:0;

        $soloMunicipios = Lugar::todos_los_municipios()->orderBy('nombre','asc')->get();
        $departamentos = Lugar::departamentos()->get();

        return view("panel-usuarios.aspirantes.inicio",[
            'soloMunicipios' => $soloMunicipios,
            'departamentos' => $departamentos,
            'ofertasDestacadas' => [],
            'usuario'=> $usuario,
            'aspirante' => $aspirante,
            'aplicaciones' => $aplicaciones,
            'experiencias' => $experiencias,
            'estudios' => $estudios,
            'tecnologias' => $tecnologias,
            'idiomas' => $idiomas,
            'eventos' => $eventos,
            'noticias' => $noticias,
            'porcentaje' => ceil(($campos/$total)*100)
        ]);
    }

    //Muestra el formulario para crear un nuevo registro.
    public function create(){
        //
    }

    //Guarda un nuevo registro en la base de datos
    public function store(Request $request){
        // dd($request->all());
        $id = Auth::id();

        $validator = Validator::make($request->all(), [
            'nombres' => 'nullable|required|string|max:255',
            'apellidos' => 'nullable|required|string|max:255',
            'documento' => 'nullable|integer',
            'fecha_de_nacimiento' => 'nullable|before:now',
            'genero' => 'nullable|in:Femenino,Masculino,Otro',
            'estado_civil' => 'nullable|in:Casado,Soltero',
            'pais' => 'nullable|string|max:255',
            'departamento' => 'nullable|exists:lugares,id',
            'lugar_id' => 'nullable|exists:lugares,id',
            'direccion' => 'nullable|string|max:255',
            'email' => 'nullable|email|unique:usuarios,correo,'.$id,
            'telefono' => 'nullable|string',
            'celular' => 'nullable|string',
            'licencia_de_conduccion' => 'nullable|in:A1,A2,B1,B2,B3,C1,C2,C3,No tengo',
            'discapacidad' => 'nullable|in:Si,No',
            'foto1' => 'max:1000',
            'foto2' => 'max:1000',
            'foto3' => 'max:1000',
            'foto4' => 'max:1000',
            'foto5' => 'max:1000',
        ]);

        $validator->after(function($validator) use($request){
            for ($i=1; $i <= 5; $i++) { 
                if($request->hasFile("foto".$i)){
                    $mime = $request->file("foto".$i)->getMimeType();
                    if($mime != 'image/jpeg' && $mime != 'image/png' && $mime != 'image/bmp'){
                        return $validator->errors()->add('archivo', 'Debe ingresar una imagen válida');
                    }
                }
            }
        });

        if (!$validator->fails()) {  

            $transaccion = DB::transaction(function () use($request, $id) {

                $usuario = Usuario::find($id);
                $aspirante = $usuario->aspirante;

                $usuario->nombre = $request->nombres;
                $usuario->apellido = $request->apellidos;
                $usuario->correo = ($request->has("email"))? $request->email : $usuario->correo;
                $usuario->cedula = $request->documento;

                $usuario->save();

                $aspirante->f_nacimiento = $request->fecha_de_nacimiento;
                $aspirante->genero = $request->genero;
                $aspirante->estado_civil = $request->estado_civil;
                $aspirante->nacionalidad = $request->pais;
                $aspirante->lugar_id = $request->has("lugar_id") ? $request->lugar_id : null;
                $aspirante->direccion = ($request->has("direccion"))? $request->direccion : $aspirante->direccion;
                $aspirante->telefonos = json_encode(['telefono' => ($request->has("telefono"))?$request->telefono:'', 'celular' => ($request->has("celular"))?$request->celular:'']);
                $aspirante->licencia_conduccion = $request->licencia_de_conduccion;
                $aspirante->discapacidad = $request->discapacidad;

                $fotos = json_decode($aspirante->foto);
                $fotos = ($fotos)? $fotos: ['','','','',''];

                for ($i=1; $i <= 5; $i++) {
                    if($request->hasFile("foto".$i)){
                        $fotos[$i-1] =($request->hasFile("foto".$i))? '/multimedia/another_files/'.$this->guardar_foto($id, $request->file("foto".$i), $i) : $fotos[$i-1];
                    }
                }

                $aspirante->foto = json_encode($fotos);

                $aspirante->save();

            });
            

        if (request()->is('api/*'))
            return response()->json(['mensaje' => 'Se ha actualizado su información exitosamente!' ]);
        else
            return back()->with(['msjExito' => "Se ha actualizado su información exitosamente!"]);

        }else{

            if (request()->is('api/*'))
                return response()->json(['error' => $validator->errors()->first()]);
            else
                return back()->with(['errors' => $validator->errors(), 'datos'=>$request->all() ]);
        }
    }

    public function cambiar_imagen(Request $request){

        $id = Auth::id();

        $validator = Validator::make($request->all(), [
            'foto' => 'required|max:4000',
        ]);

        //dd($request->all());

        $validator->after(function($validator) use($request){
            if($request->hasFile("foto")){
                $mime = $request->file("foto")->getMimeType();
                if($mime != 'image/jpeg' && $mime != 'image/png' && $mime != 'image/bmp'){
                    return $validator->errors()->add('archivo', 'Debe ingresar una imagen válida');
                }
            }
        });

        if ($validator->fails()) {
            if (request()->is('api/*')) {
                return response()->json([ 'error' => $validator->errors()->first() ]);
            }else{
                $error = $validator->errors()->first();
                return redirect()->back()->withErrors($error);
            }
        }

        try{
            DB::connection()->getPdo()->beginTransaction();
            
            $usuario = Usuario::find($id);
            $aspirante = $usuario->aspirante;
            $rutaFoto = "";

            $fotos = json_decode($aspirante->foto);
            $fotos = ($fotos)? $fotos: ['','','','',''];

            if($request->hasFile("foto")){
                $rutaFoto = '/multimedia/another_files/'.$this->guardar_foto($id, $request->file("foto"), 0);
                $fotos[0] = $rutaFoto;
                $aspirante->foto = json_encode($fotos);
                $aspirante->save();
            }

            DB::connection()->getPdo()->commit();

            if (request()->is('api/*'))
                return response()->json(['mensaje' => "Se ha actualizado su imagen de perfil exitosamente!", 'foto' => $rutaFoto ]);
            else
                return back()->with(['msjExito' => "Se ha actualizado su imagen de perfil exitosamente!" ]);

        }catch(\Exception $e){
            DB::connection()->getPdo()->rollBack();

            if (request()->is('api/*'))
                return response()->json(['error' => "Error al cambiar la imagen de perfil." ]);
            else
                return back()->with(['msjError' => $e->getMessage() ]);
        }

    }

    //Display the specified resource.
    public function show($id){
        //
    }

    //Muestra el formulario para editar un registro en especifico
    public function edit($id){
        $usuario =  Auth::user();
        $aspirante =  Auth::user()->aspirante;
        $aspirante->telefonos = json_decode($aspirante->telefonos);

        return view('panel-usuarios.aspirantes.form-perfil')
            ->with([
                'usuario' => $usuario,
                'aspirante' => $aspirante
            ]);
    }

    //Update the specified resource in storage.
    public function update(Request $request, $id){

        $id = Auth::id();

        $validator = Validator::make($request->all(), [
            'perfil_parte' => 'required|in:1,2',
            'profesion_id' => 'string|max:255',
            'profesion_otra' => 'required_if:check_otra_prof,1|string|max:255',
            'perfil_profesional' => 'required_if:perfil_parte,1|string|max:500',

            'salario' => 'required_if:perfil_parte,2|numeric|min:0',
            'ciudad' => 'required_if:perfil_parte,2|exists:lugares,id',
            'disp_viaje' => 'required_if:perfil_parte,2|in:Si,No',
            'disp_cambio_hogar' => 'required_if:perfil_parte,2|in:Si,No'

            ],[

            'profesion_id.required_without' => 'El campo profesion es obligatorio',
            'perfil_profesional.required_if' => 'El campo descripción breve Perfil profesional es obligatorio',
            'salario.required_if' => 'El campo salario es obligatorio',
            'ciudad.required_if' => 'El campo Busco trabajo en es obligatorio',
            'disp_viaje.required_if' => 'El campo disponibilidad para viajar es obligatorio',
            'disp_cambio_hogar.required_if' => 'El campo disponibilidad para cambiar de residencia es obligatorio',

            ]);

        if (!$validator->fails()) {  

            $aspirante = Aspirante::where('usuario_id', $id)->first();

            if($request->perfil_parte == 1){

                if($request->check_otra_prof){
                    if($aspirante->profesion_otra != $request->profesion_otra){
                        \Mail::send('mail.nueva-profesion', ['usuario' => Auth::user(), 'profesion' => $request->profesion_otra], function ($m){
                            $m->from('info@laguiacundinamarca.com', 'La Guía Cundinamarca');
                            $m->to('info@laguiacundinamarca.com','La Guía Cundinamarca')->subject('Nueva profesión registrada - La Guía Cundinamarca');
                        });
                    }

                    $aspirante->profesion_otra = $request->profesion_otra;
                    $aspirante->profesion_id = null;
                }else{
                    $aspirante->profesion_otra = null;
                    $aspirante->profesion_id = $request->profesion_id;
                }
                
                $aspirante->perfil_laboral = $request->perfil_profesional;

            }else if($request->perfil_parte == 2){

                $aspirante->salario = $request->salario;
                $aspirante->lugar_trabajo_id = $request->ciudad;
                $aspirante->disp_viaje = $request->disp_viaje;
                $aspirante->disp_cambio_hogar = $request->disp_cambio_hogar;
            }

            $aspirante->save();

            return response()->json(['mensaje' => "Se ha actualizado su información exitosamente!"]);

        }else{
            return response()->json(['error' => $validator->errors()->first(), 'datos'=>$request->all() ]);
        } 
    }

    //Elimina un registro especifico de la base de datos
    public function destroy($id){
    }

    public function notificar($id){

        $aspirante = Aspirante::where('usuario_id', Auth::id())->first();
        $oferta = Trabajo::find($id);

        $alerta =  Alerta::where('aspirante_id', $aspirante->id)->where('actividad_id', $oferta->categoria_id)->first();
        if(!$alerta) $alerta = new Alerta();
        $alerta->aspirante_id = $aspirante->id;
        $alerta->actividad_id = $oferta->categoria_id;
        $alerta->save();

        return back()->with('mensaje', 'Ahora recibirá ofertas similares a su correo!');
    }

    public function eliminar($id){

        $usuario = Usuario::find($id)->first();
        $usuario->delete();

        return response()->json(['mensaje' => "Usuario eliminado correctamente!"]);
    }

    public function consultar(){

        $usuario_id = Auth::id();
        $usuario = Usuario::find($usuario_id);               
        $aspirante = Aspirante::where("usuario_id", $usuario->id)->first();

        return view('amarillas.aspirante', [
            'usuario'=>$usuario,
            'aspirante'=>$aspirante
        ]);
    }

    public function cambiar_estado(Request $request){

        $usuario_id = Auth::id();

        $validator = Validator::make($request->all(), [
            'oferta_id' => 'required',
            'aspirante_id' => 'required',
            'estado' => 'required|in:En espera,Aprobado,Rechazado',
        ]);

        $validator->after(function($validator) use($request){
            $aspiranteTrabajo = AspiranteTrabajo::where('trabajo_id',$request->oferta_id)->where('aspirante_id',$request->aspirante_id)->first();
            if(!$aspiranteTrabajo){
                $validator->errors()->add('aspirante_id', 'No se encontró una oferta de trabajo presentada por este aspirante.');
            }
        });

        if ($validator->fails())
            return response()->json(['error' => $validator->errors()->first()]);


        $aspirante = Aspirante::find($request->aspirante_id);

        if(!$aspirante)
            return response()->json(['error' => 'No se ha encontrado este aspirante']);

        $oferta = OfertaTrabajo::find($request->oferta_id);

        if(!$aspirante)
            return response()->json(['error' => 'No se ha encontrado este aspirante']);

        $aspiranteTrabajo = AspiranteTrabajo::where('trabajo_id',$oferta->id)
            ->where('aspirante_id',$aspirante->id)
            ->update(['estado' => $request->estado]);

        if($oferta->entidad){
            $this->correoAprobacion($oferta,$aspirante);
        }

        if($request->estado == "Aprobado"){
            $aspirante->tengo_empleo = "Si";
            $aspirante->save();
        }

        return response()->json(['mensaje'=>'Se ha cambiado el estado del aspirante exitosamente']);
    }

    private function correoAprobacion($oferta,$aspirante){
      
        $entidad = $oferta->entidad;
        if(sizeof($entidad->sucursales)==0)
            return 0;

        $entidadSucursal = $entidad->sucursales()->where('lugar_id',$oferta->lugar_id)->get();
        
        if(sizeof($entidadSucursal)==0){
            $entidadSucursal = $entidad->sucursal_principal();
        }else{
            $entidadSucursal = $entidadSucursal[0];        
        }

        $dataMail = [
            'oferta' => $oferta,
            'entidadSucursal' => $entidadSucursal
        ];

        if($aspirante->usuario){
            \Mail::send('mail.trabajos.oferta-aprobada', $dataMail, function ($m) use ($aspirante,$entidad){
                $m->from('info@laguiacundinamarca.com', 'La Guía cundinamarca');
                $m->to($aspirante->usuario->correo,$aspirante->usuario->nombre)
                        ->subject('Felicitaciones!, usted ha sido seleccionado para trabajar en '.$entidad->razon_social);
            });
        }
    }

    public function crear_habilidad(Request $request){

        $aspirante = Aspirante::where('usuario_id', Auth::id())->first();
        $habilidades = ($aspirante->habilidades)? json_decode($aspirante->habilidades):[];
        $id = count($habilidades) + 1;
        array_push($habilidades,[$id, $request->habilidad, $request->nivel, $request->tipo]);
        $aspirante->habilidades = json_encode($habilidades);

        $aspirante->save();
        return response()->json(['mensaje' => "Se ha registrado su información exitosamente!"]);
    }

    public function eliminar_habilidad($id){
        $aspirante = Aspirante::where('usuario_id', Auth::id())->first();
        $habilidades = json_decode($aspirante->habilidades);
        for ($i=0; $i < count($habilidades); $i++) { 
            if ($habilidades[$i][0] == $id) {
                array_splice($habilidades, $i, 1);
            }
        }
        $aspirante->habilidades = json_encode($habilidades);
        $aspirante->save();

        return redirect('/usuario/aspirante/perfil?seccion=perfil&idiomas_tecnologias=1');
    }

    public function eliminar_aplicacion($id){
        $aspirante = Aspirante::where('usuario_id', Auth::id())->first();
        $oferta = AspiranteTrabajo::where("trabajo_id", $id)->where("aspirante_id", $aspirante->id)->delete();
        return redirect('/perfil');
    }

    public function generar_pdf($id){
        $aspirante = Aspirante::find($id);
        $aspirante->foto = json_decode($aspirante->foto);
        $aspirante->telefonos = json_decode($aspirante->telefonos);
        $aspirante->f_nacimiento = ($aspirante->f_nacimiento)? date('M d, Y', strtotime($aspirante->f_nacimiento)):'';
        $aspirante->habilidades = ($aspirante->habilidades)? json_decode($aspirante->habilidades):[];

        $usuario = Usuario::find($aspirante->usuario_id);

        $tecnologias = [];
        $idiomas = [];
        for ($i=0; $i < count($aspirante->habilidades) ; $i++) { 
            if ($aspirante->habilidades[$i][3] == 1) {
                array_push($tecnologias, $aspirante->habilidades[$i]);
            }else{
                array_push($idiomas, $aspirante->habilidades[$i]);
            }
        }

        $experiencias = $aspirante->experiencias;
        $experiencias = $experiencias->map(function($item, $key){
            $item->f_ingreso = date('M Y', strtotime($item->f_ingreso));
            $item->f_salida = date('M Y', strtotime($item->f_salida));
            return $item;
        });

        $estudios = $aspirante->estudios;
        $estudios = $estudios->map(function($item, $key){
            $item->f_ingreso = date('M Y', strtotime($item->f_ingreso));
            $item->f_salida = date('M Y', strtotime($item->f_salida));
            $item["tipo_educacion"] = $item->tipo_educacion;
            return $item;
        });

        $aplicaciones = $aspirante->trabajos;
        $aplicaciones = $aplicaciones->map(function($item, $key){
            $item["ciudad"] = $item->lugar->nombre;
            $item["empresa"] = $item->entidad->razon_social;
            return $item;
        });

        $pdf = PDF::loadView('empleos.hoja-vida-pdf',[
            'ofertasDestacadas' => [],
            'usuario'=> $usuario,
            'aspirante' => $aspirante,
            'aplicaciones' => $aplicaciones,
            'experiencias' => $experiencias,
            'estudios' => $estudios,
            'tecnologias' => $tecnologias,
            'idiomas' => $idiomas,
        ]);
        return $pdf->inline();
    }

    public function generar($id){
        $aspirante = Aspirante::find($id);
        $aspirante->foto = json_decode($aspirante->foto);
        $aspirante->telefonos = json_decode($aspirante->telefonos);
        $aspirante->f_nacimiento = ($aspirante->f_nacimiento)? date('M d, Y', strtotime($aspirante->f_nacimiento)):'';
        $aspirante->habilidades = ($aspirante->habilidades)? json_decode($aspirante->habilidades):[];

        $usuario = Usuario::find($aspirante->usuario_id);

        $tecnologias = [];
        $idiomas = [];
        for ($i=0; $i < count($aspirante->habilidades) ; $i++) { 
            if ($aspirante->habilidades[$i][3] == 1) {
                array_push($tecnologias, $aspirante->habilidades[$i]);
            }else{
                array_push($idiomas, $aspirante->habilidades[$i]);
            }
        }

        $experiencias = $aspirante->experiencias;
        $experiencias = $experiencias->map(function($item, $key){
            $item->f_ingreso = date('M Y', strtotime($item->f_ingreso));
            $item->f_salida = date('M Y', strtotime($item->f_salida));
            return $item;
        });

        $estudios = $aspirante->estudios;
        $estudios = $estudios->map(function($item, $key){
            $item->f_ingreso = date('M Y', strtotime($item->f_ingreso));
            $item->f_salida = date('M Y', strtotime($item->f_salida));
            $item["tipo_educacion"] = $item->tipo_educacion;
            return $item;
        });

        $aplicaciones = $aspirante->trabajos;
        $aplicaciones = $aplicaciones->map(function($item, $key){
            $item["ciudad"] = $item->lugar->nombre;
            $item["empresa"] = $item->entidad->razon_social;
            return $item;
        });
        return response()->view('empleos.hoja-vida-pdf',[
            'ofertasDestacadas' => [],
            'usuario'=> $usuario,
            'aspirante' => $aspirante,
            'aplicaciones' => $aplicaciones,
            'experiencias' => $experiencias,
            'estudios' => $estudios,
            'tecnologias' => $tecnologias,
            'idiomas' => $idiomas,
        ]);
    }

    public function tengo_empleo(){
        $id = Auth::id();
        $aspirante = Aspirante::where("usuario_id", $id)->first();
        if($aspirante->tengo_empleo){
            $aspirante->tengo_empleo = ($aspirante->tengo_empleo == "Si")? "No":"Si";
        }else{
            $aspirante->tengo_empleo = "Si";
        }
        $aspirante->save();
        return response()->json(["mensaje"=>"Se ha cambiado el estado con exito."]);
    }

    private function guardar_foto($id, $file, $i){
        $ruta_foto = $file->storeAs(
            'avatars/usuario_'.$id, 'foto-'.$i.'.'.$file->getClientOriginalExtension()
        );

        return $ruta_foto;
    }
}