<?php

namespace App\Http\Controllers\PanelUsuarios\Entidades;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Actividad;
use App\Archivo;
use App\Comentario;
use App\Entidad;
use App\Telefono;
use App\Lugar;
use App\Usuario;
use App\Sucursal;
use App\Trabajo;
use App\TipoContrato;

use Auth;
use Validator;
use Storage;
use DB;

class EntidadController extends Controller{
    
    public function edit(){
        $usuario = Auth::user();
        $entidad = $usuario->entidad;
        $identificacion = explode("-", $entidad->nit);

        return view('panel-usuarios.entidades.form-info1-empresa',[
            'identificacion' => $identificacion,
            'entidad' => $entidad,
            'usuario' => $usuario,
        ]);
    }
    
    public function mostrar_panel(){
        return redirect('/usuario/noticia');

        $usuario = Auth::user();
        $campos = 0;
        $total = 3; 
        
        $entidad = Entidad::where('entidades.usuario_id', $usuario->id)->first();
        $archivos = Archivo::where('entidad_id', $entidad->id)->orderBy('tipo', 'asc')->get();
        $ofertas = Trabajo::join('lugares', 'trabajos.lugar_id', '=', 'lugares.id')
            ->select('trabajos.*', 'lugares.nombre as ciudad')
            ->where('entidad_id', $entidad->id)->get();


        $eventos = $entidad->usuario->eventos;


        $sucursales = Sucursal::join('lugares', 'sucursales.lugar_id', '=', 'lugares.id')
            ->select('sucursales.id', 'sucursales.direccion', 'sucursales.tipo', 'lugares.nombre')
            ->where('sucursales.entidad_id', $entidad->id)
            ->get();


        if(sizeof($sucursales))
            $idLugar = $entidad->sucursal_principal()->lugar_id;
        else
            $idLugar = null;


        $identificacion = explode("-", $entidad->nit);

        $total -= ($entidad->zip)? 0:1;
        $total -= ($entidad->encargado)? 0:1;
        $total -= ($entidad->pregunta_empleado)? 0:1;
        $total -= ($entidad->tipo_cobertura)? 0:1;
        $total -= ($entidad->recibir_boletines)? 0:1;
        $total -= ($entidad->recibir_notificaciones)? 0:1;
        $total -= ($entidad->palabras_clave)? 0:1;
        $total -= ($entidad->medios_pago)? 0:1;
        $total -= ($entidad->servicios_adicionales)? 0:1;
        $total -= ($entidad->estilos)? 0:1;

        $campos_entidad = collect($entidad)->values()->all();

        for ($i=0; $i < count($campos_entidad); $i++) {
            $campos += ($campos_entidad[$i])?1:0;
            $total += 1;
        }
        
        $campos += ($sucursales->count()>0)? 3:0;

        $soloMunicipios = Lugar::municipios()->orderBy('nombre','asc')->get();

        if(Auth::user()->rol_id == 3){
            $lugar = Lugar::find( $entidad->sucursal_principal()->lugar_id );
        }else{
            $lugar = new Lugar;
        }

        return view('panel-usuarios.entidades.inicio',[
            'soloMunicipios' => $soloMunicipios,
            'usuario'=> $usuario,
            'identificacion'=> $identificacion,
            'entidad' => $entidad,
            'sucursales' => $sucursales,
            'idLugar' => $idLugar,
            'archivos' => $archivos,
            'ofertas' => $ofertas,
            'eventos' => $eventos,
            'lugar' => $lugar,
            'porcentaje' => ceil(($campos/$total)*100)
        ]);
    }
    
    public function actualizar_contacto(Request $request){
        //** params nombre, apellido, cargo, telefono, correo
        $usuario_sesion = Auth::user();

        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string',
            'apellido' => 'required|string',
            'cargo' => 'required|string',
            'telefono' => 'required|string',
            'correo' => 'required|email|unique:entidades,correo_privado,'.$usuario_sesion->id.',usuario_id'            
        ]);

        if (!$validator->fails()) {

            $reporte = DB::transaction(function () use($request, $usuario_sesion) {

                $usuario = Usuario::find($usuario_sesion->id);
                $empresa = Entidad::where("usuario_id",$usuario_sesion->id)->first();

                $usuario->nombre = $request->nombre;
                $usuario->apellido = $request->apellido;
                $usuario->correo = $request->correo;

                $usuario->save();

                $empresa->cargo = $request->cargo;
                $empresa->telefono_encargado = $request->telefono;
                $empresa->correo_privado = $request->correo;

                $empresa->save();

            });

            return response()->json(['mensaje' => 'Se ha actualizado su información correctamente!']);

        }else{
            return response()->json(['error' => $validator->errors()->first()]);
        }
    }

    public function actualizar_empresa(Request $request){

        $usuario_sesion = Auth::user();

        $validator = Validator::make($request->all(), [
            'nit' => 'nullable|string',
            'correo' => 'required|email|unique:entidades,correo_comercial,'.$usuario_sesion->id.',usuario_id',
            'web' => 'nullable|url',
            'descripcion' => 'nullable|string',
            'numero_empleados' => 'nullable|integer|min:0',     
            'logo' => 'nullable|max:4500',
            'color' => 'nullable|in:rojo,amarillo,azul,verde',
        ]);

        $validator->after(function($validator) use($request){
            if($request->hasFile('logo')){
                $mime = $request->logo->getMimeType();
                if($mime!="image/png" && $mime!="image/jpeg" && $mime!="image/gif"){
                    $validator->errors()->add('archivo', 'El archivo no es una imagen, por favor seleccione un archivo de tipo imagen.');
                }
            }
        });

        if ($validator->fails()){
            if (request()->is('api/*'))
                return response()->json(['error' => $validator->errors()->first()]);
            else
                return back()->with(['errors' => $validator->errors(), 'datos'=>$request->all() ]);
        }

        try{            
            DB::connection()->getPdo()->beginTransaction();

            $empresa = Entidad::where("usuario_id",$usuario_sesion->id)->first();

            $empresa->razon_social = $request->razon_social;
            $empresa->nit = $request->nit;
            $empresa->correo_comercial = $request->correo;
            $empresa->web = $request->web;
            $empresa->descripcion = $request->descripcion;
            $empresa->numero_empleados = $request->numero_empleados;
            $empresa->vehiculos = 0;
            $empresa->estilos = ($request->has('color') && $usuario_sesion->tipo == 2)? $request->color:null;

            $empresa->save();

            if($empresa->tipo == "Empresa" && $empresa->tipo_negocio==0 && sizeof($empresa->sucursales)){
                $sucursal = $empresa->sucursales[0];

                $telefonos = [
                    $request->telefono1,
                    $request->telefono2,
                    $request->celular1,
                    $request->celular2,
                ];

                $telefonos = json_encode($telefonos);
                $sucursal->telefonos = $telefonos;
                $sucursal->save();
            }

            if($request->hasFile('logo')){
                $carpeta_logo = 'usuario_'.$usuario_sesion->id.'/logo.'.$request->file('logo')->getClientOriginalExtension();
                $logo = file_get_contents($request->file('logo')->getRealPath());
                Storage::put($carpeta_logo, $logo);                 
                $ruta_logo = '/file/'.$usuario_sesion->id.'/logo.'.$request->file('logo')->getClientOriginalExtension();
                $empresa->logo = $ruta_logo;

                $empresa->save();
            }

            DB::connection()->getPdo()->commit();

            if (request()->is('api/*'))
                return response()->json(['mensaje' => 'Se ha actualizado su información exitosamente!' ]);
            else
                return back()->with(['msjExito' => 'Se ha actualizado su información correctamente!']);

        }catch(\Exception $e){
            DB::connection()->getPdo()->rollBack();

            if (request()->is('api/*'))
                return response()->json(['error' => 'Error al actualizar sus datos' ]);
            else
                return back()->with(['error' => 'Error al actualizar sus datos' ]);
        }

    }

    public function actualizar_comercial(Request $request){

        $usuario_sesion = Auth::user();

        $validator = Validator::make($request->all(), [
            'categorias' => 'required|array',
            'sector' => 'string',
            'ofrece' => 'required_if:check_ofrece,1|string',
            'requiere' => 'required_if:check_requiere,1|string'
        ]);

        if ($validator->fails())
            return response()->json(['error' => $validator->errors()->first()]);

        try{            
            DB::connection()->getPdo()->beginTransaction();

            $empresa = Entidad::where("usuario_id",$usuario_sesion->id)->first();

            $empresa->sector = $request->sector;

            $nuevosOfrece = $nuevosRequiere = "";

            if($empresa->ofrece != $request->ofrece)
                $nuevosOfrece = $request->ofrece;

            if($empresa->requiere != $request->requiere)
                $nuevosRequiere = $request->requiere;

            if( !$request->has('check_ofrece') )
                $request->ofrece = "";

            if( !$request->has('check_requiere') )
                $request->requiere = "";

            if(strlen($nuevosOfrece) || strlen($nuevosRequiere)){
                $dataMail = [
                    'usuario' => Auth::user(),
                    'ofrece' => $nuevosOfrece,
                    'requiere' => $nuevosRequiere
                ];

                \Mail::send('mail.nuevos-produservicios',$dataMail, function ($m){
                    $m->from('info@laguiacundinamarca.com', 'La Guía Cundinamarca');
                    $m->to('info@laguiacundinamarca.com','La Guía Cundinamarca')->subject('Nuevos productos/servicios registrados- La Guía Cundinamarca');
                });
            }
            $empresa->ofrece = $request->ofrece;
            $empresa->requiere = $request->requiere;

            //return $request->categorias;
            $empresa->save();

            \DB::table('entidades_produservicios')->where('entidad_id',$empresa->id)->delete();
            foreach (explode(",",$request->ofrece_tags) as $key => $produservicioId) {
                \DB::table('entidades_produservicios')->insert(['entidad_id' => $empresa->id, 'produservicio_id' => $produservicioId, 'ofrece' => 1]);
            }

            foreach (explode(",",$request->requiere_tags) as $key => $produservicioId) {
                \DB::table('entidades_produservicios')->insert(['entidad_id' => $empresa->id, 'produservicio_id' => $produservicioId, 'ofrece' => 2]);
            }

            \DB::table('entidades_actividades')->where('entidad_id',$empresa->id)->delete();
            foreach ($request->categorias as $key => $categoria) {
                \DB::table('entidades_actividades')->insert(['entidad_id' => $empresa->id, 'actividad_id' => $categoria]);
            }

            DB::connection()->getPdo()->commit();
            return response()->json(['mensaje' => 'Se ha actualizado su información correctamente!']);
        }catch(\Exception $e){
            DB::connection()->getPdo()->rollBack();
            return response()->json(['error' => $validator->errors()->first()]);
        }
    }

    public function cambiar_logo(Request $request){

        $usuario = Auth::user();

        $validator = Validator::make($request->all(), [   
            'foto' => 'max:4500',
        ]);

        $validator->after(function($validator) use($request){
            if($request->hasFile('foto')){
                $mime = $request->foto->getMimeType();
                if($mime!="image/png" && $mime!="image/jpeg" && $mime!="image/gif"){
                    $validator->errors()->add('archivo', 'El archivo no es una imagen, por favor seleccione un archivo de tipo imagen.');
                }
            }
        });

        if ($validator->fails()) {
            if (request()->is('api/*')) {
                return response()->json([ 'error' => $validator->errors()->first() ]);
            }else{
                $error = $validator->errors()->first();
                return redirect()->back()->withErrors($error);
            }
        }

        try{            
            DB::connection()->getPdo()->beginTransaction();

            $empresa = $usuario->entidad;
            $ruta_logo = "";

            if($request->hasFile('foto')){
                $rutaFoto = '/multimedia/another_files/'.$this->guardar_foto($id, $request->file("foto"), 0);
                $empresa->logo = $ruta_logo;
                $empresa->save();
            }

            DB::connection()->getPdo()->commit();

            if (request()->is('api/*'))
                return response()->json(['mensaje' => "Se ha actualizado su imagen de perfil exitosamente!", 'foto' => $rutaFoto ]);
            else
                return back()->with(['msjExito' => "Se ha actualizado su imagen de perfil exitosamente!" ]);

        }catch(\Exception $e){
            DB::connection()->getPdo()->rollBack();

            if (request()->is('api/*'))
                return response()->json(['error' => "Error al cambiar la imagen de perfil." ]);
            else
                return back()->with(['msjError' => "Error al cambiar la imagen de perfil." ]);
        }

    }

    private function guardar_foto($id, $file, $i){
        $ruta_foto = $file->storeAs(
            'avatars/usuario_'.$id, 'foto-'.$i.'.'.$file->getClientOriginalExtension()
        );

        return $ruta_foto;
    }
}
