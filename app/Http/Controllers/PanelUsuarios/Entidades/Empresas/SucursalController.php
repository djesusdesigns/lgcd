<?php

namespace App\Http\Controllers\PanelUsuarios\Entidades\Empresas;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Sucursal;
use App\Entidad;

use Auth;
use Validator;
use Storage;

class SucursalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $usuario = Auth::id();
        $entidad = Entidad::where('usuario_id', $usuario)->first();
        $entidad_id = $entidad->id;

        $sucursales = Sucursal::where('entidad_id', $entidad_id)->get();
        return response()->json(['error' => null, 'datos' => $sucursales, 'mensaje'=>'Se ha consultado el listado de sucursales exitosamente!']);
    }

    public function create(){}

    public function crear(Request $request){
        $usuario = Auth::id();
        $entidad = Entidad::where('usuario_id', $usuario)->first();
        $principal = Sucursal::where('entidad_id', $entidad->id)->where('tipo', 'principal')->first();

        $validator = Validator::make($request->all(), [
            'direccion' => 'required|string',
            'municipio' => 'required|string',
            'telefonos' => 'required|string',
            'vehiculos' => 'required|integer|min:0',
            'latitud' => 'required|numeric',
            'longitud' => 'required|numeric',
            'hora01' => 'required',
            'hora02' => 'required',
            'hora11' => 'required',
            'hora12' => 'required',
            'nueva-sucursal-telefono1' => 'size:7',
            'nueva-sucursal-telefono2' => 'size:7',
            'nueva-sucursal-celular1' => 'size:10',
            'nueva-sucursal-celular2' => 'size:10',
            'domicilio' => 'required|in:1,0',
            'estacionamiento' => 'required|in:1,0',
            'foto_sucursal' => 'required_if:tipo,Principal|max:1500',
            'tipo' => 'required|in:Principal,Secundaria',
        ]);

        $validator->after(function($validator) use($principal, $request){
            if ($principal && $request->tipo == "Principal") {
                $validator->errors()->add('Tipo', 'Ya existe una sucursal principal, por favor cambie el tipo de sucursal o actualice la sucursal principal');
            }
            if (!$principal && $request->tipo != "Principal") {
                $validator->errors()->add('Tipo', 'No tiene ninguna sucursal, por favor ingrese primero la sucursal principal');
            }
        });
        
        $sucursal = new Sucursal;

        if (!$validator->fails()) {

            $horario = array();
            array_push( $horario, array( $request->hora01, $request->hora02 ) );
            array_push( $horario, array( $request->hora11, $request->hora12 ) );
            $horario = json_encode($horario);

            $sucursal->entidad_id = $entidad->id;
            $sucursal->direccion = $request->direccion;
            $sucursal->telefonos = $request->telefonos;
            $sucursal->lugar_id = $request->municipio;
            $sucursal->tipo = $request->tipo;
            $sucursal->domicilio = $request->domicilio;
            $sucursal->estacionamiento = $request->estacionamiento;
            $sucursal->horario = $horario;
            $sucursal->latitud = $request->latitud;
            $sucursal->longitud = $request->longitud;
            $sucursal->vehiculos = $request->vehiculos;

            $sucursal->save();

            if($request->hasFile("foto_sucursal")){

                $carpeta_sucursal = 'usuario_'.$usuario.'/sucursal_'.$sucursal->id.'.'.$request->file('foto_sucursal')->getClientOriginalExtension();
                $sucursal_image = file_get_contents($request->file('foto_sucursal')->getRealPath());
                Storage::put($carpeta_sucursal, $sucursal_image);                 
                $ruta_sucursal = '/file/'.$usuario.'/sucursal_'.$sucursal->id.'.'.$request->file('foto_sucursal')->getClientOriginalExtension();
                $sucursal->imagen = $ruta_sucursal;

                $sucursal->save();

            }else{
                if(!$sucursal->imagen && $sucursal->tipo != "Principal"){
                    $sucursal_principal = Sucursal::select('sucursales.imagen')->where('entidad_id', $entidad->id)->where('tipo','Principal')->first();

                    if($sucursal_principal && $sucursal_principal->imagen){
                        $sucursal->imagen = $sucursal_principal->imagen;
                    }

                    $sucursal->save();
                }
            }
            
            return response()->json(['mensaje' => 'Se ha creado la sucursal correctamente!']);
        }else{
            return response()->json(['error' => $validator->errors()->first()]);
        } 
    }

    public function consultar($id){
        $usuario = Auth::id();
        $entidad = Entidad::where('usuario_id', $usuario)->first();

        $sucursal = Sucursal::find($id);

        if($sucursal->entidad_id == $entidad->id){
            return response()->json(['datos' => $sucursal, 'mensaje'=>'Se ha consultado la sucursal exitosamente!']);
        }else{
            return response()->json(['error' => 'Ocurrió un error consultando los datos de la sucursal, intentelo nuevamente']);
        }
    }

    public function edit($id){}

    public function actualizar(Request $request){
        $usuario = Auth::id();
        $entidad = Entidad::where('usuario_id', $usuario)->first();
        $principal = Sucursal::where('entidad_id', $entidad->id)->where('tipo', 'principal')->first();

        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:sucursales,id',
            'direccion' => 'required|string',
            'municipio' => 'required|string',
            'vehiculos' => 'required|integer|min:0',
            'telefonos' => 'required|string',
            'latitud' => 'required|numeric',
            'longitud' => 'required|numeric',
            'hora01' => 'required',
            'hora02' => 'required',
            'hora11' => 'required',
            'hora12' => 'required',
            'sucursal-telefono1' => 'size:7',
            'sucursal-telefono2' => 'size:7',
            'sucursal-celular1' => 'size:10',
            'sucursal-celular2' => 'size:10',
            'domicilio' => 'required|in:1,0',
            'estacionamiento' => 'required|in:1,0',
            'foto_sucursal' => 'max:1500'
        ]); 

        $validator->after(function($validator) use($principal, $request){
            if ($principal && $request->tipo == "Principal" && $principal->id != $request->id) {
                $validator->errors()->add('Tipo', 'Ya existe una sucursal principal, por favor cambie el tipo de sucursal o actualice la sucursal principal');
            }
        });
        

        if (!$validator->fails()) {

            $sucursal = Sucursal::where('id',$request->id)->first();

            $horario = array();
            array_push( $horario, array( $request->hora01, $request->hora02 ) );
            array_push( $horario, array( $request->hora11, $request->hora12 ) );
            $horario = json_encode($horario);

            $sucursal->entidad_id = $entidad->id;
            $sucursal->direccion = $request->direccion;
            $sucursal->telefonos = $request->telefonos;
            $sucursal->lugar_id = $request->municipio;
            $sucursal->domicilio = $request->domicilio;
            $sucursal->estacionamiento = $request->estacionamiento;
            $sucursal->horario = $horario;
            $sucursal->latitud = $request->latitud;
            $sucursal->longitud = $request->longitud;
            $sucursal->vehiculos = $request->vehiculos;

            $sucursal->save();

            if($request->hasFile("foto_sucursal")){

                $carpeta_sucursal = 'usuario_'.$usuario.'/sucursal_'.$sucursal->id.'.'.$request->file('foto_sucursal')->getClientOriginalExtension();
                $sucursal_image = file_get_contents($request->file('foto_sucursal')->getRealPath());
                Storage::put($carpeta_sucursal, $sucursal_image);                 
                $ruta_sucursal = '/file/'.$usuario.'/sucursal_'.$sucursal->id.'.'.$request->file('foto_sucursal')->getClientOriginalExtension();
                $sucursal->imagen = $ruta_sucursal;

                $sucursal->save();

            }else{
                if(!$sucursal->imagen && $sucursal->tipo != "Principal"){
                    $sucursal_principal = Sucursal::select('sucursales.imagen')->where('entidad_id', $entidad->id)->where('tipo','Principal')->first();

                    if($sucursal_principal && $sucursal_principal->imagen){
                        $sucursal->imagen = $sucursal_principal->imagen;
                    }

                    $sucursal->save();
                }
            }
            
            return response()->json(['mensaje' => 'Se ha actualizado la sucursal correctamente!']);

        }else{
            return response()->json(['error' => $validator->errors()->first()]);
        }

        $sucursal->save();        
    }

    public function eliminar($id){
        $usuario = Auth::id();
        $entidad = Entidad::where('usuario_id', $usuario)->first();
        $sucursal = Sucursal::find($id);

        if($sucursal->entidad_id == $entidad->id){
            if($sucursal->tipo == "Principal"){
                return response()->json(['error' => 'La sucursal principal no se puede eliminar']);
            }else{
                $sucursal->delete();
                return response()->json(['mensaje' => 'Se ha eliminado la sucursal correctamente!']);
            }
        }else{
            return response()->json(['error' => 'Ocurrió un error eliminando la sucursal, intentelo nuevamente']);
        }
    }

    private function verificar_principal($entidad_id){
        $sucursal = Sucursal::where('entidad_id',$entidad_id)->
                    where('tipo','Principal')->first();
        if($sucursal){
            return true;
        }else{
            return false;
        }
    }
}
