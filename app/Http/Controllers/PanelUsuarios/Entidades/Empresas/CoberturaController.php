<?php

namespace App\Http\Controllers\PanelUsuarios\Entidades\Empresas;

use Illuminate\Http\Request;

use App\Cobertura;
use App\Entidad;
use Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CoberturaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //listado de coberturas por entidad
    public function index()
    {
        $usuario = Auth::id();
        $entidad = Entidad::where('usuario_id', $usuario)->first();
        $entidad_id = $entidad->id;

        $coberturas = Cobertura::where('entidad_id', $entidad_id)->get();
        return response()->json(['error' => null, 'datos' => $coberturas, 'mensaje'=>'Se ha consultado el listado de coberturas exitosamente!']);
    }

    //se guardan las coberturas de una entidad
    public function store(Request $request)
    {
        $respuesta = $this->guardar_actualizar($request->input('lugares'));
        return response()->json($respuesta);
    }
    //Se actualizan las coberturas de una entidad
    public function update(Request $request)
    {
        $respuesta = $this->guardar_actualizar($request->input('lugares'));
        return response()->json($respuesta);
    }

    private function guardar_actualizar($listado)
    {
        //Captura y valida parámetros

        $usuario = Auth::id();
        $entidad = Entidad::where('usuario_id', $usuario)->first();
        $entidad_id = $entidad->id;
        $lugares = json_decode($listado);
        if(!is_array($lugares)){
            return array('error' => 'El parámetro lugares es incorrecto!', 'datos' => null);
        }

        //Borra la cobertura anterior

        Cobertura::where('entidad_id', '=', $entidad_id)->delete();

        //Guarda las nuevas coberturas

        $datos = array();
        foreach ($lugares as $lugar) {
            $fila = ["lugar_id"=>$lugar,"entidad_id"=>$entidad_id];
            array_push($datos, $fila);
        }
        Cobertura::insert($datos);
        return array('error' => null, 'datos' => null, 'mensaje'=>'Se han guardado los cambios correctamente!');

    }
}
