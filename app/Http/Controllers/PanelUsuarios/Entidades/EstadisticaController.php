<?php

namespace App\Http\Controllers\PanelUsuarios\Entidades;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Auth;
use Validator;
use Storage;
use DB;
use App;

use Excel;

use App\Sucursal;
use App\Entidad;
use App\Aspirante;
use App\Trabajo;
use App\Lugar;
use App\EntidadActividad;
use App\Produservicio;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class EstadisticaController extends Controller{    
    
    //Muestra la lista de registros
    public function entidades_activas(Request $request,$idLugar){

        $estadistica = Entidad::select(DB::raw("COUNT(distinct entidades.id) as num_entidades, date_format(entidades.created_at, '%Y-%m') as mes"))
            ->join('sucursales','sucursales.entidad_id','=','entidades.id')
            ->where('sucursales.lugar_id', $idLugar)
            ->where('entidades.tipo', 'Empresa')
            ->get();

        $estadistica = $estadistica->reverse();
        
        $categorias = $estadistica->map(function($item,$key){
            return $item->mes;
        });

        $series = $estadistica->map(function($item,$key){
            return (integer)$item->num_entidades;
        });

        $total = $series->sum();

        return response()->json([
            'mensaje' => 'Se ha consultado la estadistica exitosamente!',
            'datos'=>[
                "categorias"=>$categorias,
                "series"=>$series,
                "total"=>$total
            ]
        ]);
    }

    public function entidades_activas_excel($idLugar){

        $data = Entidad::leftJoin('sucursales', 'entidades.id', '=', 'sucursales.entidad_id')
            ->select('entidades.razon_social','entidades.nit','entidades.tipo_negocio as Empresa_formal')
            ->addSelect('entidades.encargado','entidades.sector','entidades.encargado','entidades.telefono_encargado')
            ->addSelect('entidades.cargo','entidades.correo_privado','entidades.descripcion','entidades.numero_empleados')
            ->addSelect('entidades.web','entidades.correo_comercial','entidades.ofrece','entidades.requiere')
            ->addSelect('sucursales.direccion','sucursales.telefonos','sucursales.horario')
            ->where('entidades.tipo','Empresa')
            ->where('sucursales.lugar_id',$idLugar)
            ->orderBy('sucursales.tipo','asc')
            ->groupBy('entidades.id')
            ->get()
            ->toArray();

        foreach ($data as $key => $d) {
            if(!is_null($d['telefonos'])){
                $d['telefonos'] = json_decode($d['telefonos']);
                $data[$key]['telefonos'] = join($d['telefonos'],',');
            }

            if(!is_null($d['horario'])){
                $d['horario'] = json_decode($d['horario']);
                $data[$key]['horario'] = 'Lunes a sabado de '.$d['horario'][0][0].' a '.$d['horario'][0][1].', domingos y festivos de '.$d['horario'][1][0].' a '.$d['horario'][1][1];
            }
        }

        if(sizeof($data) == 0)
            $data = ['No hay datos para esta consulta'];

        $to_export = [];

        foreach ($data as $r)
            array_push($to_export, (array) $r);

        Excel::create('Entidades_activas', function($excel) use(&$to_export) {
            $excel->sheet('Reporte', function($sheet) use(&$to_export) {
                $sheet->fromArray($to_export);
            });
        })->download('xls');

        return dd($data);
    }

    //Muestra la lista de registros
    public function aspirantes(Request $request,$idLugar){
        $aspirantes_edad = DB::select("SELECT name, COUNT(*) as y FROM aspirantes INNER JOIN (
            SELECT 
                0 as start, 19 as 'end', '0-19 años' as name
            UNION ALL 
            SELECT
                20, 29, '20-29 años'
            UNION ALL
            SELECT
                30, 39, '30-39 años'
            UNION ALL
            SELECT
                40, 49, '40-49 años'
            UNION ALL
            SELECT
                50, 59, '50-59 años'
            UNION ALL
            SELECT
                60, 150, '60+ años'
        ) sub  ON TIMESTAMPDIFF(YEAR, f_nacimiento, CURDATE()) BETWEEN start AND end  WHERE lugar_id=".$idLugar." AND (tengo_empleo='No' OR tengo_empleo is NULL) GROUP BY name WITH ROLLUP");

        $aspirantes_edad = collect($aspirantes_edad);
        $total = $aspirantes_edad->where('name',null)->first();
        $aspirantes_edad = $aspirantes_edad->map( function($item, $key) use ($total){
            $item->y = ($item->name)?floor($item->y/$total->y*10000)/100:$total->y;
            $item->y = (int)$item->y;
            return $item;
        });

        $aspirantes_edad->pop();

        $aspirantes_sexo = Aspirante::select(DB::raw("IF(aspirantes.genero IS NULL,'No especifica',aspirantes.genero) as name,COUNT(*) as y"))
            ->groupBy(DB::raw("aspirantes.genero"))
            ->where('aspirantes.lugar_id', $idLugar)
            ->where(function ($query) {
                    $query->whereNull('tengo_empleo')
                          ->orWhere('tengo_empleo','No');
            })
            ->get();

        $aspirantes_sexo = collect($aspirantes_sexo);

        $total = 0;
        foreach ($aspirantes_sexo as $key => $as) $total+=$as->y;
        if($total == 0) $total = 1;

        $aspirantes_sexo = $aspirantes_sexo->map( function($item, $key) use ($total){
            $item->y = ($item->name)?floor($item->y/$total*10000)/100:$total;
            $item->y = (int)$item->y;
            return $item;
        });

        $aspirantes_sexo->pop();

        $aspirantes_educacion = Aspirante::leftjoin('tipo_educacion','aspirantes.tipo_educacion_id','=','tipo_educacion.id')
            ->select(DB::raw("IF(aspirantes.tipo_educacion_id IS NULL,'No especifica',tipo_educacion.nombre) as name,COUNT(*) as y"))
            ->where('aspirantes.lugar_id', $idLugar)
            ->groupBy("aspirantes.tipo_educacion_id")
            ->where(function ($query) {
                    $query->whereNull('tengo_empleo')
                          ->orWhere('tengo_empleo','No');
            })
            ->get();

        $total = 0;
        foreach ($aspirantes_educacion as $key => $ae) $total+=$ae->y;
        if($total == 0) $total = 1;

        $aspirantes_educacion = $aspirantes_educacion->map( function($item, $key) use ($total){
            $item->y = floor($item->y/$total*10000)/100;
            $item->y = (int)$item->y;
            return $item;
        });

        $aspirantes_profesion = Aspirante::leftjoin('profesiones','aspirantes.profesion_id','=','profesiones.id')
            ->select(DB::raw("IF(aspirantes.profesion_id IS NULL,'Otra',profesiones.nombre) as name"), DB::raw("COUNT(*) as y"))
            ->where('aspirantes.lugar_id', $idLugar)
            ->groupBy("profesiones.id")
            ->where(function ($query) {
                    $query->whereNull('tengo_empleo')
                          ->orWhere('tengo_empleo','No');
            })
            ->get();

        $total = 0;
        foreach ($aspirantes_profesion as $key => $ap) $total+=$ap->y;
        if($total == 0) $total = 1;
        $aspirantes_profesion = $aspirantes_profesion->map( function($item, $key) use ($total){
            $item->y = floor($item->y/$total*10000)/100;
            $item->y = (int)$item->y;
            return $item;
        });

        $datosGrafico = [
            "edad"=>$aspirantes_edad,
            "sexo"=>$aspirantes_sexo,
            "educacion"=> $aspirantes_educacion,
            "profesion"=> $aspirantes_profesion
        ];

        return response()->json([
            'mensaje' => 'Se ha consultado la estadistica exitosamente!',
            'datos'=> $datosGrafico]
        );
    }

    public function aspirantes_excel($idLugar){

        $data = Aspirante::where('aspirantes.lugar_id',$idLugar)
                    ->where(function ($query) {
                        $query->whereNull('tengo_empleo')
                              ->orWhere('tengo_empleo','No');
                    })
                    ->select('usuarios.nombre','usuarios.apellido','aspirantes.f_nacimiento','aspirantes.nacionalidad','aspirantes.genero')
                    ->addSelect(DB::raw('0 as telefono'),DB::raw('0 as celular'),'aspirantes.direccion','lugares.nombre as municipio')
                    ->addSelect('aspirantes.estado_civil','aspirantes.direccion','lugares.nombre as municipio')
                    ->addSelect('aspirantes.discapacidad','aspirantes.licencia_conduccion','aspirantes.telefonos')
                    ->addSelect('aspirantes.perfil_laboral','aspirantes.profesion_otra','profesiones.nombre as profesion','aspirantes.experiencia','aspirantes.salario as salario_esperado')
                    ->addSelect('tipo_educacion.nombre as nivel_educativo','aspirantes.disp_viaje','aspirantes.disp_cambio_hogar')
                    ->join('usuarios','aspirantes.usuario_id','=','usuarios.id')
                    ->join('lugares','aspirantes.lugar_id','=','lugares.id')
                    ->join('tipo_educacion','aspirantes.tipo_educacion_id','=','tipo_educacion.id')
                    ->leftjoin('profesiones','aspirantes.profesion_id','=','profesiones.id')
                    ->get()
                    ->toArray();

        
        foreach ($data as $key => $d) {
            if(strlen($d['telefonos'])){
                $telefonos = json_decode($d['telefonos']);
                $data[$key]['telefono']= $telefonos->telefono;
                $data[$key]['celular']= $telefonos->celular;
            }else{
                $data[$key]['telefono']= "";
                $data[$key]['celular']= "";
            }

            unset($data[$key]['telefonos']);
        }

        if(sizeof($data) == 0)
            $data = ['No hay datos para esta consulta'];
        
        $to_export = [];

        foreach ($data as $r)
            array_push($to_export, (array) $r);

        Excel::create('Aspirantes', function($excel) use(&$to_export) {
            $excel->sheet('Reporte', function($sheet) use(&$to_export) {
                $sheet->fromArray($to_export);
            });
        })->download('xls');

        return dd($data);
    }

        //Muestra la lista de registros
    
    public function personas_trabajando(Request $request,$idLugar){
        $aspirantes_edad = DB::select("SELECT name, COUNT(*) as y FROM aspirantes INNER JOIN (
            SELECT 
                0 as start, 19 as 'end', '0-19 años' as name
            UNION ALL 
            SELECT
                20, 29, '20-29 años'
            UNION ALL
            SELECT
                30, 39, '30-39 años'
            UNION ALL
            SELECT
                40, 49, '40-49 años'
            UNION ALL
            SELECT
                50, 59, '50-59 años'
            UNION ALL
            SELECT
                60, 150, '60+ años'
            ) sub  ON TIMESTAMPDIFF(YEAR, f_nacimiento, CURDATE()) BETWEEN start AND end  WHERE lugar_id=".$idLugar." AND tengo_empleo='Si' GROUP BY name WITH ROLLUP");

        $aspirantes_edad = collect($aspirantes_edad);
        $total = $aspirantes_edad->where('name',null)->first();
        $aspirantes_edad = $aspirantes_edad->map( function($item, $key) use ($total){
            $item->y = ($item->name)?floor($item->y/$total->y*10000)/100:$total->y;
            $item->y = (int)$item->y;
            return $item;
        });

        $aspirantes_edad->pop();

        $aspirantes_sexo = Aspirante::select(DB::raw("IF(aspirantes.genero IS NULL,'No especifica',aspirantes.genero) as name,COUNT(*) as y"))
            ->where('aspirantes.lugar_id', $idLugar)
            ->groupBy(DB::raw("aspirantes.genero"))
            ->where('tengo_empleo','Si')
            ->get();

        $aspirantes_sexo = collect($aspirantes_sexo);

        $total = 0;
        foreach ($aspirantes_sexo as $key => $as) $total+=$as->y;
        if($total == 0) $total = 1;

        $aspirantes_sexo = $aspirantes_sexo->map( function($item, $key) use ($total){
            $item->y = ($item->name)?floor($item->y/$total*10000)/100:$total;
            $item->y = (int)$item->y;
            return $item;
        });

        $aspirantes_sexo->pop();

        $aspirantes_educacion = Aspirante::leftjoin('tipo_educacion','aspirantes.tipo_educacion_id','=','tipo_educacion.id')
            ->select(DB::raw("IF(aspirantes.tipo_educacion_id IS NULL,'No especifica',tipo_educacion.nombre) as name,COUNT(*) as y"))
            ->where('aspirantes.lugar_id', $idLugar)
            ->groupBy("aspirantes.tipo_educacion_id")
            ->where('tengo_empleo','Si')
            ->get();

        $total = 0;
        foreach ($aspirantes_educacion as $key => $ae) $total+=$ae->y;
        if($total == 0) $total = 1;

        $aspirantes_educacion = $aspirantes_educacion->map( function($item, $key) use ($total){
            $item->y = floor($item->y/$total*10000)/100;
            $item->y = (int)$item->y;
            return $item;
        });

        $aspirantes_profesion = Aspirante::leftjoin('profesiones','aspirantes.profesion_id','=','profesiones.id')
            ->select(DB::raw("IF(aspirantes.profesion_id IS NULL,'Otra',profesiones.nombre) as name"), DB::raw("COUNT(*) as y"))
            ->where('aspirantes.lugar_id', $idLugar)
            ->groupBy("profesiones.id")
            ->where('tengo_empleo','Si')
            ->get();

        $total = 0;
        foreach ($aspirantes_profesion as $key => $ap) $total+=$ap->y;
        if($total == 0) $total = 1;
        $aspirantes_profesion = $aspirantes_profesion->map( function($item, $key) use ($total){
            $item->y = floor($item->y/$total*10000)/100;
            $item->y = (int)$item->y;
            return $item;
        });

        $datosGrafico = [
            "edad"=>$aspirantes_edad,
            "sexo"=>$aspirantes_sexo,
            "educacion"=> $aspirantes_educacion,
            "profesion"=> $aspirantes_profesion
        ];

        return response()->json([
            'mensaje' => 'Se ha consultado la estadistica exitosamente!',
            'datos'=> $datosGrafico]
        );
    }

    public function personas_trabajando_excel($idLugar){

        $data = Aspirante::where('aspirantes.lugar_id',$idLugar)
                    ->where('tengo_empleo','Si')
                    ->select('usuarios.nombre','usuarios.apellido','aspirantes.f_nacimiento','aspirantes.nacionalidad','aspirantes.genero')
                    ->addSelect(DB::raw('0 as telefono'),DB::raw('0 as celular'),'aspirantes.direccion','lugares.nombre as municipio')
                    ->addSelect('aspirantes.estado_civil','aspirantes.direccion','lugares.nombre as municipio')
                    ->addSelect('aspirantes.discapacidad','aspirantes.licencia_conduccion','aspirantes.telefonos')
                    ->addSelect('aspirantes.perfil_laboral','aspirantes.profesion_otra','aspirantes.profesion_otra','aspirantes.experiencia')
                    ->addSelect('aspirantes.salario as salario_esperado','tipo_educacion.nombre as nivel_educativo','aspirantes.disp_viaje','aspirantes.disp_cambio_hogar')
                    ->join('usuarios','aspirantes.usuario_id','=','usuarios.id')
                    ->join('lugares','aspirantes.lugar_id','=','lugares.id')
                    ->join('tipo_educacion','aspirantes.tipo_educacion_id','=','tipo_educacion.id')
                    ->get()
                    ->toArray();
        
        foreach ($data as $key => $d) {
            if(strlen($d['telefonos'])){
                $telefonos = json_decode($d['telefonos']);
                $data[$key]['telefono']= $telefonos->telefono;
                $data[$key]['celular']= $telefonos->celular;
            }else{
                $data[$key]['telefono']= "";
                $data[$key]['celular']= "";
            }

            unset($data[$key]['telefonos']);
        }

        if(sizeof($data) == 0)
            $data = ['No hay datos para esta consulta'];
        
        $to_export = [];

        foreach ($data as $r)
            array_push($to_export, (array) $r);

        Excel::create('Empleados', function($excel) use(&$to_export) {
            $excel->sheet('Reporte', function($sheet) use(&$to_export) {
                $sheet->fromArray($to_export);
            });
        })->download('xls');

        return dd($data);
    }

    //Muestra la lista de registros
    public function dist_empresas_empleos(Request $request,$idLugar){
        $empresas_empleos = DB::select("SELECT name, COUNT(*) as y FROM entidades INNER JOIN (
                SELECT 
                    0 as start, 10 as 'end', '0-10 empleados' as name
                UNION ALL 
                SELECT
                    11, 50, '11-50 empleados'
                UNION ALL
                SELECT
                    51, 200, '51-200 empleados'
                UNION ALL
                SELECT
                    201, 2000000, 'mas de 200 empleados'
            ) sub ON numero_empleados BETWEEN start AND end 
            INNER JOIN sucursales ON sucursales.entidad_id = entidades.id
            WHERE sucursales.lugar_id=".$idLugar." AND sucursales.tipo='Principal' AND entidades.tipo='Empresa' GROUP BY name WITH ROLLUP");

        $empresas_empleos = collect($empresas_empleos);
        $total = $empresas_empleos->where('name',null)->first();
        $empresas_empleos = $empresas_empleos->map( function($item, $key) use ($total){
            $item->y = ($item->name)?floor($item->y/$total->y*10000)/100:$total->y;
            $item->y = (int)$item->y;
            return $item;
        });

        $empresas_empleos->pop();

        $datosGrafico = [
                "empleados"=>$empresas_empleos,
            ];

        return response()->json([
                'mensaje' => 'Se ha consultado la estadistica exitosamente!',
                'datos'=> $datosGrafico
            ]
        );
    }

    public function dist_empresas_empleos_excel($idLugar){
        $data = Entidad::leftJoin('sucursales', 'entidades.id', '=', 'sucursales.entidad_id')
            ->select('entidades.razon_social','entidades.nit','entidades.numero_empleados')
            ->addSelect('entidades.sector','entidades.web','entidades.correo_privado')
            ->addSelect('entidades.tipo_negocio as Empresa_formal','entidades.descripcion')
            ->where('entidades.tipo','Empresa')
            ->where('sucursales.lugar_id',$idLugar)
            ->where('sucursales.tipo','Principal')
            ->orderBy('sucursales.tipo','asc')
            ->get()
            ->toArray();

        if(sizeof($data) == 0)
            $data = ['No hay datos para esta consulta'];

        $to_export = [];

        foreach ($data as $r)
            array_push($to_export, (array) $r);

        Excel::create('Empleados por empresa-comercio', function($excel) use(&$to_export) {
            $excel->sheet('Reporte', function($sheet) use(&$to_export) {
                $sheet->fromArray($to_export);
            });
        })->download('xls');

        return dd($data);
    }

    //Muestra la lista de registros
    public function empleos_creados(Request $request,$idLugar){
        $fecha1 = date("Y-m-d 00:00:00",strtotime('-3 months'));
        $fecha2 = date("Y-m-d 23:59:59");

        $ofertas = Trabajo::select(DB::raw("COUNT(trabajos.id) as ofertas, date_format(trabajos.created_at, '%Y-%m') as mes"))
                ->where('trabajos.lugar_id',$idLugar)
                ->whereBetween('trabajos.created_at',[$fecha1,$fecha2])
                ->orderBy('mes', 'desc')
                ->groupBy('mes')
                ->distinct()
                ->limit(4)
                ->get();
        //return $ofertas;

            $ofertas = $ofertas->reverse();
            //$ofertas->pop();
            $categorias = $ofertas->map(function($item,$key){
                return $item->mes;
            });
            $series = $ofertas->map(function($item,$key){
                return (integer)$item->ofertas;
            });
            $total = $series->sum();
            return response()->json([
                'mensaje' => 'Se ha consultado la estadistica exitosamente!',
                'datos'=>[
                    "categorias"=>$categorias,
                    "series"=>$series,
                    "total"=>$total
                ]
            ]);
    }

    public function empleos_creados_excel($idLugar){
        $fecha1 = date("Y-m-d 00:00:00",strtotime('-3 months'));
        $fecha2 = date("Y-m-d 23:59:59");

        $data = Trabajo::where('trabajos.lugar_id',$idLugar)
                ->select('trabajos.id','trabajos.titulo','actividades.nombre as categoria','trabajos.disp_viaje')
                ->addSelect('trabajos.disp_cambio_hogar','tipo_contrato.nombre as tipo_contrato','trabajos.experiencia')
                ->addSelect('trabajos.horario_laboral','entidades.nombre as empresa','trabajos.descripcion')
                ->addSelect('trabajos.salario','trabajos.edad_min','trabajos.edad_max','lugares.nombre as municipio')
                ->join('entidades','trabajos.entidad_id','=','entidades.id')
                ->join('lugares','trabajos.lugar_id','=','lugares.id')
                ->join('actividades','trabajos.categoria_id','=','actividades.id')
                ->join('tipo_contrato','trabajos.tipo_contrato_id','=','tipo_contrato.id')
                ->join('tipo_educacion','trabajos.tipo_educacion_id','=','tipo_educacion.id')
                ->whereBetween('trabajos.created_at',[$fecha1,$fecha2])
                ->orderBy('trabajos.created_at', 'desc')
                ->get()
                ->toArray();
        
        $to_export = [];

        if(sizeof($data) == 0)
            $data = ['No hay datos para esta consulta'];

        foreach ($data as $r)
            array_push($to_export, (array) $r);

        Excel::create('Empleos_creados', function($excel) use(&$to_export) {
            $excel->sheet('Reporte', function($sheet) use(&$to_export) {
                $sheet->fromArray($to_export);
            });
        })->download('xls');

        return dd($data);
    }
    
    //Muestra la lista de registros
    public function ofertas_aspirantes(Request $request,$idLugar){
            $aspirantes = Aspirante::select(DB::raw("COUNT(aspirantes.id) as aspirantes, date_format(aspirantes.created_at, '%Y-%m') as mes"))
                ->where('aspirantes.lugar_id', $idLugar)
                ->orderBy('mes', 'desc')
                ->groupBy('mes')
                ->distinct()
                ->limit(12)
                ->get();

            $aspirantes = $aspirantes->reverse();
            
            $aspirante_categorias = $aspirantes->map(function($item,$key){
                return $item->mes;
            });
            $aspirante_series = $aspirantes->map(function($item,$key){
                return (integer)$item->aspirantes;
            });

            $ofertas = Trabajo::select(DB::raw("COUNT(trabajos.id) as ofertas, date_format(trabajos.created_at, '%Y-%m') as mes"))
                ->where('trabajos.lugar_id',$idLugar)
                ->orderBy('mes', 'desc')
                ->groupBy('mes')
                ->distinct()
                ->limit(12)
                ->get();

            $ofertas = $ofertas->reverse();
            
            $ofertas_categorias = $ofertas->map(function($item,$key){
                return $item->mes;
            });
            $ofertas_series = $ofertas->map(function($item,$key){
                return (integer)$item->ofertas;
            });

            $categorias = ($ofertas_categorias->count() > $aspirante_categorias->count())? $ofertas_categorias:$aspirante_categorias;
            return response()->json([
                'mensaje' => 'Se ha consultado la estadistica exitosamente!',
                'datos'=>[
                    "categorias"=>$categorias,
                    "series"=> [
                        ["name" => "Aspirantes", "data" => $aspirante_series],
                        ["name" => "Ofertas", "data" => $ofertas_series],
                    ]
                ]
            ]);
    }

    public function ofertas_aspirantes_excel($idLugar){
        $data = [];

        $aspirantes = DB::table('aspirantes')->select(DB::raw("0 as num_ofertas,COUNT(aspirantes.id) as num_aspirantes, date_format(aspirantes.created_at, '%Y-%m') as mes"))
                ->where('aspirantes.lugar_id', $idLugar)
                ->limit(12);

        $ofertas = DB::table('trabajos')->select(DB::raw("COUNT(trabajos.id) as num_ofertas,0 as num_aspirantes, date_format(trabajos.created_at, '%Y-%m') as mes"))
            ->where('trabajos.lugar_id',$idLugar)
            ->whereNull('trabajos.deleted_at')
            ->limit(12);

        $query = $ofertas->unionAll($aspirantes);
        $querySql = $query->toSql();
        $query2 = DB::table(DB::raw("($querySql) as a"))
                                ->mergeBindings($query)
                                ->select(DB::raw("sum(a.num_ofertas) as num_ofertas,sum(a.num_aspirantes) as num_aspirantes,mes"))
                                ->groupBy('mes');
        
        $data = $query2->get();
        
        $to_export = [];

        if(sizeof($data) == 0)
            $data = ['No hay datos para esta consulta'];

        foreach ($data as $r)
            array_push($to_export, (array) $r);

        Excel::create('Ofertas_vs_aspirantes', function($excel) use(&$to_export) {
            $excel->sheet('Reporte', function($sheet) use(&$to_export) {
                $sheet->fromArray($to_export);
            });
        })->download('xls');

        return dd($data);
    }

    //Muestra la lista de registros
    public function actividades(Request $request,$idLugar){
        $actividades = EntidadActividad::join('actividades', 'entidades_actividades.actividad_id', '=', 'actividades.id')
            ->join('entidades', 'entidades_actividades.entidad_id', '=', 'entidades.id')
            ->join('sucursales', 'sucursales.entidad_id', '=', 'entidades.id')
            ->where('entidades.tipo','Empresa')
            ->where('sucursales.lugar_id',$idLugar)
            ->select("actividades.nombre as name", DB::raw("COUNT(*) as y"))
            ->orderBy("y","desc")
            ->groupBy("actividades.nombre")
            ->limit('20')
            ->get();

        $total = EntidadActividad::join('actividades', 'entidades_actividades.actividad_id', '=', 'actividades.id')
            ->join('entidades', 'entidades_actividades.entidad_id', '=', 'entidades.id')
            ->join('sucursales', 'sucursales.entidad_id', '=', 'entidades.id')
            ->where('entidades.tipo','Empresa')
            ->where('sucursales.lugar_id',$idLugar)
            ->select(DB::raw("COUNT(*) as y"))
            ->first();

        $actividades->push(["name"=>"Otros", "y"=> $total->y - $actividades->sum('y')]);
        $actividades->push(["name"=>null, "y"=> $total->y]);

        $actividades = $actividades->sortBy('y')->values()->all();
        $actividades = collect($actividades);
        $actividades = $actividades->map( function($item, $key) use ($total){
            if($item["name"]){
                if($total->y > 0)
                    $item["y"] = floor($item["y"]/$total->y*10000)/100;
                else
                    $item["y"] = 0;
            }else{
                $total->y;
            }


            return $item;
        });

        $actividades->reverse();
        $actividades->pop();

        return response()->json(['mensaje' => 'Se ha consultado la estadistica exitosamente!', 'datos'=>$actividades]);
    }

    public function actividades_excel($idLugar){
        $actividades = EntidadActividad::join('actividades', 'entidades_actividades.actividad_id', '=', 'actividades.id')
            ->join('entidades', 'entidades_actividades.entidad_id', '=', 'entidades.id')
            ->join('sucursales', 'sucursales.entidad_id', '=', 'entidades.id')
            ->where('entidades.tipo','Empresa')
            ->where('sucursales.lugar_id',$idLugar)
            ->select("actividades.nombre as name", DB::raw("COUNT(*) as y"))
            ->orderBy("y","desc")
            ->groupBy("actividades.nombre")
            ->get();

        $total = EntidadActividad::select(DB::raw("COUNT(*) as y"))
            ->first();

        $actividades->push(["name"=>"Otros", "y"=> $total->y - $actividades->sum('y')]);
        $actividades->push(["name"=>null, "y"=> $total->y]);

        $actividades = $actividades->sortBy('y')->values()->all();
        $actividades = collect($actividades);
        $actividades = $actividades->map( function($item, $key) use ($total){
            if($item["name"]){
                if($total->y > 0)
                    $item["y"] = floor($item["y"]/$total->y*10000)/100;
                else
                    $item["y"] = 0;
            }else{
                $total->y;
            }


            return $item;
        });

        $actividades->pop();

        $data = [];

        foreach ($actividades as $key => $actividad) {
            array_push($data, [
                    'actividad' => $actividad["name"],
                    'porcentaje' => $actividad["y"]
                    ]);
        }
        
        $to_export = [];

        if(sizeof($data) == 0)
            $data = ['No hay datos para esta consulta'];

        foreach ($data as $r)
            array_push($to_export, (array) $r);

        Excel::create('Participacion_en_actividades_economicas', function($excel) use(&$to_export) {
            $excel->sheet('Reporte', function($sheet) use(&$to_export) {
                $sheet->fromArray($to_export);
            });
        })->download('xls');

        return dd($data);
    }

    //Muestra la lista de registros
    public function empleos_empresas(Request $request,$idLugar){
        $empleos =  Trabajo::join('entidades', 'trabajos.entidad_id', '=', 'entidades.id')
            ->select(DB::raw('COUNT(trabajos.id) as ofertas'), 'entidades.razon_social')
            ->where('trabajos.lugar_id', $idLugar)
            ->orderBy('ofertas', 'desc')
            ->groupBy('entidades.razon_social')
            ->orderBy('ofertas','desc')
            ->distinct()
            ->limit(10)
            ->get();

        //$empleos = $empleos->reverse();
        
        $categorias = $empleos->map(function($item,$key){
            return $item->razon_social;
        });

        $series = $empleos->map(function($item,$key){
            return (integer)$item->ofertas;
        });

        return response()->json([
            'mensaje' => 'Se ha consultado la estadistica exitosamente!',
            'datos'=>[
                "categorias"=>$categorias,
                "series"=>$series
            ]
        ]);
    }

    public function empleos_empresas_excel($idLugar){
        $empleos =  Trabajo::join('entidades', 'trabajos.entidad_id', '=', 'entidades.id')
            ->select('entidades.razon_social',DB::raw('COUNT(trabajos.id) as ofertas'))
            ->where('trabajos.lugar_id', $idLugar)
            ->orderBy('ofertas', 'desc')
            ->groupBy('entidades.razon_social')
            ->distinct()
            ->get();


        $empleos = $empleos->reverse();
    
        $data = [];

        foreach ($empleos as $key => $empleo) {
            array_push($data, [
                    'Empresa' => $empleo->razon_social,
                    'Ofertas' => $empleo->ofertas
                    ]);
        }

        $to_export = [];

        if(sizeof($data) == 0)
            $data = ['No hay datos para esta consulta'];

        foreach ($data as $r)
            array_push($to_export, (array) $r);

        Excel::create('Archivo', function($excel) use(&$to_export) {
            $excel->sheet('Reporte', function($sheet) use(&$to_export) {
                $sheet->fromArray($to_export);
            });
        })->download('xls');

        return dd($data);
    }

    //Muestra la lista de registros
    public function ofertas_empresa($idEntidad){
        $creadas = Trabajo::where('trabajos.entidad_id', $idEntidad)
            ->where('estado','abierto')        
            ->count();

        $destacadas = Trabajo::where('trabajos.entidad_id', $idEntidad)
            ->where('estado','abierto')
            ->where('destacar',1)
            ->count();

        $haceUnMes = date('Y-m-d 00:00:00',strtotime('-1 month'));

        $nuevas = Trabajo::where('trabajos.entidad_id', $idEntidad)
            ->where('estado','abierto')
            ->where('created_at','>',$haceUnMes)
            ->count();

        return response()->json([
            'mensaje' => 'Se ha consultado la estadistica exitosamente!',
            'datos'=>[
                "creadas"=>$creadas,
                "destacadas"=>$destacadas,
                "nuevas"=>$nuevas
            ]
        ]);
    }

    public function ofertas_empresa_excel($idEntidad){
        $query = Trabajo::where('trabajos.entidad_id', $idEntidad)
            ->select('trabajos.id','trabajos.titulo','actividades.nombre as categoria','trabajos.disp_viaje')
            ->addSelect('trabajos.disp_cambio_hogar','tipo_contrato.nombre as tipo_contrato','trabajos.experiencia')
            ->addSelect('trabajos.horario_laboral','entidades.nombre as empresa','trabajos.descripcion')
            ->addSelect('trabajos.salario','trabajos.edad_min','trabajos.edad_max','lugares.nombre as municipio')
            ->join('entidades','trabajos.entidad_id','=','entidades.id')
            ->join('lugares','trabajos.lugar_id','=','lugares.id')
            ->join('actividades','trabajos.categoria_id','=','actividades.id')
            ->join('tipo_contrato','trabajos.tipo_contrato_id','=','tipo_contrato.id')
            ->join('tipo_educacion','trabajos.tipo_educacion_id','=','tipo_educacion.id');

        $creadas = clone $query;
        $destacadas = clone $query;
        $nuevas = clone $query;

        $creadas = $creadas
            ->where('estado','abierto')        
            ->get()
            ->toArray();

        $destacadas = $destacadas
            ->where('estado','abierto')
            ->where('destacar',1)
            ->get()
            ->toArray();

        $haceUnMes = date('Y-m-d 00:00:00',strtotime('-1 month'));

        $nuevas = $nuevas
            ->where('estado','abierto')
            ->where('trabajos.created_at','>',$haceUnMes)
            ->get()
            ->toArray();

        $data = [$creadas,$destacadas,$nuevas];
        $to_export = [[],[],[]];

        foreach ($data as $key => $d) {
            if(sizeof($d) == 0)
                $data[$key] = ['No hay datos para esta consulta'];
        }

        foreach ($data as $key1 => $d){
            foreach ($d as $r) {
                array_push($to_export[$key1], (array) $r);
            }
        }

        Excel::create('Ofertas_de_empleo', function($excel) use(&$to_export) {
            $excel->sheet('Ofertas creadas', function($sheet) use(&$to_export) {
                $sheet->fromArray($to_export[0]);
            });

            $excel->sheet('Ofertas destacadas', function($sheet) use(&$to_export) {
                $sheet->fromArray($to_export[1]);
            });

            $excel->sheet('Nuevas ofertas', function($sheet) use(&$to_export) {
                $sheet->fromArray($to_export[2]);
            });
        })->download('xls');

        return dd($data);
    }

    //Muestra la lista de registros
    public function cv_empresa($idEntidad){
        $entidad = Entidad::find($idEntidad);

        $hojasRecibidas = $entidad
                            ->trabajos()
                            ->leftjoin('aspirante_trabajo','aspirante_trabajo.trabajo_id','=','trabajos.id')
                            ->count();

        $hojasPendientes = $entidad
                            ->trabajos()
                            ->leftjoin('aspirante_trabajo','aspirante_trabajo.trabajo_id','=','trabajos.id')
                            ->where('aspirante_trabajo.estado','En espera')
                            ->count();

        $ofertasSinAspirantes = $entidad
                            ->trabajos()
                            ->leftjoin('aspirante_trabajo','aspirante_trabajo.trabajo_id','=','trabajos.id')
                            ->whereNull('aspirante_trabajo.aspirante_id')
                            ->count();

        return response()->json([
            'mensaje' => 'Se ha consultado la estadistica exitosamente!',
            'datos'=>[
                "recibidas"=>$hojasRecibidas,
                "pendientes"=>$hojasPendientes,
                "sin_aspirantes"=>$ofertasSinAspirantes
            ]
        ]);
    }

    public function cv_empresa_excel($idEntidad){
        $entidad = Entidad::find($idEntidad);

        $query = $entidad
                        ->trabajos()
                        ->leftjoin('aspirante_trabajo','aspirante_trabajo.trabajo_id','=','trabajos.id')
                        ->join('aspirantes','aspirante_trabajo.aspirante_id','=','aspirantes.id')
                        ->join('tipo_educacion','aspirantes.tipo_educacion_id','=','tipo_educacion.id')
                        ->join('usuarios','aspirantes.usuario_id','=','usuarios.id')
                        ->select('trabajos.id','trabajos.titulo as oferta','usuarios.nombre','usuarios.apellido')
                        ->addSelect('aspirantes.experiencia','aspirantes.disp_viaje','aspirantes.disp_cambio_hogar')
                        ->addSelect('tipo_educacion.nombre as nivel_educativo','aspirante_trabajo.f_solicitud','aspirante_trabajo.estado');

        $hojasRecibidas = clone $query;
        $hojasPendientes = clone $query;

        $hojasRecibidas = $hojasRecibidas
                            ->get()
                            ->toArray();

        $hojasPendientes = $hojasPendientes
                            ->where('aspirante_trabajo.estado','En espera')
                            ->get()
                            ->toArray();

        $ofertasSinAspirantes = $entidad
                            ->trabajos()
                            ->leftjoin('aspirante_trabajo','aspirante_trabajo.trabajo_id','=','trabajos.id')
                            ->select('trabajos.id','trabajos.titulo','actividades.nombre as categoria','trabajos.disp_viaje')
                            ->addSelect('trabajos.disp_cambio_hogar','tipo_contrato.nombre as tipo_contrato','trabajos.experiencia')
                            ->addSelect('trabajos.horario_laboral','entidades.nombre as empresa','trabajos.descripcion')
                            ->addSelect('trabajos.salario','trabajos.edad_min','trabajos.edad_max','lugares.nombre as municipio')
                            ->join('entidades','trabajos.entidad_id','=','entidades.id')
                            ->join('lugares','trabajos.lugar_id','=','lugares.id')
                            ->join('actividades','trabajos.categoria_id','=','actividades.id')
                            ->join('tipo_contrato','trabajos.tipo_contrato_id','=','tipo_contrato.id')
                            ->join('tipo_educacion','trabajos.tipo_educacion_id','=','tipo_educacion.id')
                            ->whereNull('aspirante_trabajo.aspirante_id')
                            ->get()
                            ->toArray();

        $data = [$hojasRecibidas,$hojasPendientes,$ofertasSinAspirantes];
        $to_export = [[],[],[]];

        foreach ($data as $key => $d) {
            if(sizeof($d) == 0)
                $data[$key] = ['No hay datos para esta consulta'];
        }

        foreach ($data as $key1 => $d){
            foreach ($d as $r) {
                array_push($to_export[$key1], (array) $r);
            }
        }

        Excel::create('Reporte Hojas de vida', function($excel) use(&$to_export) {
            $excel->sheet('Recibidas', function($sheet) use(&$to_export) {
                $sheet->fromArray($to_export[0]);
            });

            $excel->sheet('Pendientes', function($sheet) use(&$to_export) {
                $sheet->fromArray($to_export[1]);
            });

            $excel->sheet('Ofertas sin hojas de vida', function($sheet) use(&$to_export) {
                $sheet->fromArray($to_export[2]);
            });
        })->download('xls');

        return dd($data);
    }

    //Muestra la lista de registros
    public function competitividad($idLugar){
        $fecha1 = date("Y-m-d 00:00:00",strtotime('-3 months'));
        $fecha2 = date("Y-m-d 23:59:59");

        $empresas_nuevas = Entidad::join('sucursales','sucursales.entidad_id','=','entidades.id')
                ->where('sucursales.lugar_id', $idLugar)
                ->whereBetween('sucursales.created_at',[$fecha1,$fecha2])
                ->where('entidades.tipo', 'Empresa')
                ->count();

        $ofertas_nuevas = Trabajo::where('trabajos.lugar_id',$idLugar)
                ->whereBetween('trabajos.created_at',[$fecha1,$fecha2])
                ->count();

        $empresas_nuevas_otros_mun = Entidad::join('sucursales','sucursales.entidad_id','=','entidades.id')
                ->whereNotIn('sucursales.lugar_id', [$idLugar])
                ->whereBetween('sucursales.created_at',[$fecha1,$fecha2])
                ->where('entidades.tipo', 'Empresa')
                ->count();

        $ofertas_nuevas_otros_mun = Trabajo::whereNotIn('trabajos.lugar_id',[$idLugar])
                ->whereBetween('trabajos.created_at',[$fecha1,$fecha2])
                ->count();

        //numero municipios cundinamarca
        $municipios_cundi = Lugar::municipios()->count();

        $empresas_nuevas_otros_mun /=$municipios_cundi; //promedio de nuevas empresas de los demas municipios
        $ofertas_nuevas_otros_mun /=$municipios_cundi; //promedio de nuevas empresas de los demas municipios

        return response()->json([
            'mensaje' => 'Se ha consultado la estadistica exitosamente!',
            'datos'=>[
                "categorias"=>['Creación de empleos','Creación de empresas/comercio'],
                "empleos"=>[$empresas_nuevas,ceil($empresas_nuevas_otros_mun)],
                "comercio"=>[$ofertas_nuevas,ceil($ofertas_nuevas_otros_mun)]
            ]
        ]);
    }

    //Muestra la lista de registros
    public function competitividad_excel($idLugar){
        $fecha1 = date("Y-m-d 00:00:00",strtotime('-3 months'));
        $fecha2 = date("Y-m-d 23:59:59");

        $empresas_nuevas = Entidad::join('sucursales','sucursales.entidad_id','=','entidades.id')
                ->where('sucursales.lugar_id', $idLugar)
                ->whereBetween('sucursales.created_at',[$fecha1,$fecha2])
                ->where('entidades.tipo', 'Empresa')
                ->count();

        $ofertas_nuevas = Trabajo::where('trabajos.lugar_id',$idLugar)
                ->whereBetween('trabajos.created_at',[$fecha1,$fecha2])
                ->count();

        $empresas_nuevas_otros_mun = Entidad::select(DB::raw("lugares.nombre as municipio,COUNT(distinct entidades.id) as num_entidades"))
                ->join('sucursales','sucursales.entidad_id','=','entidades.id')
                ->join('lugares','sucursales.lugar_id','=','lugares.id')
                ->whereNotIn('sucursales.lugar_id', [$idLugar])
                ->whereBetween('sucursales.created_at',[$fecha1,$fecha2])
                ->where('entidades.tipo', 'Empresa')
                ->groupBy('sucursales.lugar_id')
                ->get();

        $ofertas_nuevas_otros_mun = Trabajo::select(DB::raw("lugares.nombre as municipio,COUNT(trabajos.id) as ofertas"))
                ->join('lugares','trabajos.lugar_id','=','lugares.id')
                ->whereNotIn('trabajos.lugar_id',[$idLugar])
                ->whereBetween('trabajos.created_at',[$fecha1,$fecha2])
                ->groupBy('trabajos.lugar_id')
                ->get();

        //numero municipios cundinamarca
        $municipio = Lugar::find($idLugar);

        $data_empresas = [];
        $data_ofertas = [];

        foreach ($empresas_nuevas_otros_mun as $key1 => $d){
            if($empresas_nuevas > 0){
                $p_crecimiento = (($empresas_nuevas - $d->num_entidades)/$empresas_nuevas) * 100;
                $p_crecimiento = round( $p_crecimiento );
            }else{
                $p_crecimiento = $d->num_entidades * 100;
            }

            $fila = [
                    "Municipio"=>$d->municipio,
                    "Nuevos empleos"=>$d->num_entidades,
                    "Nuevos empleos en $municipio->nombre"=>$empresas_nuevas,
                    "Porcentaje de desarrollo y competitividad comparativa" => $p_crecimiento."%"
                ];
            array_push($data_empresas,$fila);
        }


        foreach ($ofertas_nuevas_otros_mun as $key1 => $d){
            if($ofertas_nuevas > 0){
                $p_crecimiento = (($ofertas_nuevas - $d->ofertas)/$ofertas_nuevas) * 100;
                $p_crecimiento = round( $p_crecimiento );
            }else{
                $p_crecimiento = $d->ofertas * 100;
            }

            $fila = [
                    "Municipio"=>$d->municipio,
                    "Nuevos empleos"=>$d->ofertas,
                    "Nuevos empleos en $municipio->nombre"=>$ofertas_nuevas,
                    "Porcentaje de desarrollo y competitividad comparativa" => $p_crecimiento."%"
                ];

            array_push($data_ofertas,$fila);
        }

        Excel::create('Reporte desarrollo y crecimiento en los ultimos 3 meses', function($excel) use(&$data_empresas,&$data_ofertas) {
            $excel->sheet('Nuevo Comercio - empresas', function($sheet) use(&$data_empresas) {
                $sheet->fromArray($data_empresas);
            });

            $excel->sheet('Nuevos Empleos', function($sheet) use(&$data_ofertas) {
                $sheet->fromArray($data_ofertas);
            });
        })->download('xls');
    }

    //Muestra la lista de registros
    public function produservicios(Request $request,$idLugar){
        $produservicios = Produservicio::join('entidades_produservicios', 'entidades_produservicios.produservicio_id', '=', 'produservicios.id')
            ->join('entidades', 'entidades_produservicios.entidad_id', '=', 'entidades.id')
            ->join('sucursales', 'sucursales.entidad_id', '=', 'entidades.id')
            ->where('entidades.tipo','Empresa')
            ->where('sucursales.lugar_id',$idLugar)
            ->select("produservicios.nombre as name", DB::raw("COUNT(*) as y"))
            ->orderBy("y","desc")
            ->groupBy("produservicios.nombre")
            ->limit('20')
            ->get();

        $total = Produservicio::join('entidades_produservicios', 'entidades_produservicios.produservicio_id', '=', 'produservicios.id')
            ->join('entidades', 'entidades_produservicios.entidad_id', '=', 'entidades.id')
            ->join('sucursales', 'sucursales.entidad_id', '=', 'entidades.id')
            ->where('entidades.tipo','Empresa')
            ->where('sucursales.lugar_id',$idLugar)
            ->select(DB::raw("COUNT(*) as y"))
            ->first();

        $produservicios->push(["name"=>"Otros", "y"=> $total->y - $produservicios->sum('y')]);
        $produservicios->push(["name"=>null, "y"=> $total->y]);

        $produservicios = $produservicios->sortBy('y')->values()->all();
        $produservicios = collect($produservicios);
        $produservicios = $produservicios->map( function($item, $key) use ($total){
            if($item["name"]){
                if($total->y > 0)
                    $item["y"] = floor($item["y"]/$total->y*10000)/100;
                else
                    $item["y"] = 0;
            }else{
                $total->y;
            }


            return $item;
        });

        $produservicios->reverse();
        $produservicios->pop();

        return response()->json(['mensaje' => 'Se ha consultado la estadistica exitosamente!', 'datos'=>$produservicios]);
    }

    public function produservicios_excel($idLugar){
        $produservicios = Produservicio::join('entidades_produservicios', 'entidades_produservicios.produservicio_id', '=', 'produservicios.id')
            ->join('entidades', 'entidades_produservicios.entidad_id', '=', 'entidades.id')
            ->join('sucursales', 'sucursales.entidad_id', '=', 'entidades.id')
            ->where('entidades.tipo','Empresa')
            ->where('sucursales.lugar_id',$idLugar)
            ->select("produservicios.nombre as name", DB::raw("COUNT(*) as y"))
            ->orderBy("y","desc")
            ->groupBy("produservicios.nombre")
            ->get();

        $total = Produservicio::join('entidades_produservicios', 'entidades_produservicios.produservicio_id', '=', 'produservicios.id')
            ->join('entidades', 'entidades_produservicios.entidad_id', '=', 'entidades.id')
            ->join('sucursales', 'sucursales.entidad_id', '=', 'entidades.id')
            ->where('entidades.tipo','Empresa')
            ->where('sucursales.lugar_id',$idLugar)
            ->select(DB::raw("COUNT(*) as y"))
            ->first();

        $produservicios->push(["name"=>"Otros", "y"=> $total->y - $produservicios->sum('y')]);
        $produservicios->push(["name"=>null, "y"=> $total->y]);

        $produservicios = $produservicios->sortBy('y')->values()->all();
        $produservicios = collect($produservicios);
        $produservicios = $produservicios->map( function($item, $key) use ($total){
            if($item["name"]){
                if($total->y > 0)
                    $item["y"] = floor($item["y"]/$total->y*10000)/100;
                else
                    $item["y"] = 0;
            }else{
                $total->y;
            }


            return $item;
        });

        $produservicios->pop();

        $data = [];

        foreach ($produservicios as $key => $produservicio) {
            array_push($data, [
                    'actividad' => $produservicio["name"],
                    'porcentaje' => $produservicio["y"]
                    ]);
        }
        
        $to_export = [];

        if(sizeof($data) == 0)
            $data = ['No hay datos para esta consulta'];

        foreach ($data as $r)
            array_push($to_export, (array) $r);

        Excel::create('Reporte Productos y servicios', function($excel) use(&$to_export) {
            $excel->sheet('Reporte', function($sheet) use(&$to_export) {
                $sheet->fromArray($to_export);
            });
        })->download('xls');

        return dd($data);
    }
}