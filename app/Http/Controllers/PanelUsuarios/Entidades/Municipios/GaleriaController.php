<?php 

namespace App\Http\Controllers\PanelUsuarios\Entidades\Municipios;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lugar;
use App\Evento;
use App\Session;
use App\Funcionario;
use App\MultimediaLugar;

use View;
use DB;
use Redirect;
use Input;

class GaleriaController extends Controller
{

	/**
	 * Muestra la lista de registros
	 *
	 * @return Response
	 */

	public function index()
	{
		return "index";
	}


	/**
	 * Muestra el formulario para crear un nuevo registro.
	 *
	 * @return Response
	 */

	public function create($idMunicipio)
	{
		return "create";
	}


	/**
	 * Guarda un nuevo registro en la base de datos
	 *
	 * @return Response
	 */

	public function store()
	{
		$idMunicipio = Input::get('idMunicipio');
		$municipio = Lugar::find($idMunicipio);

		if(!$municipio)
			return Redirect::to('usuario/entidad/perfil?seccion=galeria')->with('error',"Municipio no encontrado");

		$foto = new MultimediaLugar;
		$foto->tipo = 'Foto';
		$foto->ruta = $this->guardarImagen(Input::file('foto'),'municipios');
		$foto->lugar_id = $municipio->id;
		$foto->save();

		if($foto)
			return Redirect::to('usuario/entidad/perfil?seccion=galeria')->with('mensaje',"Foto creada con éxito");
		else
			return Redirect::to('usuario/entidad/perfil?seccion=galeria')->with('error',"Error al crear esta foto, revisa los datos de ingreso");
	}


	/**
	 * Elimina un registro especifico de la base de datos
	 *
	 * @param  int $id
	 * @return Response
	 */


	public function destroy($idSitio)
	{
		$idMunicipio = Input::get('idMunicipio');
		$municipio = Lugar::find($idMunicipio);
		$foto = MultimediaLugar::find($idSitio);

		if($foto->delete())
			return Redirect::to('usuario/entidad/perfil?seccion=galeria')->with('mensaje',"Foto eliminada con éxito");
		else
			return Redirect::to('usuario/entidad/perfil?seccion=galeria')->with('error',"Error al eliminar esta foto");
	}


}