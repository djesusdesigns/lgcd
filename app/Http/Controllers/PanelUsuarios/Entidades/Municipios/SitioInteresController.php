<?php 
namespace App\Http\Controllers\PanelUsuarios\Entidades\Municipios;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lugar;
use App\Noticia;
use App\Fuente;
use App\SitioInteres;

use View;
use DB;
use Input;
use Auth;
use Redirect;

class SitioInteresController extends Controller

{

	/**
	 * Muestra la lista de registros
	 *
	 * @return Response
	 */

	public function index($idMunicipio)
	{
		return "index";
	}


	/**
	 * Muestra el formulario para crear un nuevo registro.
	 *
	 * @return Response
	 */

	public function create($idMunicipio)
	{
		return "create";
	}


	/**
	 * Guarda un nuevo registro en la base de datos
	 *
	 * @return Response
	 */

	public function store()
	{
		$idMunicipio = Input::get('idMunicipio');
		$municipio = Lugar::find($idMunicipio);

		if(!$municipio)
			return Redirect::to('admin/municipio/'.$idMunicipio.'/sitio-interes')->with('error',"Municipio no encontrado");

		$sitioInteres = new SitioInteres;
		$sitioInteres->nombre = Input::get('nombre');
		$sitioInteres->foto = $this->guardarImagen(Input::file('foto'),'sitioInteres');
		$sitioInteres->direccion = Input::get('direccion');
		$sitioInteres->telefono = Input::get('telefono');
		$sitioInteres->email = Input::get('email');
		$sitioInteres->web = Input::get('web');
		$sitioInteres->descripcion = Input::get('descripcion');
		$sitioInteres->lugar_id = $municipio->id;
		$sitioInteres->save();

		if($sitioInteres)
			return Redirect::to('usuario/entidad/perfil?seccion=sitios_interes')->with('mensaje',"Número de emergencia creado con éxito");
		else
			return Redirect::to('usuario/entidad/perfil?seccion=sitios_interes')->with('error',"Error al crear este número de emergencia, revisa los datos de ingreso");
	}

	public function edit($idMunicipio,$idSitio)
	{
		return "edit";
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @return Response
	 */

	public function update($idSitio)
	{
		$idMunicipio = Input::get('idMunicipio');
		$municipio = Lugar::find($idMunicipio);

		if(!$municipio)
			return Redirect::to('admin/municipio/'.$idMunicipio.'/sitio-interes')->with('error',"Municipio no encontrado");

		$sitioInteres = SitioInteres::find($idSitio);
		$sitioInteres->nombre = Input::get('nombre');
		
		if (Input::hasFile('foto'))
		{
			$sitioInteres->foto = $this->guardarImagen(Input::file('foto'),'sitioInteres');
		}

		$sitioInteres->direccion = Input::get('direccion');
		$sitioInteres->telefono = Input::get('telefono');
		$sitioInteres->email = Input::get('email');
		$sitioInteres->web = Input::get('web');
		$sitioInteres->descripcion = Input::get('descripcion');
		$sitioInteres->lugar_id = $municipio->id;
		$sitioInteres->save();

		if($sitioInteres)
			return Redirect::to('usuario/entidad/perfil?seccion=sitios_interes')->with('mensaje',"Sitio de interés creado con éxito");
		else
			return Redirect::to('usuario/entidad/perfil?seccion=sitios_interes')->with('error',"Error al crear este sitio, revisa los datos de ingreso");
	}


	/**
	 * Elimina un registro especifico de la base de datos
	 *
	 * @param  int $id
	 * @return Response
	 */


	public function destroy($idSitio)
	{
		$idMunicipio = Input::get('idMunicipio');
		$municipio = Lugar::find($idMunicipio);
		$sitioInteres = SitioInteres::find($idSitio);

		if($sitioInteres->delete())
			return Redirect::to('usuario/entidad/perfil?seccion=sitios_interes')->with('mensaje',"Número de emergencia eliminado con éxito");
		else
			return Redirect::to('usuario/entidad/perfil?seccion=sitios_interes')->with('error',"Error al eliminar este número de emergencia");
	}


}
