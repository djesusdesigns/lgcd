<?php

namespace App\Http\Controllers\PanelUsuarios\Entidades\Municipios;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Auth;
use Validator;
use Storage;
use DB;

use App\Entidad;
use App\Usuario;
use App\Sucursal;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MunicipioController extends Controller{

    public function crear(Request $request){
        //**param** lugar_id, clave, clave_confirmation, correo

        $validator = Validator::make($request->all(), [
            'lugar_id' => 'required|string|exists:lugares,id',
            'correo' => 'required|email|unique:usuarios,correo',
            'clave' => 'required|min:8|confirmed'
        ]);

        $municipio = Entidad::join('sucursales', 'entidades.id', '=', 'sucursales.entidad_id')
                        ->where('entidades.tipo','Municipio')
                        ->where('sucursales.lugar_id', $request->lugar_id)
                        ->first();

        $validator->after(function($validator) use($municipio){
            if ($municipio) {
                $validator->errors()->add('Municipio', 'Este municipio ya se ha registrado');
            }
        });

        if (!$validator->fails()) {

            $reporte = DB::transaction(function () use($request) {

                $usuario = new Usuario();

                $usuario->correo = $request->correo;   
                $usuario->rol_id = 3;   
                $usuario->tipo = 1;   
                $usuario->contrasena = bcrypt($request->clave);   
                $usuario->estado = 1;

                $usuario->save();

                $entidad = new Entidad();
                $entidad->usuario_id = $usuario->id;
                $entidad->correo_privado = $request->correo;
                $entidad->tipo = "Municipio";

                $entidad->save();

                $sucursal = new Sucursal();   
                $sucursal->entidad_id = $entidad->id;
                $sucursal->lugar_id = $request->lugar_id;

                $sucursal->save();                

            });

            return response()->json(['mensaje' => "Se ha creado el registro exitosamente!"]);

        }else{
            return response()->json(['error' => $validator->errors()->first()]);
        }
    }
}