<?php 

namespace App\Http\Controllers\PanelUsuarios;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lugar;
use App\Evento;
use App\Entidad;

use Input;
use Auth;
use Validator;

class EventoController extends Controller
{
	public function index(Request $request){
		return view('panel-usuarios.eventos');
	}

	public function create(Request $request){
		$evento = new Evento;
		return view('panel-usuarios.form-eventos')
		->with('evt',$evento);
	}

	public function edit(Request $request,$id){
		$evento = Evento::find($id);

		if(!$evento)
			abort(404);

		return view('panel-usuarios.form-eventos')
		->with('evt',$evento);
	}

	public function store(Request $request)
	{
		//dd(Auth::user());
		try{
			\DB::connection()->getPdo()->beginTransaction();

			$validator = Validator::make($request->all(), [
	            'titulo' => 'required|string',
	            'horario' => 'required|string',
	            'f_realizacion' => 'required|date',
				'f_finalizacion' => 'required|date',
	            'correo_info' => 'required',
	            'miniatura' => 'max:8500',
	            'lugar_id' => 'required'
	        ]);

	        $validator->after(function($validator) use($request){
	            if( strtotime($request->f_realizacion) > strtotime($request->f_finalizacion) ){
	               $validator->errors()->add('f_realizacion', 'La fecha de realización debe ser menor o igual a la fecha de finalización');
	            }
	        });

			if ($validator->fails()){
				if($request->is('api/*'))
					return response()->json(['errors' => $validator->errors() ]);
				else
					return back()->withErrors($validator);
			}

			$evento = new Evento;
			$evento->titulo = Input::get('titulo');
			$evento->contenido = Input::get('contenido');
			$evento->f_realizacion = Input::get('f_realizacion');
			$evento->f_caducidad = Input::get('f_caducidad');
			$evento->horario = Input::get('horario');
			$evento->f_finalizacion = Input::get('f_finalizacion');
			$evento->lugar_evento = Input::get('lugar_evento');
			$evento->telefonos_info = Input::get('telefonos_info');
			$evento->correo_info = Input::get('correo_info');
			$evento->f_publicacion = Input::get('f_publicacion');
			
			if($request->hasFile('miniatura'))
				$evento->miniatura = $this->guardarImagen(Input::file('miniatura'),'eventos');
			else
				$evento->miniatura = "";

			$evento->lugar_id = Input::get('lugar_id');
			$evento->usuario_id = Auth::user()->id;
			$evento->save();

			\DB::connection()->getPdo()->commit();

			return response()->json(['mensaje' => 'Evento creado con éxito', 'id' => $evento->id ]);

		}catch(\Exception $e){
            \DB::connection()->getPdo()->rollBack();
			return response()->json(['error' => 'Error al crear este evento, revisa los datos de ingreso.']);
        }

	}

	public function update(Request $request,$id)
	{
		$validator = Validator::make($request->all(), [
            'titulo' => 'required|string',
            'f_realizacion' => 'required|date',
			'f_finalizacion' => 'required|date',
            'correo_info' => 'required|email',
            'imagen_evento' => 'max:1500',
            'lugar_id' => 'required'
        ]);

        $validator->after(function($validator) use($request){
            if( strtotime($request->f_realizacion) > strtotime($request->f_finalizacion) ){
               $validator->errors()->add('f_realizacion', 'La fecha de realización debe ser menor o igual a la fecha de finalización');
            }
        });

        if ($validator->fails()){
			if($request->is('api/*'))
				return response()->json(['errors' => $validator->errors() ]);
			else
				return back()->withErrors($validator);
		}

		$evento = Evento::find($id);
		$evento->titulo = Input::get('titulo');
		$evento->contenido = Input::get('contenido');
		$evento->f_realizacion = Input::get('f_realizacion');
		$evento->f_caducidad = Input::get('f_caducidad');
		$evento->f_finalizacion = Input::get('f_finalizacion');
		$evento->horario = Input::get('horario');
		$evento->lugar_evento = Input::get('lugar_evento');
		$evento->telefonos_info = Input::get('telefonos_info');
		$evento->correo_info = Input::get('correo_info');
		$evento->f_publicacion = Input::get('f_publicacion');
		
		if (Input::hasFile('imagen_evento'))
		{
			$evento->miniatura = $this->guardarImagen(Input::file('imagen_evento'),'eventos');
		}
		
		$evento->lugar_id = Input::get('lugar_id');
		$evento->save();

		if($evento)
			return response()->json(['mensaje' => 'Evento editado con éxito', 'id' => $evento->id ]);
		else
			return response()->json(['error' => 'Error al editar este evento, revisa los datos de ingreso']);
	}

	public function destroy($id)
	{
		// delete
		$evento = Evento::find($id);
		$evento->delete();

		if($evento)
			return response()->json(['mensaje' => 'Evento eliminado con éxito']);
		else
			return response()->json(['error' => 'Error al eliminar este evento, revisa los datos de ingreso']);
	}

}
