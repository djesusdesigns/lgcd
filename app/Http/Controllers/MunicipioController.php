<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lugar;
use App\DatosLugar;
use App\Noticia;
use App\Evento;
use App\Comentario;
use App\Entidad;

use View;
use DB;

class MunicipioController extends Controller
{

	public function indexGuest()
	{

		$provincias = Lugar::provincias()->orderBy('nombre','asc')->get();

		if (request()->is('api/*')) {
			$municipios = Lugar::municipios()->orderBy('nombre','asc')->get();
			return response()
            ->json($municipios);
		}else{
			return redirect('/municipio/25');
		}

		return View::make('municipios.index')
			->with('provincias', $provincias);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function show(Request $request,$slugMunicipio)
	{
		$slugArray = explode("-",$slugMunicipio);
		$idMunicipio = $slugArray[sizeof($slugArray)-1];
		$request->merge(['lugar' => $idMunicipio]);

		$municipio = Lugar::find($idMunicipio);
		//dd($municipio->datos);
		//$this->verificarTema($municipio->id);

		$datos = ($municipio->datos) ? $municipio->datos : new DatosLugar;
		$alcalde = $municipio
					->funcionarios()
					->select('funcionarios.*')
					->where(function($query)
				        {
				            $query
				            	->orWhere('cargo','=','Alcalde')
								->orWhere('cargo','=','alcalde')
								->orWhere('cargo','=','gobernador')
								->orWhere('cargo','=','Gobernador')
								->orWhere('cargo','=','GOBERNADOR')
								->orWhere('cargo','=','gobernadora')
								->orWhere('cargo','=','Gobernadora')
								->orWhere('cargo','=','GOBERNADORA')
								->orWhere('cargo','=','ALCALDE')
								->orWhere('cargo','=','Alcaldesa')
								->orWhere('cargo','=','alcaldesa')
								->orWhere('cargo','=','ALCALDESA');
				        }
				    )
					->first();

		$dependencias = $municipio->dependencias;

		$bandera = $municipio
					->simbolos()
					->select('simbolos.*')
					->where('tipo','=','bandera')
					->first();

		$escudo = $municipio
					->simbolos()
					->select('simbolos.*')
					->where('tipo','=','escudo')
					->first();

		$himno = $municipio
					->simbolos()
					->select('simbolos.*')
					->where('tipo','=','himno')
					->first();

		$himnoYoutube = $municipio
					->simbolos()
					->select('simbolos.*')
					->where('tipo','=','himno_youtube')
					->first();

		$flor = $municipio
					->simbolos()
					->select('simbolos.*')
					->where('tipo','=','flor')
					->first();

		$arbol = $municipio
					->simbolos()
					->select('simbolos.*')
					->where('tipo','=','arbol')
					->first();

		$emergencias = $municipio->numEmergencias;
		$simbolos = $municipio->simbolos;
		$noticias = Noticia::orderBy('f_publicacion', 'desc')
								->where('lugar_id','=',$municipio->id )
								->orderBy('f_publicacion','desc')
								->take(6)
								->get();

		$aniosNoticias = $this->aniosNoticias($municipio->id);

		$eventos = Evento::orderBy('f_realizacion', 'desc')
								->where('lugar_id','=',$municipio->id )
								->orderBy('f_realizacion','desc')
								->take(6)
								->get();

		$comentarios = [];//Comentario::seccion('Municipio',$municipio->id)->get();

		$municipio->alcalde = $alcalde;
		$municipio->noticias = $noticias;
		$municipio->eventos = $eventos;
		$municipio->escudo = $escudo;
		$municipio->bandera = $bandera;
		$municipio->sitiosInteres = $municipio->sitiosInteres;
		$municipio->datos = $municipio->datos;
		$municipio->datos->clean_web = str_replace("http://","",$municipio->datos->web);
		$municipio->datos->clean_web = str_replace("/","",$municipio->datos->clean_web);

		foreach ($municipio->noticias as $key => $noti) {
            $noti = $this->completarDatosNoticia($noti);
        }

        foreach ($municipio->eventos as $key => $evt) {
            $evt = $this->completarDatosEvento($evt);
        }

        foreach ($municipio->sitiosInteres as $key => $sitio) {
            $sitio = $this->completarDatosSitio($sitio);
        }

		if (request()->is('api/*')) {
			return response()
            ->json($municipio);
		}

		return View::make('municipios.show')->with('municipio', $municipio);
	}

	public function aniosNoticias($idMunicipio)
	{
		$aniosNoticias = Noticia::publicadas()
							->select(DB::raw('YEAR(f_publicacion) as anio'))
							->orderBy('f_publicacion', 'desc')
							->groupBy('anio')
							->having('anio','>',2000)
							->where('lugar_id','=',$idMunicipio)
							->get();

		return $aniosNoticias;
	}

	public function listAjax()
	{
		$municipios = DB::table('municipios')
					->select('id', 'nombre')
					->where('departamento_id', '33')
					->get();
		return $municipios;
	}

	public function getJson()
	{
		return Lugar::municipios()->get();
	}
}
