<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Entidad;

use Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function verificarTema($idMunicipio){

    	$premium =  Entidad::verificarPremium($idMunicipio);
        if($premium){
            request()->session()->put('lugarPremium', $premium);
        }else{
            request()->session()->forget('lugarPremium');
        }
    }

    public function saveFile($file,$ruta='',$name='')
    {

        $mime=$file->getMimeType();

        if($mime=='image/jpeg')
            $extension = '.jpg';
        else if($mime=='image/png')
            $extension = '.png';
        else if($mime=='image/gif')
            $extension = '.gif';
        else
            $extension = '.'.$file->getClientOriginalExtension();

        if($name=='')
            $name = time();

        $file->move($ruta,$name.$extension);

        return $ruta.'/'.$name.$extension;
    }

    public function guardarImagen($archivo,$seccion)
    {
        $ruta = 'multimedia/images/'.$seccion.'/'.date('Y').'/'.date('m');
        return $this->saveFile($archivo,$ruta);
    }

    public function completarDatosNoticia($noti)
    {
        $noti->lugar->nombre;
        $noti->f_pub = $noti->f_pub;
        $noti->usuario->nombre;
        $noti->usuario->apellido;
        $noti->usuario->nombre_completo = $noti->usuario->nombre_completo;
        $noti->usuario->imagen = $noti->usuario->imagen;
        $noti->imagenes = $noti->imagenes;
        $noti->documentos = $noti->documentos;
        $noti->audios = $noti->audios;
        $noti->videos = $noti->videos;


        if($noti->fuente)
            $noti->fuente->nombre;
        else
            $noti->fuente = (object) [ 'nombre' => ''];
        
        $noti->like = 0;

        if(request()->is('api') || request()->is('api/*')){
            if(request()->input('token','undefined') == 'undefined')
                return $noti;
            // this will set the token on the object
            \JWTAuth::parseToken();

            // and you can continue to chain methods
            $user = \JWTAuth::parseToken()->authenticate();

            if($user){
                $likesNoticia = Auth::user()
                                ->likes()
                                ->where('seccion_type','Noticia')
                                ->where('seccion_id',$noti->id)
                                ->count() + 0;

                $noti->like = ($likesNoticia) ? 1 : 0;
            }
        }else{
            $user = Auth::user();

            if($user){
                $likesNoticia = Auth::user()
                                ->likes()
                                ->where('seccion_type','Noticia')
                                ->where('seccion_id',$noti->id)
                                ->count() + 0;

                $noti->like = ($likesNoticia) ? 1 : 0;
            }
        }

        return $noti;
    }

    public function completarDatosEvento($evt)
    {
        if($evt->lugar)
            $evt->lugar->nombre;
        else
            $evt->lugar = (object) ['nombre'=> 'Cundinamarca'];

        $evt->usuario->nombre;
        $evt->usuario->apellido;
        $evt->usuario->nombre_completo = $evt->usuario->nombre_completo;
        $evt->usuario->imagen = $evt->usuario->imagen;
        $evt->descripcion = $evt->descripcion;
        $evt->dia = $evt->dia;
        $evt->mes_corto = $evt->mes_corto;
        $evt->dia_fin = $evt->dia_fin;
        $evt->mes_fin_corto = $evt->mes_fin_corto;

        $evt->imagenes = $evt->imagenes;
        $evt->documentos = $evt->documentos;
        $evt->audios = $evt->audios;
        $evt->videos = $evt->videos;

        $evt->like = 0;

        if(request()->is('api') || request()->is('api/*')){
            if(request()->input('token','undefined') == 'undefined')
                return $evt;
            // this will set the token on the object
            \JWTAuth::parseToken();

            // and you can continue to chain methods
            $user = \JWTAuth::parseToken()->authenticate();
            if($user){
                $likesEvento = Auth::user()
                                ->likes()
                                ->where('seccion_type','Evento')
                                ->where('seccion_id',$evt->id)
                                ->count() + 0;
                                
                $evt->like = ($likesEvento) ? 1 : 0;
            }
        }else{
            $user = Auth::user();
            
            if($user){
                $likesEvento = Auth::user()
                                ->likes()
                                ->where('seccion_type','Evento')
                                ->where('seccion_id',$evt->id)
                                ->count() + 0;
                                
                $evt->like = ($likesEvento) ? 1 : 0;
            }
        }

        return $evt;
    }


    public function completarDatosColumna($columna)
    {
        $columna->lugar->nombre;
        $columna->f_pub = $columna->f_pub;
        $columna->usuario->nombre;
        $columna->usuario->apellido;
        $columna->usuario->nombre_completo = $columna->usuario->nombre_completo;
        $columna->usuario->imagen = $columna->usuario->imagen;
        $columna->imagenes = $columna->imagenes;
        $columna->documentos = $columna->documentos;
        $columna->audios = $columna->audios;
        $columna->videos = $columna->videos;


        if($columna->fuente)
            $columna->fuente->nombre;
        else
            $columna->fuente = (object) [ 'nombre' => ''];
        
        $columna->like = 0;

        if(request()->is('api') || request()->is('api/*')){
            if(request()->input('token','undefined') == 'undefined')
                return $columna;
            // this will set the token on the object
            \JWTAuth::parseToken();

            // and you can continue to chain methods
            $user = \JWTAuth::parseToken()->authenticate();

            if($user){
                $likesColumna = Auth::user()
                                ->likes()
                                ->where('seccion_type','Columna')
                                ->where('seccion_id',$columna->id)
                                ->count() + 0;

                $columna->like = ($likesColumna) ? 1 : 0;
            }
        }else{
            $user = Auth::user();

            if($user){
                $likesColumna = Auth::user()
                                ->likes()
                                ->where('seccion_type','Columna')
                                ->where('seccion_id',$columna->id)
                                ->count() + 0;

                $columna->like = ($likesColumna) ? 1 : 0;
            }
        }

        return $columna;
    }



    public function completarDatosSitio($sitio)
    {
        $sitio->lugar->nombre;


        $sitio->imagenes = $sitio->imagenes;
        $sitio->documentos = $sitio->documentos;
        $sitio->audios = $sitio->audios;
        $sitio->videos = $sitio->videos;

        $sitio->like = 0;

        if(request()->is('api') || request()->is('api/*')){
            if(request()->input('token','undefined') == 'undefined')
                return $sitio;
            // this will set the token on the object
            \JWTAuth::parseToken();

            // and you can continue to chain methods
            $user = \JWTAuth::parseToken()->authenticate();
            if($user){
                $likesSitio = Auth::user()
                                ->likes()
                                ->where('seccion_type','SitioInteres')
                                ->where('seccion_id',$sitio->id)
                                ->count() + 0;
                                
                $sitio->like = ($likesSitio) ? 1 : 0;
            }
        }else{
            $user = Auth::user();
            
            if($user){
                $likesSitio = Auth::user()
                                ->likes()
                                ->where('seccion_type','SitioInteres')
                                ->where('seccion_id',$sitio->id)
                                ->count() + 0;
                                
                $sitio->like = ($likesSitio) ? 1 : 0;
            }
        }

        return $sitio;
    }
}
