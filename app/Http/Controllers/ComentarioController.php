<?php  
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Comentario;

use Redirect;
use Input;
use Auth;

class ComentarioController extends Controller
{
	/**
	 * Guarda un nuevo registro en la base de datos
	 *
	 * @return Response
	 */
	public function store()
	{
		$comentario = new Comentario;
		$comentario->comentario = Input::get('comentario');
		$comentario->usuario_id = Auth::user()->id;
		$comentario->seccion_type = Input::get('seccion');
		$comentario->seccion_id = Input::get('seccion_id');
		$comentario->comentario_id = Input::get('comentario_id',0);
		$comentario->save();

		return Redirect::back()->with('message', 'Operation Successful !');
	}
}