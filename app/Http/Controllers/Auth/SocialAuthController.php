<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Socialite;
use Session;
use URL;
use App\Usuario;
use App\Aspirante;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SocialAuthController extends Controller
{
    // Metodo encargado de la redireccion a Facebook
    public function redirectToProvider($provider,$usertype=null)
    {
        session_start();

        if(isset($usertype) && !is_null($usertype)){
            $_SESSION = [];
            $_SESSION['usertype'] = $usertype;
        }

        if($provider=='facebook'){
            return Socialite::driver('facebook')->fields([
                'first_name', 'last_name', 'email', 'gender', 'birthday'
            ])->scopes([
                'email', 'user_birthday'
            ])->redirect();
        }else{
            return Socialite::driver($provider)->redirect();
        }
    }
    
    // Metodo encargado de obtener la información del usuario
    public function handleProviderCallback(Request $request,$provider)
    {
        if($provider=='twitter' && !$request->has('oauth_token')){
            return redirect('registro')->with([
                'errorMessage' => 'Debes autorizar tu inicio de sesión en redes sociales para poder disfrutar de nuestros beneficios.'
            ]);
        }

        try {

            if($provider=='facebook'){
                // Obtenemos los datos del usuario
                $social_user = Socialite::driver('facebook')->fields(
                    ['first_name', 'last_name', 'email', 'gender', 'birthday']
                )
                ->stateless()
                ->user();
            }else{
                $social_user = Socialite::driver($provider)->user();
            }

        }catch (\Exception $e) {
            return redirect('registro')->with([
                'msjError' => 'Ha ocurrido un error, recuerda que debes autorizar tu inicio de sesión en redes sociales para poder disfrutar de nuestros beneficios.'
            ]);
            \Log::error('ERROR DE AUTENTICACION CON REDES SOCIALES: '.$e->getMessage());
        }
        
        // Comprobamos si el usuario ya existe
        if ($user = Usuario::where('provider', $provider)->where('provider_id', $social_user->id)->first()) { 
            return $this->authAndRedirect($user); // Login y redirección
        } else if ($user = Usuario::where('correo', $social_user->email)->first()) { 
            return $this->authAndRedirect($user); // Login y redirección
        } else {
            session_start();
            // En caso de que no exista creamos un nuevo usuario con sus datos.
            $userAr = $social_user->user;
            $usertype = isset($_SESSION['usertype']) ? $_SESSION['usertype'] : null;

            if($provider == 'facebook'){
                $nombres = $userAr['first_name'];
                $apellidos = $userAr['last_name'];
                $genero = (isset($userAr['gender']) && $userAr['gender'] == 'male') ? 'Masculino' : 'Femenino';
            }else if($provider == 'google'){    
                $nombres = $userAr['name']['givenName'];
                $apellidos = $userAr['name']['familyName'];
                $genero = (isset($userAr['gender']) && $userAr['gender'] == 'male') ? 'Masculino' : 'Femenino';
            }else if($provider == 'twitter'){
                $nombres = $social_user->name;
                $apellidos = '';
                $genero = '';
            }

            if(is_null($usertype)){
                session_destroy();
                return redirect('registro')->with([
                    'msjError' => 'Elige que tipo de usuario eres.',
                    'facebook' => false,
                    'id' => $social_user->id,
                    'first_name' => $nombres,
                    'last_name' => $apellidos,
                    'email' => $social_user->email,
                    'gender' => $genero,
                    'photo' => $social_user->avatar_original,
                ]);                
            }

            if($usertype == 'aspirante'){
                $usuario = new Usuario();

                $usuario->correo = $social_user->email;
                $usuario->nombre = $nombres;
                $usuario->apellido = $apellidos;
                $usuario->rol_id = 1;
                $usuario->tipo = 1;
                $usuario->provider = $provider;
                $usuario->provider_id = $social_user->id;
                $usuario->tipo = 1;
                $usuario->contrasena = bcrypt($social_user->id);
                $usuario->estado = 1;

                $usuario->save();

                $aspirante = new Aspirante();
                $aspirante->usuario_id = $usuario->id;
                $aspirante->tipo_educacion_id = 1;
                $aspirante->foto = json_encode([$social_user->avatar_original]);
                $aspirante->save();

                return $this->authAndRedirect($usuario); // Login y redirección

            }else{
                session_destroy();
                return redirect('registro')->with([
                    'facebook' => true,
                    'id' => $social_user->id,
                    'first_name' => $nombres,
                    'last_name' => $apellidos,
                    'email' => $social_user->email,
                    'gender' => $genero,
                    'photo' => $social_user->avatar_original,
                ]);
            }

            session_destroy();
            return $this->authAndRedirect($user); // Login y redirección
        }
    }

    // Login y redirección
    public function authAndRedirect($user)
    {
        Auth::login($user,true);
        return redirect('/usuario/perfil');
    }

    public function apiAuthAndRedirect($user)
    {   
        /*
        return response()->json([
            'usuario' => $user,
            'auth' => Auth::login($user,true),
            'request' => request()->all(),
            'data'=> [ 'token' => 'felipe' ]
        ]);
        */

        Auth::login($user,true);
        $usuario = Auth::user();
        $usuario->imagen = $usuario->imagen;
        $token = \JWTAuth::fromUser($usuario);
        return response()->json(
            ['mensaje' => "Iniciaste sesión correctamente", "usuario" => $usuario, 'data'=> [ 'token' => $token ] ]
        );
    }

     // Metodo encargado de obtener la información del usuario
    public function appSocialLogin(Request $request,$provider)
    {
        //return response()->json($request->all());
        if($provider == 'facebook'){
            $nombres = $request->name;
            $userId = $request->id;
            $foto = $request->picture;
            $email = $request->email;
         }else if($provider == 'google'){    
            $nombres = $request->givenName+" "+$request->familyName;
            $userId = $request->userId;
            $foto = $request->imageurl;
            $email = $request->email;
         }else if($provider == 'twitter'){
            $nombres = $request->userName;
            $userId = $request->userId;
            $foto = $request->profile_image_url;
            $email = null;
         }

         // Comprobamos si el usuario ya existe
         if ($user = Usuario::where('provider', $provider)->where('provider_id', $userId)->first()) { 
             return $this->apiAuthAndRedirect($user); // Login y redirección
         } else if ($user = Usuario::where('correo', $email)->first()) { 
             return $this->apiAuthAndRedirect($user); // Login y redirección
         } else {
            $usertype = 'aspirante';
            $apellidos = '';

             if($usertype == 'aspirante'){
                 $usuario = new Usuario();
 
                 $usuario->correo = $email;
                 $usuario->nombre = $nombres;
                 $usuario->apellido = $apellidos;
                 $usuario->rol_id = 1;
                 $usuario->tipo = 1;
                 $usuario->provider = $provider;
                 $usuario->provider_id = $userId;
                 $usuario->tipo = 1;
                 $usuario->contrasena = bcrypt($userId);
                 $usuario->estado = 1;
 
                 $usuario->save();
 
                 $aspirante = new Aspirante();
                 $aspirante->usuario_id = $usuario->id;
                 $aspirante->tipo_educacion_id = 1;
                 $aspirante->foto = json_encode([$foto]);
                 $aspirante->save();
                
                 return $this->apiAuthAndRedirect($usuario); // Login y redirección
 
             }else{
                return response()->json(
                    ['error' => "No es posible iniciar sesión usando $provider"]
                );
             }
             
             return $this->apiAuthAndRedirect($user); // Login y redirección
         }
     }
}