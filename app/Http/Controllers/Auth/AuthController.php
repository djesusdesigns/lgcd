<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Mail\Message;
use Illuminate\Support\Str;

use Auth;
use Mail;
use Password;
use DB;
use Validator;
use JWTAuth;
use App\Entidad;
use App\Usuario;
use App\Aspirante;
use App\Sucursal;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AuthController extends Controller{

    public function index(Request $request){
        return view('general.login');
    }

    public function login(Request $request){
        $correo = $request->correo;
        $contrasena = $request->contrasena;
        $remember = true;

        if ( Auth::attempt(['correo' => $correo, 'password' => $contrasena, 'estado'=>1],$remember) ) {
            
            if (request()->is('api')){
                $usuario = Auth::user();
                $usuario->imagen = $usuario->imagen;
                return response()->json(['mensaje' => 'Ha ingresado correctamente!', 'usuario' => $usuario->imagen ]);
            }else{
                return redirect('/usuario/perfil');
            }

        }else{
            if (request()->is('api'))
                return response()->json(['error' => 'Correo o contraseña incorrectos']);
            else
                return view('general.login')->with(['msjError' => 'Correo o contraseña incorrectos']);
        }
    }

    public function logout(Request $request){
        Auth::logout();
        return redirect('/');
    }


    public function mostrar_registro(){

        return view('general.registro');
    }

    public function registro(Request $request){
        //**param** nombre, apellido, cedula, correo, clave

        $validator = Validator::make($request->all(), [
            'foto' => 'max:10000',
            'perfil' => 'required|in:Persona,Empresa',
            'nombres' => 'required_if:perfil,==,Persona|nullable',
            'apellidos' => 'required_if:perfil,==,Persona|nullable',
            'razon_social' => 'required_if:perfil,==,Empresa|nullable',
            'nit' => 'required_if:perfil,==,Empresa|nullable',
            'correo' => 'required|email|unique:usuarios,correo',
            'contrasena' => 'required|min:8|confirmed',
            'terminos' => 'accepted',
        ]);

        if ($validator->fails()){
            if (request()->is('api/*'))
                return response()->json(['error' => $validator->errors()->first()]);
            else
                return view('general.registro')->with(['errors' => $validator->errors() ]);
        }
        

        $reporte = DB::transaction(function () use($request) {

            $usuario = new Usuario();

            $usuario->correo = $request->correo;
            $usuario->nombre = $request->nombres;
            $usuario->apellido = $request->apellidos;
            $usuario->rol_id = ($request->perfil == 'Persona') ? 1 : 2;
            $usuario->tipo = 1;
            $usuario->contrasena = bcrypt($request->contrasena);
            $usuario->estado = 0;

            $usuario->save();

            if($request->perfil == 'Persona'){
                $aspirante = new Aspirante();
                $aspirante->usuario_id = $usuario->id;
                $aspirante->tipo_educacion_id = 1;
                $aspirante->lugar_id = $request->lugar_id;
                $aspirante->save();
            }else{
                $empresa = new Entidad();
                $empresa->usuario_id = $usuario->id;
                $empresa->nombre = $request->razon_social;
                $empresa->razon_social = $request->razon_social;
                $empresa->nit = $request->nit;
                $empresa->correo_privado = $request->correo;
                $empresa->tipo = "Empresa";
                $empresa->save();

                $request->tipo_negocio = 1;

                if($request->tipo_negocio){
                    $horario = [];
                    array_push( $horario,["", ""]);
                    array_push( $horario,["", ""]);
                    $horario = json_encode($horario);

                    $telefonos = [];

                    array_push( $telefonos, "" );
                    array_push( $telefonos, "" );

                    array_push( $telefonos, "" );
                    array_push( $telefonos, "" );

                    $telefonos = json_encode($telefonos);

                    $sucursal = new Sucursal;

                    $sucursal->entidad_id = $empresa->id;
                    $sucursal->telefonos = $telefonos;
                    $sucursal->horario = $horario;
                    $sucursal->lugar_id = $request->input('lugar_id',25);
                    $sucursal->tipo = "Principal";

                    $sucursal->save();
                }
            }
            

            $token = hash_hmac('sha256', Str::random(40), Str::random(10));

            DB::table('comprobar_correo')->insert(
                ['correo' => $usuario->correo, 'token' => $token]
            );

            \Mail::send('mail.registro-entidad', ['token' => $token], function ($m)  use($usuario){
                $m->from('info@laguiacundinamarca.com', 'La Guía Cundinamarca');
                $m->to($usuario->correo, $usuario->nombre)->subject('Verificar correo - La Guía Cundinamarca');
            });

        });

        if (request()->is('api/*'))
            return response()->json(['mensaje' => 'Hemos enviado un mensaje a tu correo electrónico con instrucciones para que disfrutes nuestro contenido.' ]);
        else
            return view('general.registro')->with(['msjExito' => 'Hemos enviado un mensaje a tu correo electrónico con instrucciones para que disfrutes nuestro contenido.' ]);
    }

    public function verificar_correo($token){

        $comprobacion = DB::table('comprobar_correo')->where('token', $token)->first();
        if($comprobacion){
            $usuario = Usuario::where("correo", $comprobacion->correo)->first();
            $comprobacion = DB::table('comprobar_correo')->where('token', $token)->delete();
            $usuario->estado = 1;
            $usuario->save();

            Auth::login( $usuario, true);

            return response()->view('general.verificar_correo', ['exito' => true]);
        }else{
            return response()->view('general.verificar_correo', ['exito' => false]);
        }
    }

    //METODOS PARA RECUPERAR LA CONTRASEÑA

    public function recuperar_password(){
        return response()->view("general.recuperar_password");
    }

    public function enviar_correo(Request $request){
        $this->validate($request, [
            'correo' => 'required|email|exists:usuarios,correo',
        ]);

        $usuario = Usuario::where("correo", $request->correo)->first();

        $token = hash_hmac('sha256', Str::random(40), Str::random(10));

        DB::table('password_resets')->insert([
            'email' => $usuario->correo,
            'token' => $token,
            'created_at' => date("Y-m-d H:i:s")
        ]);

        \Mail::send('mail.password', ['token' => $token, 'user' => $usuario], function ($m)  use($usuario){
            $m->from('info@laguiacundinamarca.com', 'La Guía Cundinamarca');
            $m->to($usuario->correo, $usuario->nombre)->subject('Recuperar contraseña - La Guía Cundinamarca');
        });

        if (request()->is('api/*')) {
            return response()->json(['mensaje' => "Hemos enviado un mensaje a su correo electrónico con las instrucciones para que recupere su contraseña."]);
        }else{
            return redirect()->back()->with('status', "Hemos enviado un mensaje a su correo electrónico con las instrucciones para que recupere su contraseña.");
        }

    }

    public function verificar_token_password($token = null){
        if (is_null($token)) {
            throw new NotFoundHttpException;
        }

        return view('general.resetear_password')->with('token', $token);
    }

    public function resetear_password(Request $request){

        $validator = Validator::make($request->all(), [
            'token' => 'required|exists:password_resets,token',
            'correo' => 'required|email|exists:usuarios,correo',
            'contrasena' => 'required|confirmed|min:8',
        ]);

        if (!$validator->fails()) {
            $token = DB::table('password_resets')->where('token', $request->token)->first();
            $usuario = Usuario::where("correo", $request->correo)->first();
            if($usuario->correo == $token->email){
                $token = DB::table('password_resets')->where('token', $request->token)->delete();
                $usuario->contrasena = bcrypt($request->contrasena);
                $usuario->estado = 1;
                $usuario->save();
                return redirect('/login')->with('status', 'se ha restablecido su contraseña exitosamente!');
            }else{
                return redirect('/login')->with('error', 'token es inválido');
            }
        }else{
            return redirect()->back()
                ->withInput($request->only('correo'))
                ->with('error', $validator->errors()->first());
        }
    }

    public function loginj(Request $request)
    {
        $credentials = [
            'correo' => $request->correo,
            'password' => $request->contrasena,
        ];

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['success' => false, 'error' => 'Correo o contraseña incorrectos.']);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'error' => 'Error del sistema.'], 500);
        }

        $usuario = Auth::user();
        $usuario->imagen = $usuario->imagen;
        // all good so return the token
        return response()->json(['mensaje' => 'Ha ingresado correctamente!','success' => true, 'usuario' => $usuario, 'data'=> [ 'token' => $token ]]);
    }
    /**
     * Log out
     * Invalidate the token, so user cannot use it anymore
     * They have to relogin to get a new token
     *
     * @param Request $request
     */
    public function logoutj(Request $request) {
        $this->validate($request, ['token' => 'required']);
        try {
            JWTAuth::invalidate($request->input('token'));
            return response()->json(['success' => true]);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
        }
    }

}