<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lugar;
use App\SitioInteres;

use View;
use DB;

class SitioInteresController extends Controller
{
	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */

	public function show($id)
	{
		$sitioInteres = SitioInteres::find($id);

		if(!$sitioInteres)
			return abort(404);
	
		$otrosSitiosInteres = SitioInteres::take(5)
							->where('lugar_id',$sitioInteres->lugar->id)
							->orderByRaw("RAND()")
                            ->get();
		
		//$comentarios = $SitioInteres->comentarios;		

		return View::make('sitios-interes.show')
					   ->with('sitio', $sitioInteres)
					   ->with('otrosSitiosInteres', $otrosSitiosInteres);
		
		
	}

}
