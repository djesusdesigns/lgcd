<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Archivo;
use App\Comentario;
use App\Entidad;
use App\Telefono;
use App\Lugar;
use App\Usuario;
use App\Sucursal;
use App\Actividad;

use Auth;
use Validator;
use Storage;
use DB;

class MedioController extends Controller{

    public function index(Request $request){
        $error = false;

        $actividades = Actividad::all();
        $horario = $request->input('horario');

        $empresas = Entidad::join('sucursales', 'entidades.id', '=', 'sucursales.entidad_id')
            ->join('usuarios', 'entidades.usuario_id', '=', 'usuarios.id')
            ->leftjoin('entidades_actividades','entidades_actividades.entidad_id','=','entidades.id')
            ->join('lugares','sucursales.lugar_id','=','lugares.id')
            ->select('sucursales.id', 'entidades.razon_social', 'sucursales.direccion', 'entidades.descripcion', 'entidades.web', 'entidades.logo', 'entidades.requiere', 'entidades.ofrece', 'sucursales.telefonos','sucursales.horario','lugares.nombre as municipio');

        if( sizeof($request->all()) == 0){
            $request->session()->forget('lugar');
            $request->session()->forget('categoria');
            $request->session()->forget('requiere');
            $request->session()->forget('ofrece');
        }

        if($request->has('borrar-lugar')){
            $request->session()->forget('lugar');
        }

        if($request->has('borrar-categoria')){
            $request->session()->forget('categoria');
        }

        if($request->has('borrar-requiere')){
            $request->session()->forget('requiere');
        }

        if($request->has('borrar-ofrece')){
            $request->session()->forget('ofrece');
        }

        $medios = $empresas
            ->where(function ($query) use ($request){
                $query
                    ->orWhere("entidades_actividades.actividad_id",40)
                    ->orWhere("entidades_actividades.actividad_id",722)
                    ->orWhere("entidades_actividades.actividad_id",1646)
                    ->orWhere("entidades_actividades.actividad_id",1405)
                    ->orWhere("entidades_actividades.actividad_id",1441)
                    ->orWhere("entidades_actividades.actividad_id",1565);
            });

        if ($request->has('categoria') || session('categoria') ) {
            $actividadId = $request->has('categoria') ? $request->categoria : session('categoria')['id'];
            $actividad = Actividad::find($actividadId);

            $request->session()->put('categoria',[
                'id'=>$actividad->id,
                'nombre'=>$actividad->nombre
            ]);

            $empresas = $empresas->where("entidades_actividades.actividad_id",$request->categoria);
        }

        if($request->has('lugar') || session('lugar') || session('lugarPremium')){
            $lugarId = $request->has('lugar') ? $request->lugar : session('lugar')['id'];
            $lugarId = session('lugarPremium') ? session('lugarPremium')['lugar'] : $lugarId;
            
            $municipio = Lugar::find($lugarId);

            $request->session()->put('lugar',[
                'id'=>$municipio->id,
                'nombre'=>$municipio->nombre
            ]);

            $this->verificarTema($municipio->id);

            $empresas = $empresas->where("sucursales.lugar_id",$request->municipio);
        }
            
        $medios = $medios
            ->where('entidades.tipo', 'Empresa')
            ->orderBy('usuarios.tipo', 'desc')
            ->groupBy('entidades.id')
            ->paginate(8);

        
        if(isset($municipio)){
            return view('medios.index',[
                'error' => $error,
                'medios' => $medios,
                'busquedaLugar' => $municipio,
                'actividades' => $actividades
            ]);
        }else{
            return view('medios.index',[
                'error' => $error,
                'medios' => $medios,
                'actividades' => $actividades
            ]);

        }
    }

    public function consultar($sucursalId){
        // razon-social, nombre, descripción, direccion, municipio, telefonos, web, imagen sucursal, horario, logo, estacionamiento, domicilio,
        //otras sucursales: municipio, direccion, horario, telefonos, domicilio, estacionamiento
        $error = false;

        $entidad = Sucursal::find($sucursalId)->entidad;
        $eventos = $entidad->usuario->eventos;
        $empresa = Sucursal::join('entidades', 'sucursales.entidad_id', '=', 'entidades.id')
            ->join('lugares', 'lugares.id', '=', 'sucursales.lugar_id')
            ->select('entidades.id as empresa_id','sucursales.id','sucursales.entidad_id')
            ->addSelect('sucursales.imagen','sucursales.direccion','sucursales.horario')
            ->addSelect('sucursales.domicilio','sucursales.estacionamiento','sucursales.telefonos')
            ->addSelect('sucursales.latitud','sucursales.longitud','lugares.nombre as municipio')
            ->addSelect('entidades.descripcion','entidades.logo','entidades.web')
            ->addSelect('entidades.razon_social','sucursales.lugar_id')
            ->where('sucursales.id',$sucursalId)
            ->first();

        if($empresa){
            $sucursales = Sucursal::join('entidades', 'sucursales.entidad_id', '=', 'entidades.id')
                ->join('lugares', 'lugares.id', '=', 'sucursales.lugar_id')
                ->select('sucursales.id','sucursales.direccion','sucursales.horario')
                ->addSelect('sucursales.telefonos','sucursales.domicilio')
                ->addSelect('sucursales.estacionamiento','sucursales.latitud')
                ->addSelect('sucursales.longitud','lugares.nombre as municipio')
                ->where('entidades.id', $empresa->entidad_id)
                ->where('sucursales.id','!=', $empresa->id)
                ->get();
                
            $empresa->horario = json_decode($empresa->horario);

            $sucursales->transform(function ($sucursal, $key) {
                $sucursal->horario = json_decode($sucursal->horario);                    
                return $sucursal;
            });

            $comentarios = Comentario::seccion('Sucursal',$empresa->id)->get();
        }else{
            $error = "No se encontró ningun medio";
            $sucursales = null;
            $comentarios = null;
        }

        $actividades = Actividad::all();

        $mediosRelacionados = Entidad::medios()
            ->join('lugares','sucursales.lugar_id','=','lugares.id')
            ->select('sucursales.id', 'entidades.razon_social', 'sucursales.direccion')
            ->addSelect('entidades.descripcion','entidades.logo','lugares.nombre as municipio')
            ->whereNotIn('entidades.id',[$empresa->empresa_id])
            ->groupBy('entidades.id')
            ->where('sucursales.lugar_id',$empresa->lugar_id)
            ->get();

        if(sizeof($mediosRelacionados)==0){
            $mediosRelacionados = Entidad::medios()
                ->join('lugares','sucursales.lugar_id','=','lugares.id')
                ->select('sucursales.id', 'entidades.razon_social', 'sucursales.direccion')
                ->addSelect('entidades.descripcion','entidades.logo','lugares.nombre as municipio')
                ->whereNotIn('entidades.id',[$empresa->empresa_id])
                ->groupBy('entidades.id')
                ->get();
        }
        
        return view('medios.show', [
            'error' => $error,
            'entidad'=> $entidad,
            'empresa'=> $empresa,
            'sucursales'=>$sucursales,
            'comentarios'=>$comentarios,
            'actividades' => $actividades,
            'eventos' => $eventos,
            'mediosRelacionados' => $mediosRelacionados
        ]);
    }

     public function enviarCorreoContacto(Request $request){

        $validator = Validator::make($request->all(), [
            'idemp'  => 'required|exists:entidades,id',
            'nombre' => 'required|string',
            'correo' => 'required|email',
            'asunto' => 'required|string',
            'mensaje' => 'required|string'
        ]);

        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return redirect()->back()->withErrors($error);
        }

        $empresa = Entidad::find($request->idemp);
        $dataMail = [
                'nombre' => $request->nombre,
                'correo' => $request->correo,
                'asunto' => $request->asunto,
                'mensaje' => $request->mensaje
            ];

        \Mail::send('mail.mail-contacto-empresa', $dataMail, function ($m) use ($request,$empresa){
            $m->from($request->correo,$request->nombre);
            $correoEmpresa = (strlen($empresa->correo_privado)) ? $empresa->correo_privado : $empresa->usuario->correo;
            $m->to($correoEmpresa,$empresa->razon_social)->subject($request->asunto);
        });

        return redirect()->back()->with('mensajeForm','Correo enviado exitosamente');
    }
    
    public function busquedaPorMunicipio(Request $request,$slugMunicipio)
    {
        $slugArray = explode("-",$slugMunicipio);
        $idMunicipio = $slugArray[sizeof($slugArray)-1];
        $request->merge(['lugar' => $idMunicipio]);
        return $this->index($request);
    }

    public function enviarCorreoContacto2(Request $request){

        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string',
            'correo' => 'required|email',
            'asunto' => 'required|string',
            'mensaje' => 'required|string'
        ]);

        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return redirect()->back()->withErrors($error);
        }

        $dataMail = [
                'nombre' => $request->nombre,
                'correo' => $request->correo,
                'asunto' => $request->asunto,
                'mensaje' => $request->mensaje
            ];

        \Mail::send('mail.mail-contacto-empresa', $dataMail, function ($m){
            $m->from('info@laguiacundinamarca.com', 'La Guía cundinamarca');
            $m->to('webmaster@impricol.com', 'Impricol')->subject('Contacto LGC:');
        });

        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string',
            'correo' => 'required|email',
            'asunto' => 'required|string',
            'mensaje' => 'required|string'
        ]);

        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return redirect()->back()->withErrors($error);
        }

        $dataMail = [
                'nombre' => $request->nombre,
                'correo' => $request->correo,
                'asunto' => $request->asunto,
                'mensaje' => $request->mensaje
            ];

        \Mail::send('mail.contactarResponse', $dataMail, function ($m) use ($request){
            $m->from('info@laguiacundinamarca.com', 'La Guía cundinamarca');
            $m->to($request->correo,'')->subject('Contacto LGC:');
        });


        return redirect()->back()->with('mensajeForm','Correo enviado exitosamente');
    }

}