<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\OfertaTrabajo;
use App\Trabajo;
use App\Entidad;
use App\Lugar;
use App\Actividad;
use App\TipoContrato;

use Auth;

class OfertaTrabajoController extends Controller
{


    private $empresasGrandes;
    private $ofertasDestacadas;
    private $municipiosAsociados;
    private $categorias;

    /*
     * Muestra la lista de registros
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $this->empresasGrandes = Entidad::where('tipo','Empresa')->orderBy('numero_empleados','desc')->take(5)->get();
        $this->ofertasDestacadas = OfertaTrabajo::where('destacar',1)->orderByRaw("RAND()")->take(3)->orderBy('created_at')->get();
        $this->municipiosAsociados = Lugar::municipios()->orderByRaw("RAND()")->take(6)->get();
        $this->categorias = Actividad::all();

        $query = OfertaTrabajo::orderBy('created_at','desc')->whereNull('trabajos.deleted_at');

        if( sizeof($request->all()) == 0){
            $request->session()->forget('lugar');
            $request->session()->forget('categoria');
            $request->session()->forget('tipo_contrato');
            $request->session()->forget('salario');
            $request->session()->forget('rango_salario');
            $request->session()->forget('f_publicacion');
        }

        if($request->has('borrar-lugar')){
            $request->session()->forget('lugar');
        }

        if($request->has('borrar-categoria')){
            $request->session()->forget('categoria');
        }

        if($request->has('borrar-categoria_str')){
            $request->session()->forget('categoria_str');
        }

        if($request->has('borrar-tipo_contrato')){
            $request->session()->forget('tipo_contrato');
        }

        if($request->has('borrar-salario')){
            $request->session()->forget('salario');
        }

        if($request->has('borrar-rango_salario')){
            $request->session()->forget('rango_salario');
        }

        if($request->has('borrar-f_publicacion')){
            $request->session()->forget('f_publicacion');
        }
        
        if($request->has('lugar') || session('lugar') || session('lugarPremium')){
            $lugarId = $request->has('lugar') ? $request->lugar : session('lugar')['id'];
            $lugarId = session('lugarPremium') ? session('lugarPremium')['lugar'] : $lugarId;
            
            $municipio = Lugar::find($lugarId);

            $request->session()->put('lugar',[
                'id'=>$municipio->id,
                'nombre'=>$municipio->nombre
            ]);

            $query->where('lugar_id','=',$municipio->id);
            $this->verificarTema($municipio->id);
        }

        if($request->has('categoria') || session('categoria')){
            $request->session()->forget('categoria_str');
            $categoriaId = $request->has('categoria') ? $request->categoria : session('categoria')['id'];

            $categoria = Actividad::find($categoriaId);

            $request->session()->put('categoria',[
                'id'=>$categoria->id,
                'nombre'=>$categoria->nombre
            ]);

            $query->where('categoria_id','=',$categoriaId); 
        }

        if($request->has('categoria_str') || session('categoria_str')){
            $request->session()->forget('categoria');
            $categoriaStr = $request->has('categoria_str') ? $request->categoria_str : session('categoria_str');
            $categorias = Actividad::where('nombre','LIKE','%'.$categoriaStr.'%')->get()->pluck('id');
            $request->session()->put('categoria_str',$categoriaStr);
            $query->whereIn('categoria_id',$categorias); 
        }

        if($request->has('tipo_contrato') || session('tipo_contrato')){
            $tipoContId = $request->has('tipo_contrato') ? $request->tipo_contrato : session('tipo_contrato')['id'];

            $tipoCont = TipoContrato::find($tipoContId);

            $request->session()->put('tipo_contrato',[
                'id'=>$tipoCont->id,
                'nombre'=>$tipoCont->nombre
            ]);

            $query->where('tipo_contrato_id','=',$request->tipo_contrato);
        }

        if (($request->has('salario') && $request->has('filtrosalario')) || session('salario')){
            $salario = $request->has('salario') ? $request->salario : session('salario')['valor'];

            if($request->filtrosalario == 'menos'){
                $query->where('salario','<=',$salario);
                $request->session()->put('salario',[
                    'valor'=>$salario,
                    'comp'=>'Menos de'
                ]);
            }elseif($request->filtrosalario == 'mas'){
                $query->where('salario','>=',$salario);
                $request->session()->put('salario',[
                    'valor'=>$salario,
                    'comp'=>'Más de'
                ]);
            }
        }

        if (($request->has('salario1') && $request->has('salario2')) || session('rango_salario')){
            $salario1 = $request->has('salario1') ? $request->salario1 : session('rango_salario')['salario1'];
            $salario2 = $request->has('salario2') ? $request->salario2 : session('rango_salario')['salario2'];

            $query->whereBetween('salario',[$salario1,$salario2]);
            $request->session()->put('rango_salario',[
                    'salario1' => $salario1,
                    'salario2' => $salario2
                ]);
        }

        if ($request->has('f_publicacion') || session('f_publicacion')){

            $f_publicacion = $request->has('f_publicacion') ? $request->f_publicacion : session('f_publicacion')['tiempo'];

            $data = [];
            if($request->f_publicacion == 'hoy'){
                $inicio = date('Y-m-d 00:00:00');
                $data['str'] = 'Hoy';
            }else if($request->f_publicacion == '3dias'){
                $inicio = date('Y-m-d 00:00:00', strtotime('-3 day') );
                $data['str'] = 'Últimos 3 días';
            }else if($request->f_publicacion == '7dias'){
                $inicio = date('Y-m-d 00:00:00', strtotime('-7 day') );
                $data['str'] = 'Última semana';
            }else if($request->f_publicacion == '15dias'){
                $inicio = date('Y-m-d 00:00:00', strtotime('-15 day') );
                $data['str'] = 'Últimos 15 días';
            }else if($request->f_publicacion == 'mes'){
                $inicio = date('Y-m-d 00:00:00', strtotime('-1 month') );
                $data['str'] = 'Último mes';
            }

            $data['tiempo'] = $f_publicacion;
            $request->session()->put('f_publicacion',$data);
            $query->whereBetween("created_at",[$inicio,date('Y-m-d 23:59:59')]);
        }

        $ofertas = $query->paginate(5);


        if(isset($municipio)){
            return view('empleos.index')
                        ->with('ofertas',$ofertas)
                        ->with('busquedaLugar', $municipio)
                        ->with('categorias',$this->categorias)
                        ->with('empresasGrandes',$this->empresasGrandes)
                        ->with('ofertasDestacadas',$this->ofertasDestacadas)
                        ->with('municipiosAsociados',$this->municipiosAsociados)
                        ->with('parametros',$request->all());
        }else{
            return view('empleos.index')
                        ->with('ofertas',$ofertas)
                        ->with('categorias',$this->categorias)
                        ->with('empresasGrandes',$this->empresasGrandes)
                        ->with('ofertasDestacadas',$this->ofertasDestacadas)
                        ->with('municipiosAsociados',$this->municipiosAsociados)
                        ->with('parametros',$request->all()); 
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $oferta = OfertaTrabajo::find($id);
        $empresa = $oferta->entidad;
        $this->categorias = Actividad::all();
        $ofertasSimilares =  OfertaTrabajo::where('categoria_id',$oferta->categoria_id)
                                                ->orWhere('titulo','LIKE','%'.$oferta->titulo.'%')
                                                ->whereNotIn('id',[$oferta->id])
                                                ->orderByRaw("RAND()")
                                                ->take(5)
                                                ->get();

        return view('empleos.show')
                        ->with('categorias',$this->categorias)
                        ->with('ofertasSimilares',$ofertasSimilares)
                        ->with('empresa',$empresa)
                        ->with('oferta',$oferta);
    }

    public function busquedaPorMunicipio(Request $request,$slugMunicipio)
    {
        $slugArray = explode("-",$slugMunicipio);
        $idMunicipio = $slugArray[sizeof($slugArray)-1];
        $request->merge(['lugar' => $idMunicipio]);
        return $this->index($request);
    }
    
}
