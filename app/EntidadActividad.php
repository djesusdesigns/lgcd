<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// muestra que actividades economicas realiza un comercio/empresa

class EntidadActividad extends Model
{
    protected $table = 'entidades_actividades';
    public $timestamps = false;

    public function entidad(){
		return $this->belongsTo('App\Entidad','entidad_id', 'id');
	}

    public function actividad(){
		return $this->belongsTo('App\Actividad','actividad_id', 'id');
	}
}
