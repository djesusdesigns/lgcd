<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use URL;

class SitioInteresUsuario extends Model
{
    protected $table = 'sitios_interes_usuario';


    public function lugar(){
        return $this->belongsTo('App\Lugar', 'lugar_id');
    }


     public function usuario(){
        return $this->belongsTo('App\Usuario', 'usuario_id');
    }


    public function comentarios()
    {
        return $this->morphMany('App\Comentario', 'seccion');
    }

    public function likes()
    {
        return $this->morphMany('App\Like', 'seccion');
    }

    public function archivos()
    {
        return $this->morphMany('App\Archivo', 'modelo');
    }

     public function imagenes()
    {
        return $this->morphMany('App\Archivo', 'modelo')->where('tipo','Imagen');
    }

     public function videos()
    {
        return $this->morphMany('App\Archivo', 'modelo')->where('tipo','Video');
    }

    public function getUrlAttribute(){
        return URL::to('sitio-interes-usuario/' . $this->id .'/'.str_slug($this->nombre) );
    }

}
