<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use URL;

// representa los eventos que pueden registrar todo tipo de usuarios

class Evento extends Model
{
    protected $table = 'eventos';

    public function lugar(){
        return $this->belongsTo('App\Lugar', 'lugar_id');
    }

    public function usuario(){
        return $this->belongsTo('App\Usuario', 'usuario_id');
    }

    static function activos(){
        return Evento::where('f_finalizacion','>',date('Y-m-d'));
    }

    public function comentarios()
    {
        return $this->morphMany('App\Comentario', 'seccion');
    }

    public function likes()
    {
        return $this->morphMany('App\Like', 'seccion');
    }

    public function getUrlAttribute(){
        return URL::to('evento/' . $this->id .'/'.str_slug($this->titulo) );
    }

    public function getDiaAttribute(){
        $locDate = strftime("%d",strtotime($this->f_realizacion));
        return strtoupper($locDate[0]).substr($locDate, 1);
    }

    public function getDiaFinAttribute(){
        $locDate = strftime("%d",strtotime($this->f_finalizacion));
        return strtoupper($locDate[0]).substr($locDate, 1);
    }

    public function getMesCortoAttribute(){
        $locDate = strftime("%b",strtotime($this->f_realizacion));
        return strtoupper($locDate[0]).substr($locDate, 1);
    }

    public function getMesAttribute(){
        $locDate = strftime("%B",strtotime($this->f_realizacion));
        return strtoupper($locDate[0]).substr($locDate, 1);
    }

    public function getMesFinCortoAttribute(){
        $locDate = strftime("%b",strtotime($this->f_finalizacion));
        return strtoupper($locDate[0]).substr($locDate, 1);
    }

    public function getDescripcionAttribute(){
        return str_limit(strip_tags($this->contenido),135);
    }

    public function archivos()
    {
        return $this->morphMany('App\Archivo', 'modelo');
    }

    public function imagenes()
    {
        return $this->morphMany('App\Archivo', 'modelo')->where('tipo','Imagen');
    }

    public function audios()
    {
        return $this->morphMany('App\Archivo', 'modelo')->where('tipo','Audio');
    }

    public function documentos()
    {
        return $this->morphMany('App\Archivo', 'modelo')->where('tipo','Documento');
    }

    public function videos()
    {
        return $this->morphMany('App\Archivo', 'modelo')->where('tipo','Video');
    }
}
