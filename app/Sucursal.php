<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    protected $table = 'sucursales';

    public function lugar(){
        return $this->belongsTo('App\Lugar', 'lugar_id');
    }

    public function entidad(){
        return $this->belongsTo('App\Entidad', 'entidad_id');
    }
}
