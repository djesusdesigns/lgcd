<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// alamacena los numeros de emergencia de los municipio
class NumEmergencia extends Model
{
    protected $table = 'num_emergencias';
    public $timestamps = false;

    public function lugar(){
        return $this->belongsTo('App\Lugar', 'lugar_id');
    }
}
