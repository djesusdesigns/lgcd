<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use URL;

//representa las noticias que se muestran en la guia

class Noticia extends Model
{
    protected $table = 'noticias';

    public function lugar(){
        return $this->belongsTo('App\Lugar', 'lugar_id');
    }

    public function fuente(){
        return $this->belongsTo('App\Fuente', 'fuente_id');
    }

    public function usuario(){
        return $this->belongsTo('App\Usuario', 'usuario_id');
    }

    public function getFPubAttribute(){
        $locDate = strftime("%b %d, %Y",strtotime($this->f_publicacion));
        return strtoupper($locDate[0]).substr($locDate, 1);
    }

    public function getUrlAttribute(){
        return URL::to('noticia/' . $this->id .'/'.str_slug($this->titulo) );
    }

    public function comentarios()
    {
        return $this->morphMany('App\Comentario', 'seccion');
    }

    public function likes()
    {
        return $this->morphMany('App\Like', 'seccion');
    }

    //se usa regularmente, filtra las noticias que el usuario ha marcado con una fecha
    //de publicación especifica
    static function publicadas(){
        return Noticia::where('f_publicacion','<=',date('Y-m-d'));
    }
    
    public function archivos()
    {
        return $this->morphMany('App\Archivo', 'modelo');
    }

    public function imagenes()
    {
        return $this->morphMany('App\Archivo', 'modelo')->where('tipo','Imagen');
    }

    public function audios()
    {
        return $this->morphMany('App\Archivo', 'modelo')->where('tipo','Audio');
    }

    public function documentos()
    {
        return $this->morphMany('App\Archivo', 'modelo')->where('tipo','Documento');
    }

    public function videos()
    {
        return $this->morphMany('App\Archivo', 'modelo')->where('tipo','Video');
    }
}
