<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tramite extends Model
{
    protected $table = 'tramites';

    public function lugar(){
        return $this->belongsTo('App\Lugar', 'lugar_id');
    }
}
