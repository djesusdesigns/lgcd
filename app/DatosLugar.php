<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


// este modelo representa los datos de un municipio,
// por ejemplo su gentilicio, costumbres, datos de contacto, etc


class DatosLugar extends Model
{
    protected $table = 'datos_lugar';
    
    public function lugar()
    {
        return $this->belongsTo('App\Lugar','lugar_id', 'id');
    }
}
