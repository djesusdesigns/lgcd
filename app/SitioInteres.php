<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use URL;

// representa los sitios de interes de un municipio

class SitioInteres extends Model
{
    protected $table = 'sitio_interes';
    public $timestamps = false;

    public function lugar(){
        return $this->belongsTo('App\Lugar', 'lugar_id');
    }

    public function likes()
    {
        return $this->morphMany('App\Like', 'seccion');
    }

    public function archivos()
    {
        return $this->morphMany('App\Archivo', 'modelo');
    }

    public function imagenes()
    {
        return $this->morphMany('App\Archivo', 'modelo')->where('tipo','Imagen');
    }

    public function audios()
    {
        return $this->morphMany('App\Archivo', 'modelo')->where('tipo','Audio');
    }

    public function documentos()
    {
        return $this->morphMany('App\Archivo', 'modelo')->where('tipo','Documento');
    }

    public function videos()
    {
        return $this->morphMany('App\Archivo', 'modelo')->where('tipo','Video');
    }

    public function comentarios()
    {
        return $this->morphMany('App\Comentario', 'seccion');
    }

    public function getUrlAttribute(){
        return URL::to('sitio-interes/' . $this->id .'/'.str_slug($this->nombre) );
    }
}
