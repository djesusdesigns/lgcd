<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

// representa los comentarios almacenados en el sistema
// para comentar las personas deben registrarse

class Comentario extends Model
{
    use SoftDeletes;

    protected $table = 'comentarios';

    protected $dates = ['deleted_at'];

    public function respuestas(){
		return $this->hasMany('App\Comentario');
	}

    public function padre(){
        return $this->belongsTo('App\Comentario','comentario_id', 'id');
    }

    public function usuario(){
        return $this->belongsTo('App\Usuario');
    }

    public function seccion()
    {
        return $this->morphTo();
    }
}
