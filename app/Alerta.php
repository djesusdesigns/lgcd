<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//un aspirante puede configurar alertas en email, actualmente está en deshuso

class Alerta extends Model
{
    protected $table = 'alertas';    
    public $timestamps = false;

    public function categoria(){
		return $this->belongsTo('App\Categoria','categoria_id', 'id');
	}

	public function aspirante(){
		return $this->belongsTo('App\Aspirante','aspirante_id', 'id');
	}

	public function lugar(){
		return $this->belongsTo('App\Lugar','lugar_id', 'id');
	}
}
