<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = 'categorias';

	public function alertas(){
		return $this->hasMany('App\Alerta', 'categoria_id');
	}

	public function trabajos(){
		return $this->hasMany('App\Trabajo', 'categoria_id');
	}

	public function aspirantes(){
        return $this->belongsToMany('App\Aspirante', 'alertas', 'categoria_id', 'aspirante_id');
    }

	public function empresas(){
        return $this->belongsToMany('App\Entidades');
    }    
}
