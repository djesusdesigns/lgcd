<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesion extends Model
{
    protected $table = 'profesiones';
    public $timestamps = false;


	public function superProfesion(){
        return $this->belongsTo('App\SuperProfesion', 'super_profesion_id');
    }
}
