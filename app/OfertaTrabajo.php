<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//represennta las ofertas de trabajo que se muestran en cundiempleos
class OfertaTrabajo extends Model
{
    protected $table = 'trabajos';

    public function tipo_educacion(){
        return $this->belongsTo('App\TipoEducacion', 'tipo_educacion_id');
    }

    public function tipo_contrato(){
        return $this->belongsTo('App\TipoContrato', 'tipo_contrato_id');
    }

    public function lugar(){
        return $this->belongsTo('App\Lugar', 'lugar_id');
    }

    public function entidad(){
        return $this->belongsTo('App\Entidad', 'entidad_id');
    }

    public function categoria(){
        return $this->belongsTo('App\Actividad', 'categoria_id');
    }

	public function aspirantes(){
        return $this->belongsToMany('App\Aspirante', 'aspirante_trabajo', 'trabajo_id', 'aspirante_id');
    }

    public function recientes()
    {
        return $this->orderBy('created_at');
    }
}
