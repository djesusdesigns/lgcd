<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Usuario;
use App\DatosLugar;
use App\Lugar;

// este modelo representa a los medios/empresas/comercio/medios
// todas las entidades se relacionan con la tabla usuarios
// en la tabla usuarios esta el rol que le corresponde
// si el usuario tiene el rol No 1 se entenderá como aspirante
// si el usuario tiene el rol No 2 se entenderá como empresa
// si el usuario tiene el rol No 3 se entenderá como municipio
// si el usuario tiene el rol No 4 se entenderá como administrador
// adicional a esto, si una entidad con rol # 2 tiene registrada una o varias
// de las categorias relacionadas a medios se considerara como tal
// para saber si una empresa es formal o no, se debe revisar el campo tipo_negocio
// en las busquedas de cundiamarillas solo apareceran las entidades que tengan sucursales
// si una empresa no es formal, se crea una sucursal en la base de datos pero se oculta al publico en las vistas


class Entidad extends Model
{
    protected $table = 'entidades';

	public function sucursales(){
		return $this->hasMany('App\Sucursal', 'entidad_id');
	}

    public function sucursal($id){
        return $this->hasMany('App\Sucursal', 'entidad_id')->where('sucursal.id',$id);
    }

    public function sucursal_principal(){
        return $this->hasMany('App\Sucursal', 'entidad_id')->where('tipo','Principal');
    }

	public function lugares(){
        return $this->belongsToMany('App\Lugar', 'coberturas', 'entidad_id', 'lugar_id');
    }

    public function usuario(){
        return $this->belongsTo('App\Usuario', 'usuario_id');
    }

	public function req_express(){
		return $this->hasMany('App\ReqExpress', 'entidad_id');
	}

    public function sector(){
		return $this->belongsTo('App\Sector','sector_id', 'id');
	}

	public function archivos(){
		return $this->hasMany('App\Archivo', 'entidad_id');
	}

	public function trabajos(){
		return $this->hasMany('App\Trabajo', 'entidad_id');
	}

	public function categorias(){
        return $this->belongsToMany('App\Categoria', 'entidades_actividades', 'entidad_id', 'actividad_id');
    }

    public function produservicios(){
        return $this->belongsToMany('App\Produservicio', 'entidades_produservicios', 'entidad_id', 'produservicio_id');
    }

    public function produservicios_ofrece(){
        return $this->belongsToMany('App\Produservicio', 'entidades_produservicios', 'entidad_id', 'produservicio_id')->where('entidades_produservicios.ofrece',1);
    }

    public function produservicios_requiere(){
        return $this->belongsToMany('App\Produservicio', 'entidades_produservicios', 'entidad_id', 'produservicio_id')->where('entidades_produservicios.ofrece',2);
    }

    public function actividades(){
        return $this->belongsToMany('App\Actividad', 'entidades_actividades', 'entidad_id', 'actividad_id');
    }

    public function es_medio()
    {
        $actividades = $this->actividades()
                ->where(function ($query){
                    $query
                        ->orWhere("entidades_actividades.actividad_id",40)
                        ->orWhere("entidades_actividades.actividad_id",722)
                        ->orWhere("entidades_actividades.actividad_id",1646)
                        ->orWhere("entidades_actividades.actividad_id",1405)
                        ->orWhere("entidades_actividades.actividad_id",1441)
                        ->orWhere("entidades_actividades.actividad_id",1565);
                })->get();

        return sizeof($actividades) ? true : false;
    }

	static function empresas($request){
        $empresas = collect([]);
		if($request->has('lugar')){
			$empresas = Entidad::join('sucursales', 'entidades.id', '=', 'sucursales.entidad_id')
	            ->select('entidades.razon_social')
	            ->where('sucursales.lugar_id', $request->lugar)
	            ->where('sucursales.tipo', 'Principal')
	            ->where('entidades.tipo', 'Empresa')
	            ->get();
        }
        return $empresas;
	}

    static function medios(){

        $medios = Entidad::join('sucursales', 'entidades.id', '=', 'sucursales.entidad_id')
            ->leftjoin('entidades_actividades','entidades_actividades.entidad_id','=','entidades.id')
            ->where('entidades.tipo', 'Empresa')
            ->where(function ($query){
                $query
                    ->orWhere("entidades_actividades.actividad_id",40)
                    ->orWhere("entidades_actividades.actividad_id",722)
                    ->orWhere("entidades_actividades.actividad_id",1646)
                    ->orWhere("entidades_actividades.actividad_id",1405)
                    ->orWhere("entidades_actividades.actividad_id",1441)
                    ->orWhere("entidades_actividades.actividad_id",1565);
            });

        return $medios;
    }

	static function verificarPremium($lugar_id){
        $municipio = Lugar::find($lugar_id);
        $entidad = Sucursal::join('entidades', 'sucursales.entidad_id', '=', 'entidades.id')
        ->join('lugares', 'lugares.id', '=', 'sucursales.lugar_id')
        ->select('entidades.*')
        ->where('sucursales.lugar_id',$municipio->id)
        ->where('entidades.tipo','Municipio')
        ->first();

        if($entidad){
        	$usuario = Usuario::where('id','=',$entidad->usuario_id)->first();
        	if($usuario->tipo == 2){
        		$datosLugar = $municipio->datos;
        		$logo = ($datosLugar)? $datosLugar->logo:"";
                $bannerBuscador = ($datosLugar)? $datosLugar->banner_buscador:"";
        		return [
                    "tema"=>$entidad->estilos,
                    'logo'=> $logo,
                    'bannerBuscador'=> $bannerBuscador,
                    'lugar'=>$lugar_id,
                    'entidad' => $entidad,
                    'municipio' => $municipio
                ];
        	}else{
        		return null;
        	}
        }else{
        	return null;
        }
	}


}
