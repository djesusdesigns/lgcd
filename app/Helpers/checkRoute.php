<?php
/**
* change plain number to formatted currency
*
* @param $number
* @param $currency
*/
function routeHas($route)
{
   return request()->is($route) || request()->is($route.'/*');
}