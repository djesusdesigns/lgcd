<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// representa los banners que se muestran en la guia


class Banner extends Model
{
    protected $table = 'banners';

    public function lugar(){
        return $this->belongsTo('App\Lugar', 'lugar_id');
    }

    static function get($tipo,$seccion)
    {
    	$banners = Banner::where('tipo','=',$tipo)
                                ->where(function ($query) use($seccion) {
                                    $query->where('seccion','=',$seccion)
                                          ->orWhere('seccion','=','todas');
                                })
                                //->orderByRaw("RAND()")
                                ->orderBy('id', 'desc')
                                ->take(1);

        // los lugares premium son municipios que pagan y tienen ciertos privilegios
        // uno de esos privilegios es que se puede mostrar publicidad especificamente para ese municipio
        if(session('lugarPremium'))
            $banners = $banners->where('lugar_id',session('lugar')['id'])->first();
        else
            $banners = $banners->first();


        return $banners;
    }

    static function getAll($tipo,$seccion,$random = 0,$limit = 0)
    {
        $banners = Banner::where('tipo','=',$tipo)
        ->where(function ($query) use($seccion) {
            $query->where('seccion','=',$seccion)
                  ->orWhere('seccion','=','todas');
        });

        if($limit){
            $banners = $banners
            ->limit($limit);
        }

        if($random = 1){
            $banners = $banners
            ->orderByRaw("RAND()")
            ->get();
        }else{
            $banners = $banners//->orderByRaw("RAND()")
            ->orderBy('id', 'desc')
            ->get();
        }

        return $banners;
    }
}
