<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use App\Noticia;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Schema::defaultStringLength(191);

        Relation::morphMap([
            'Noticia' => 'App\Noticia',
            'Columna' => 'App\Columna',
            'Evento' => 'App\Evento',
            'SitioInteres' => 'App\SitioInteres',
        ]);

        $NewsFeed = Noticia::publicadas()
            ->take(3)
            ->orderBy('f_publicacion', 'desc')
            ->get();

        view()->share('NewsFeed', $NewsFeed);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
