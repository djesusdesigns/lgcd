<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...


        View::composer('*','App\Http\ViewComposers\CurrentUserComposer');
        View::composer('*','App\Http\ViewComposers\LugarComposer');

        View::composer(
            ['panel-usuarios.entidades.inicio', 'panel-usuarios.aspirantes.inicio'],
            'App\Http\ViewComposers\ActividadComposer'
        );

        View::composer(
            ['panel-usuarios.entidades.inicio', 'panel-usuarios.aspirantes.inicio'],
            'App\Http\ViewComposers\ContratoComposer'
        );

        View::composer(
            ['panel-usuarios.entidades.inicio', 'panel-usuarios.aspirantes.inicio'],
            'App\Http\ViewComposers\EducacionComposer'
        );

        
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}