<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuperProfesion extends Model
{
    protected $table = 'super_profesiones';
    public $timestamps = false;


	public function profesiones(){
        return $this->hasMany('App\Profesion', 'super_profesion_id');
    }
}
