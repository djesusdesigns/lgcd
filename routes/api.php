<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'cors'],function(){

	Route::get('auth/{provider}/usuario/{usertype}', 'Auth\SocialAuthController@appSocialLogin');
	Route::post('auth/{provider}/usuario/{usertype}', 'Auth\SocialAuthController@appSocialLogin');

	Route::post('login', 'Auth\AuthController@loginj');
	Route::post('logout', 'Auth\AuthController@logoutj');

	Route::post('/password/recuperar','Auth\AuthController@enviar_correo')->name('password.reset');

	Route::post('/registro', 'Auth\AuthController@registro');

	Route::get('/','InicioController@index');
	
	Route::get('noticias','NoticiaController@indexGuest');
	Route::get('columnas','ColumnaController@indexGuest');
	Route::get('noticias/{slugMunicipio}','NoticiaController@busquedaPorMunicipio');

	Route::resource("usuario/like", "PanelUsuarios\LikeController")->middleware('jwt.auth');

	Route::post("usuario/noticias", "PanelUsuarios\NoticiaController@store")->middleware('jwt.auth');
	Route::post("usuario/noticia", "PanelUsuarios\NoticiaController@store")->middleware('jwt.auth');
	Route::post("usuario/noticia/{id}/editar", "PanelUsuarios\NoticiaController@update")->middleware('jwt.auth');
	Route::post("usuario/noticia/{id}/eliminar", "PanelUsuarios\NoticiaController@destroy")->middleware('jwt.auth');

	Route::post("usuario/columnas", "PanelUsuarios\ColumnaController@store")->middleware('jwt.auth');
	Route::post("usuario/columna", "PanelUsuarios\ColumnaController@store")->middleware('jwt.auth');
	Route::post("usuario/columna/{id}/editar", "PanelUsuarios\ColumnaController@update")->middleware('jwt.auth');
	Route::post("usuario/columna/{id}/eliminar", "PanelUsuarios\ColumnaController@destroy")->middleware('jwt.auth');

	Route::get('eventos','EventoController@indexGuest');
	Route::get('eventos/{slugMunicipio}','EventoController@busquedaPorMunicipio');

	Route::post("usuario/eventos", "PanelUsuarios\EventoController@store")->middleware('jwt.auth');
	Route::post("usuario/evento/{id}/editar", "PanelUsuarios\EventoController@update")->middleware('jwt.auth');
	Route::post("usuario/evento/{id}/eliminar", "PanelUsuarios\EventoController@destroy")->middleware('jwt.auth');

	Route::get('municipios','MunicipioController@indexGuest');
	Route::get('municipio/{slugMunicipio}','MunicipioController@show');

	Route::get('usuario', 'InicioController@getUsuario')->middleware('jwt.auth');
	Route::get('usuario/noti-eventos', 'UsuarioController@notiEventos')->middleware('jwt.auth');

	Route::post('usuario/aspirante/update', 'PanelUsuarios\Aspirantes\AspiranteController@store')->middleware('jwt.auth');
	Route::post('usuario/aspirante/img-perfil', 'PanelUsuarios\Aspirantes\AspiranteController@cambiar_imagen')->middleware('jwt.auth');

	Route::post('usuario/entidad/update', 'PanelUsuarios\Entidades\EntidadController@actualizar_empresa')->middleware('jwt.auth');
	Route::post('usuario/entidad/img-perfil', 'PanelUsuarios\Entidades\EntidadController@cambiar_logo')->middleware('jwt.auth');

	Route::get('/archivo/{id}/link', 'PanelUsuarios\ArchivoPublicoController@obtenerEnlace');

	Route::post('/archivo/{modelo}/{modeloId}', 'PanelUsuarios\ArchivoPublicoController@cargar')->middleware('jwt.auth');

	Route::get('banners', 'Admin\BannerController@getBanners');
});

