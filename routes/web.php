<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//RUTAS LA GUIA CUNDINAMARCA
Route::get('/','InicioController@index');
Route::get('/laguia','InicioController@volverInicio');

Route::get('/buscador','BuscadorController@redireccionar');
Route::get('/busqueda/{tipo}/{idMunicipio}/{clave}','BuscadorController@busqueda');
Route::get('/busqueda/{tipo}/{idMunicipio}','BuscadorController@busqueda');

Route::get('noticias','NoticiaController@indexGuest');
Route::get('noticias/{slugMunicipio}','NoticiaController@busquedaPorMunicipio');
Route::get('noticia/{id}/{titulo}','NoticiaController@show');
Route::get('noticia/{id}','NoticiaController@show');

Route::get('columnas','ColumnaController@indexGuest');
Route::get('columnas/{slugMunicipio}','ColumnaController@busquedaPorMunicipio');
Route::get('columna/{id}/{titulo}','ColumnaController@show');
Route::get('columna/{id}','ColumnaController@show');

Route::post("usuario/columna/{id}/editar", "PanelUsuarios\ColumnaController@update");

Route::get('eventos','EventoController@indexGuest');
Route::get('eventos/{slugMunicipio}','EventoController@busquedaPorMunicipio');
Route::get('eventos/archivo/{fecha}','EventoController@archivo');
Route::get('evento/{id}/{titulo}','EventoController@show');
Route::get('evento/{id}','EventoController@show');

Route::get('municipios','MunicipioController@indexGuest');
Route::get('municipios/json','MunicipioController@getJson');
Route::get('municipio/{slugMunicipio}','MunicipioController@show');
Route::get('municipio/{slugMunicipio}/{nombre}','MunicipioController@show');

Route::get('sitio-interes-usuario/{id}/{nombre}','SitioInteresUsuarioController@show');
Route::get('sitio-interes-usuario/{id}','SitioInteresUsuarioController@show');

Route::get('sitio-interes/{id}/{titulo}','SitioInteresController@show');
Route::get('sitio-interes/{id}','SitioInteresController@show');

Route::get('medios','MedioController@index');
Route::get('medios/{slugMunicipio}','MedioController@busquedaPorMunicipio');
Route::get('medio/{id}/{slug}','MedioController@consultar');
Route::post('medios/contacto-medio', 'MedioController@enviarCorreoContacto');

Route::get('empleos', 'OfertaTrabajoController@index');
Route::get('empleos/{slugMunicipio}', 'OfertaTrabajoController@busquedaPorMunicipio');
Route::get('empleo/{id}', 'OfertaTrabajoController@show');
Route::get('empleo/{id}/{slug}', 'OfertaTrabajoController@show');

Route::get('amarillas/', 'EmpresaController@index');
Route::get('amarillas/{slugMunicipio}','EmpresaController@busquedaPorMunicipio');
Route::get('amarillas/empresa/{sucursal}', 'EmpresaController@consultar');
Route::get('amarillas/empresa/{sucursal}/{slug}', 'EmpresaController@consultar');
Route::post('amarillas/contacto-empresa', 'EmpresaController@enviarCorreoContacto');

Route::get('tramites','TramiteController@indexGuest');
Route::get('tramites/{slugMunicipio}','TramiteController@busquedaPorMunicipio');
Route::get('tramite/{id}','TramiteController@show');
Route::get('tramite/{id}/{nombre}','TramiteController@show');

Route::post("comentario/{id}/respuestas", "ComentarioController@store");
Route::post("comentario", "ComentarioController@store");

//Páginas estaticas
Route::get('/acerca-de', 'InicioController@acercade');
Route::get('/terminos-y-condiciones', 'InicioController@terminos');
Route::get('/politicas-privacidad', 'InicioController@polidepriv');
Route::get('/ayuda-empresas', 'InicioController@ayudaEmpresas');
Route::get('/ayuda-personas', 'InicioController@ayudaPersonas');
Route::get('/actualizar-datos', 'InicioController@landinglogin');
Route::get('/paute-aqui', 'InicioController@pauteaqui');
Route::get('/preguntas-frecuentes', 'InicioController@preguntas');
Route::get('/contactenos', 'InicioController@contactenos');
Route::get('/reporte-problema', 'InicioController@reporteProblema');

//vista de aspirante sin autenticacion
Route::get('/aspirante/{id}', 'PanelUsuarios\Aspirantes\AspiranteController@generar_pdf');
Route::get('/aspirante_html/{id}', 'PanelUsuarios\Aspirantes\AspiranteController@generar');

Route::post('/crear_municipio', 'PanelUsuarios\Entidades\Municipios\MunicipioController@crear');

Route::get('profesion/buscar/{formato}','PanelUsuarios\ProfesionController@buscar');
Route::get('produservicio/buscar/{formato}','PanelUsuarios\ProduservicioController@buscar');

//RUTA PARA DESCARGAR ARCHIVOS SUBIDOS POR EMPRESAS
Route::get('/file/{usuario}/{archivo}', 'PanelUsuarios\ArchivoController@descargar');

//ruta autenticacion social
Route::group(['middleware' => ['web']], function () {
    Route::get('auth/{provider}', 'Auth\SocialAuthController@redirectToProvider');
    Route::get('auth/{provider}/usuario/{usertype}', 'Auth\SocialAuthController@redirectToProvider');
	Route::get('auth/{provider}/callback', 'Auth\SocialAuthController@handleProviderCallback');
});

Route::group(['middleware' => 'auth','prefix' => 'usuario'], function () {

	//redireccional al perfil correcto de cada tipo de usuario
	Route::get('/perfil',function (){
		if(\Auth::user()->rol_id == 1)
			return redirect('/usuario/aspirante/perfil');
		else if(\Auth::user()->rol_id == 2 || \Auth::user()->rol_id == 3)
			return redirect('/usuario/entidad/perfil');
		else if(\Auth::user()->rol_id == 4)
			return redirect('/admin');
	});
	
	Route::resource("noticia", "PanelUsuarios\NoticiaController");
	Route::resource("evento", "PanelUsuarios\EventoController");
	Route::resource("columna", "PanelUsuarios\ColumnaController");
	Route::resource("sitio-interes", "PanelUsuarios\SitioInteresController");
	Route::resource("like", "PanelUsuarios\LikeController");

	Route::post("/{id}/eliminar", "UsuarioController@destroy");

	Route::post('/cargar_archivo', 'PanelUsuarios\ArchivoController@cargar');
	Route::post('/eliminar_archivo/{id}', 'PanelUsuarios\ArchivoController@eliminar');

	Route::get('/oferta/{id}/aplicar', 'PanelUsuarios\Aspirantes\OfertaTrabajoController@aplicar');
	Route::resource("oferta", "PanelUsuarios\OfertaTrabajoController");

	Route::post('/archivo/{modelo}/{modeloId}', 'PanelUsuarios\ArchivoPublicoController@cargar');
	Route::delete('/archivo/{id}/eliminar', 'PanelUsuarios\ArchivoPublicoController@eliminar');

	Route::post('/multimedia/imagen','Admin\InicioController@subirImagen');

	//ASPIRANTES
	Route::group(['namespace' => 'PanelUsuarios\Aspirantes'], function () {
		Route::get('/aspirante/perfil', 'AspiranteController@mostrar_panel');

		Route::post('/aspirante/img-perfil', 'AspiranteController@cambiar_imagen');
		Route::post('/aspirante/tengo_empleo', 'AspiranteController@tengo_empleo');
		Route::post('/aspirante/cambiar_estado', 'AspiranteController@cambiar_estado');
		Route::post('/aspirante/habilidad', 'AspiranteController@crear_habilidad');


		Route::delete('/aspirante/habilidad/{id}', 'AspiranteController@eliminar_habilidad');	
		Route::delete('/aspirante/oferta/{id}', 'AspiranteController@eliminar_aplicacion');

		Route::resource("aspirante", "AspiranteController");

		Route::resource("aspirante/experiencia", "ExperienciaController");

		Route::post('aspirante/educacion/{id}/editar', 'EducacionController@update');
		Route::resource("aspirante/educacion", "EducacionController");

		Route::get('/{id}/notificar', 'AspiranteController@notificar');
	});

	//EMPRESAS
	Route::group(['prefix' => 'entidad', 'namespace' => 'PanelUsuarios\Entidades'], function () {
		Route::get('/perfil', 'EntidadController@mostrar_panel');
		Route::get('/perfil/edit', 'EntidadController@edit');

		Route::post('/img-perfil', 'EntidadController@cambiar_logo');
		Route::post('/actualizar_contacto', 'EntidadController@actualizar_contacto');
		Route::post('/actualizar_empresa', 'EntidadController@actualizar_empresa');		
		Route::post('/actualizar_comercial', 'EntidadController@actualizar_comercial');	

		Route::group(['prefix' => 'estadistica'], function () {

			Route::get('/{idLugar}/entidades_activas', 'EstadisticaController@entidades_activas');
			Route::get('/{idLugar}/entidades_activas/excel', 'EstadisticaController@entidades_activas_excel');

			Route::get('/{idLugar}/aspirantes', 'EstadisticaController@aspirantes');
			Route::get('/{idLugar}/aspirantes/excel', 'EstadisticaController@aspirantes_excel');

			Route::get('/{idLugar}/personas_trabajando', 'EstadisticaController@personas_trabajando');
			Route::get('/{idLugar}/personas_trabajando/excel', 'EstadisticaController@personas_trabajando_excel');

			Route::get('/{idLugar}/empleos_creados', 'EstadisticaController@empleos_creados');
			Route::get('/{idLugar}/empleos_creados/excel', 'EstadisticaController@empleos_creados_excel');

			Route::get('/{idLugar}/ofertas_aspirantes', 'EstadisticaController@ofertas_aspirantes');
			Route::get('/{idLugar}/ofertas_aspirantes/excel', 'EstadisticaController@ofertas_aspirantes_excel');

			Route::get('/{idLugar}/actividades', 'EstadisticaController@actividades');
			Route::get('/{idLugar}/actividades/excel', 'EstadisticaController@actividades_excel');

			Route::get('/{idLugar}/creacion_empleos_empresas', 'EstadisticaController@empleos_empresas');
			Route::get('/{idLugar}/creacion_empleos_empresas/excel', 'EstadisticaController@empleos_empresas_excel');

			Route::get('/{idLugar}/dist_empresas_empleos', 'EstadisticaController@dist_empresas_empleos');
			Route::get('/{idLugar}/dist_empresas_empleos/excel', 'EstadisticaController@dist_empresas_empleos_excel');

			Route::get('/{idLugar}/competitividad', 'EstadisticaController@competitividad');
			Route::get('/{idLugar}/competitividad/excel', 'EstadisticaController@competitividad_excel');

			Route::get('/{idLugar}/produservicios', 'EstadisticaController@produservicios');
			Route::get('/{idLugar}/produservicios/excel', 'EstadisticaController@produservicios_excel');

			Route::get('/{idEntidad}/empresa/ofertas', 'EstadisticaController@ofertas_empresa');
			Route::get('/{idEntidad}/empresa/ofertas/excel', 'EstadisticaController@ofertas_empresa_excel');

			Route::get('/{idEntidad}/empresa/hojas-de-vida', 'EstadisticaController@cv_empresa');
			Route::get('/{idEntidad}/empresa/hojas-de-vida/excel', 'EstadisticaController@cv_empresa_excel');

		});

	});

	//EMPRESAS
	Route::group(['prefix' => 'empresa', 'namespace' => 'PanelUsuarios\Entidades\Empresas'], function () {
		Route::get('/consultar_sucursal/{id}', 'SucursalController@consultar');		
		Route::post('/eliminar_sucursal/{id}', 'SucursalController@eliminar');		
		Route::post('/crear_sucursal', 'SucursalController@crear');		
		Route::post('/actualizar_sucursal', 'SucursalController@actualizar');	
	});

	//MUNICIPIOS
	Route::group(['prefix' => 'municipio', 'namespace' => 'PanelUsuarios\Entidades\Municipios'], function () {
		Route::resource("dependencia", "DependenciaController");
		Route::resource("funcionario", "FuncionarioController");
		Route::resource("num-emergencia", "NumEmergenciaController");
		Route::resource("galeria", "GaleriaController");
		Route::resource("sitio-interes", "SitioInteresController");
	});

});

//RUTAS PARA PANEL ADMIN
Route::group(['middleware' => ['auth','auth.admin'],'prefix' => 'admin','namespace' => 'Admin'],function(){
	Route::get('/','InicioController@inicio');
	Route::post('multimedia/imagen','InicioController@subirImagen');

	Route::get('usuario/{id}/cambiar_estado','UsuarioController@cambiar_estado');
	Route::post('usuario/{id}/update','UsuarioController@update');
	Route::resource("usuario", 'UsuarioController');

	Route::get('noticia/{id}/destacar','NoticiaController@destacar');
	Route::get('noticia/{id}/replegar','NoticiaController@replegar');
	Route::get('noticia/{id}/eliminar','NoticiaController@destroy');
	Route::post('noticia/{id}/update','NoticiaController@update');
	Route::resource("noticia", "NoticiaController");

	Route::get('columna/{id}/destacar','ColumnaController@destacar');
	Route::get('columna/{id}/replegar','ColumnaController@replegar');
	Route::get('columna/{id}/eliminar','ColumnaController@destroy');
	Route::post('columna/{id}/update','ColumnaController@update');
	Route::resource("columna", "ColumnaController");

	Route::get('evento/{id}/destacar','EventoController@destacar');
	Route::get('evento/{id}/replegar','EventoController@replegar');
	Route::get('evento/{id}/eliminar','EventoController@destroy');
	Route::post('evento/{id}/update','EventoController@update');
	Route::resource("evento", "EventoController");

	Route::get('municipio/{id}/entidad/{idEntidad}/personalizar','MunicipioController@personalizar');
	Route::post('municipio/{id}/update','MunicipioController@update');
	Route::get('municipio/{id}/estadisticas','MunicipioController@estadisticas');
	Route::resource("municipio", "MunicipioController");

	Route::post('tramite/{id}/update','TramiteController@update');
	Route::resource("tramite", "TramiteController");

	Route::get('entidad/{id}/eliminar','EntidadController@destroy');
	Route::get('entidad/{id}/suscripcion','EntidadController@mostrar_suscripcion');
	Route::get('entidad/{id}/estadisticas','EntidadController@estadisticas');
	Route::post('entidad/suscripcion','EntidadController@suscribir');
	Route::post('entidad/{id}/update','EntidadController@update');
	Route::resource("entidad", 'EntidadController');
	
	Route::get('empleo/{id}/destacar','OfertaTrabajoController@destacar');
	Route::get('empleo/{id}/replegar','OfertaTrabajoController@replegar');
	Route::get('empleo/{id}/eliminar','OfertaTrabajoController@destroy');
	Route::post('empleo/{id}/update','OfertaTrabajoController@update');
	Route::resource("empleo", 'OfertaTrabajoController');

	Route::get('comentario/{id}/eliminar','ComentarioController@destroy');
	Route::resource("comentario", "ComentarioController");

	Route::post('banner/{id}/update','BannerController@update');
	Route::get('banner/{id}/eliminar','BannerController@destroy');
	Route::resource("banner", "BannerController");

	Route::post('municipio/{idMunicipio}/dependencia/{idDependencia}/update','DependenciaController@update');
	Route::get('municipio/{idMunicipio}/dependencia/{idDependencia}/eliminar','DependenciaController@destroy');
	Route::resource("municipio.dependencia", "DependenciaController");

	Route::post('municipio/{idMunicipio}/funcionario/{idFuncionario}/update','FuncionarioController@update');
	Route::get('municipio/{idMunicipio}/funcionario/{idFuncionario}/eliminar','FuncionarioController@destroy');
	Route::resource("municipio.funcionario", "FuncionarioController");

	Route::get('municipio/{idMunicipio}/simbolo/create/{tipo}','SimboloController@create');
	Route::get('municipio/{idMunicipio}/simbolo/{idSimbolo}/edit/{tipo}','SimboloController@edit');
	Route::post('municipio/{idMunicipio}/simbolo/{idSimbolo}/update','SimboloController@update');
	Route::get('municipio/{idMunicipio}/simbolo/{idSimbolo}/eliminar','SimboloController@destroy');
	Route::resource("municipio.simbolo", "SimboloController");

	Route::post('municipio/{idMunicipio}/num-emergencia/{idNumEmergencia}/update','NumEmergenciaController@update');
	Route::get('municipio/{idMunicipio}/num-emergencia/{idNumEmergencia}/eliminar','NumEmergenciaController@destroy');
	Route::resource("municipio.num-emergencia", "NumEmergenciaController");

	Route::post('municipio/{idMunicipio}/sitio-interes/{idSitio}/update','SitioInteresController@update');
	Route::get('municipio/{idMunicipio}/sitio-interes/{idSitio}/eliminar','SitioInteresController@destroy');
	Route::resource("municipio.sitio-interes", "SitioInteresController");

	Route::get('municipio/{idMunicipio}/galeria/{idFoto}/eliminar','GaleriaController@destroy');
	Route::resource("municipio.galeria", "GaleriaController");

	Route::get('estadisticas','EstadisticaController@inicio');

	Route::group(['prefix' => 'estadisticas'],function(){
		
		Route::get('/entidades_activas', 'EstadisticaController@entidades_activas');
		Route::get('/entidades_activas/excel', 'EstadisticaController@entidades_activas_excel');
		Route::get('/aspirantes', 'EstadisticaController@aspirantes');
		Route::get('/aspirantes/excel', 'EstadisticaController@aspirantes_excel');
		Route::get('/personas_trabajando', 'EstadisticaController@personas_trabajando');
		Route::get('/personas_trabajando/excel', 'EstadisticaController@personas_trabajando_excel');
		Route::get('/empleos_creados', 'EstadisticaController@empleos_creados');
		Route::get('/empleos_creados/excel', 'EstadisticaController@empleos_creados_excel');
		Route::get('/ofertas_aspirantes', 'EstadisticaController@ofertas_aspirantes');
		Route::get('/ofertas_aspirantes/excel', 'EstadisticaController@ofertas_aspirantes_excel');
		Route::get('/actividades', 'EstadisticaController@actividades');
		Route::get('/actividades/excel', 'EstadisticaController@actividades_excel');
		Route::get('/creacion_empleos_empresas', 'EstadisticaController@empleos_empresas');
		Route::get('/creacion_empleos_empresas/excel', 'EstadisticaController@empleos_empresas_excel');
		Route::get('/dist_empresas_empleos', 'EstadisticaController@dist_empresas_empleos');
		Route::get('/dist_empresas_empleos/excel', 'EstadisticaController@dist_empresas_empleos_excel');
		Route::get('/comp_empresas_indep', 'EstadisticaController@comp_empresas_indep');
		Route::get('/comp_empresas_indep/excel', 'EstadisticaController@comp_empresas_indep_excel');
	});
	

	Route::group(['prefix' => 'configuracion'],function(){
		Route::get('/','InicioController@inicioConf');

		Route::get('actividad/buscar','ActividadController@buscar');
		Route::get('actividad/{id}/eliminar','ActividadController@destroy');
		Route::post('actividad/{id}/update','ActividadController@update');
		Route::resource("actividad", "ActividadController");

		Route::get('profesion/buscar','ProfesionController@buscar');
		Route::get('profesion/{id}/eliminar','ProfesionController@destroy');
		Route::post('profesion/{id}/update','ProfesionController@update');
		Route::resource("profesion", "ProfesionController");

		Route::get('produservicio/buscar','ProduservicioController@buscar');
		Route::get('produservicio/{id}/eliminar','ProduservicioController@destroy');
		Route::post('produservicio/{id}/update','ProduservicioController@update');
		Route::resource("produservicio", "ProduservicioController");

		Route::get('superProfesion/buscar','SuperProfesionController@buscar');
		Route::get('superProfesion/{id}/eliminar','SuperProfesionController@destroy');
		Route::post('superProfesion/{id}/update','SuperProfesionController@update');
		Route::resource("superProfesion", "SuperProfesionController");

		Route::get('superProduservicio/buscar','SuperProduservicioController@buscar');
		Route::get('superProduservicio/{id}/eliminar','SuperProduservicioController@destroy');
		Route::post('superProduservicio/{id}/update','SuperProduservicioController@update');
		Route::resource("superProduservicio", "SuperProduservicioController");
	});
});

//RUTAS DE AUTENTICACION Y REGISTRO

//Auth::routes();
//Route::get('/home', 'HomeController@index')->name('home');



Route::get('/login', 'Auth\AuthController@index')->name('login');
Route::get('/logout', 'Auth\AuthController@logout');

Route::post('/login', 'Auth\AuthController@login');

Route::get('/password/recuperar', 'Auth\AuthController@recuperar_password');
Route::post('/password/recuperar', 'Auth\AuthController@enviar_correo');

Route::get('/password/reset/{token}', 'Auth\AuthController@verificar_token_password')->name('password.reset');
Route::post('/password/reset', 'Auth\AuthController@resetear_password');

Route::get('/registro', 'Auth\AuthController@mostrar_registro');
Route::post('/registro', 'Auth\AuthController@registro');

Route::get('/correo/verificar/{token}', 'Auth\AuthController@verificar_correo');


//colas
Route::get('/colas/correo-ofertas-similares', 'PanelUsuarios\OfertaTrabajoController@correo_ofertas_similares');
Route::get('/colas/correo-estadisticas-empresas', 'PanelUsuarios\OfertaTrabajoController@correo_ofertas_similares');

Route::get('/registro/medio', function()
{
	return view('general.registro-medio');
});

Route::get('/registro/entidad', function()
{
	return view('general.registro-entidad');
});

Route::get('/registro/persona', function()
{
	return view('general.registro-persona');
});

Route::get('/fotos-municipales/', function()
{
	return view('fotos-municipales');
});

View::composer('*', function ($view) {
	View::share('viewName', $view->getName());
});
